- BUGS:

- Short term:
  - Controls
    - Allow mouse controls for menus?
    - Switch to using axes rather than booleans for movement directions -- 
	  will allow for a better feeling movement on controllers.
    - Multiple binds per input?  i.e.: menus use either thumbstick or d-pad.
  - Sound
    - Bulletproof the DirectSound interface.
	- Final SFX:
	  - Player / Saucer hit
	- Streaming music
	  - Menu track, gameplay track
  - UI
	- Options
	  - Rebindable keys?
	  - Colorblind mode?
	    - TODO:  Research the mechanics of doing this.  Can we just 
		  cheat and do a color remap in a post shader?

- Long term:
  - DirectX renderer impl?
  - Normal mapped lighting?
    - Lot of art cost here, but hand-drawing depth maps is not out 
      of the question.
  - Optional switch for using the kernel/hardware mixer rather than
    software?
  - Linux support
  - Achievements?
  - Networked high scores