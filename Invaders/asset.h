//
// Invaders - asset.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

enum AssetID {
    AssetID_SpriteBG,
    AssetID_SpriteBGEmission,
    AssetID_SpriteInvader,
    AssetID_SpriteSaucer,
    AssetID_SpriteSaucerEmission,
    AssetID_SpriteShip,
	AssetID_SpriteShipDebris,
    AssetID_SpriteProjectiles,
    AssetID_SpriteButtons,
    AssetID_SpriteWideButtons,
    AssetID_SpriteSplashLogo,
    AssetID_SpriteSplashText,
    AssetID_SpriteSplashCursor,

    AssetID_OggGameMusic,

    AssetID_Count,
};

#pragma once

#pragma pack(push, 1)
struct PackedSpriteSet {
    u32 id;
    u32 png_data_offset;  // Offset from first byte of file
    u32 png_data_size;    // Size of png data, in bytes
    u32 sprite_count;     // How many PackedSprites follow this structure
};

struct PackedSprite {
    float size_x;
    float size_y;
    float uv_min_x;
    float uv_min_y;
    float uv_max_x;
    float uv_max_y;
};

struct PackedOgg {
    u32 id;
    u32 ogg_data_offset;
    u32 ogg_data_size;
};
#pragma pack(pop)