//
// Invaders - colors.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"

const vec4 COLOR_BLACK = Vec4(0.000000f, 0.000000f, 0.000000f, 1.0f);
const vec4 COLOR_DARK_VIOLET = Vec4(0.133333f, 0.125490f, 0.203922f, 1.0f);
const vec4 COLOR_DARK_PURPLE = Vec4(0.270588f, 0.156863f, 0.235294f, 1.0f);
const vec4 COLOR_DARK_BROWN = Vec4(0.400000f, 0.223529f, 0.192157f, 1.0f);
const vec4 COLOR_BROWN = Vec4(0.560784f, 0.337255f, 0.231373f, 1.0f);
const vec4 COLOR_ORANGE = Vec4(0.874510f, 0.443137f, 0.149020f, 1.0f);
const vec4 COLOR_LIGHT_BROWN = Vec4(0.850980f, 0.627451f, 0.400000f, 1.0f);
const vec4 COLOR_PEACH = Vec4(0.933333f, 0.764706f, 0.603922f, 1.0f);
const vec4 COLOR_YELLOW = Vec4(0.984314f, 0.949020f, 0.211765f, 1.0f);
const vec4 COLOR_LIGHT_GREEN = Vec4(0.600000f, 0.898039f, 0.313725f, 1.0f);
const vec4 COLOR_GREEN = Vec4(0.415686f, 0.745098f, 0.188235f, 1.0f);
const vec4 COLOR_TEAL = Vec4(0.215686f, 0.580392f, 0.431373f, 1.0f);
const vec4 COLOR_DARK_GREEN = Vec4(0.294118f, 0.411765f, 0.184314f, 1.0f);
const vec4 COLOR_SAGE = Vec4(0.321569f, 0.294118f, 0.141176f, 1.0f);
const vec4 COLOR_DARK_SAGE = Vec4(0.196078f, 0.235294f, 0.223529f, 1.0f);
const vec4 COLOR_DARK_BLUE = Vec4(0.247059f, 0.247059f, 0.454902f, 1.0f);
const vec4 COLOR_BLUE = Vec4(0.188235f, 0.376471f, 0.509804f, 1.0f);
const vec4 COLOR_LIGHT_BLUE = Vec4(0.356863f, 0.431373f, 0.882353f, 1.0f);
const vec4 COLOR_PALE_BLUE = Vec4(0.388235f, 0.607843f, 1.000000f, 1.0f);
const vec4 COLOR_LIGHT_TEAL = Vec4(0.372549f, 0.803922f, 0.894118f, 1.0f);
const vec4 COLOR_PALE_TEAL = Vec4(0.796078f, 0.858824f, 0.988235f, 1.0f);
const vec4 COLOR_WHITE = Vec4(1.000000f, 1.000000f, 1.000000f, 1.0f);
const vec4 COLOR_STEEL_BLUE = Vec4(0.607843f, 0.678431f, 0.717647f, 1.0f);
const vec4 COLOR_LIGHT_GREY = Vec4(0.517647f, 0.494118f, 0.529412f, 1.0f);
const vec4 COLOR_GREY = Vec4(0.411765f, 0.415686f, 0.415686f, 1.0f);
const vec4 COLOR_DARK_GREY = Vec4(0.349020f, 0.337255f, 0.321569f, 1.0f);
const vec4 COLOR_PURPLE = Vec4(0.462745f, 0.258824f, 0.541176f, 1.0f);
const vec4 COLOR_RED = Vec4(0.674510f, 0.196078f, 0.196078f, 1.0f);
const vec4 COLOR_LIGHT_RED = Vec4(0.850980f, 0.341176f, 0.388235f, 1.0f);
const vec4 COLOR_PINK = Vec4(0.843137f, 0.482353f, 0.729412f, 1.0f);
const vec4 COLOR_LIGHT_SAGE = Vec4(0.560784f, 0.592157f, 0.290196f, 1.0f);
const vec4 COLOR_YELLOW_BROWN = Vec4(0.541176f, 0.435294f, 0.188235f, 1.0f);