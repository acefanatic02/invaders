//
// Invaders - component.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include "invaders.h"
#include "component.h"
#include "game.h"

void * 
GetComponentFromEntity(GameState * game_state, ComponentTypeID type, EntityHandle entity) {
	assert(entity < MAX_ENTITY_COUNT);
	if (entity == NULL_ENTITY) return NULL;

	Entity * e = game_state->entities + entity;

	for (u32 i = 0; i < e->component_count; ++i) {
		ComponentHandle component = e->components[i];
		if (component.type_id == type) {
			return FetchComponent(&game_state->component_system, component);
		}
	}
	return NULL;
}

u32 
GetComponentsOfTypeFromEntity(GameState * game_state, ComponentTypeID type, EntityHandle entity, void ** out_buffer, u32 out_buffer_count) {
	assert(entity < MAX_ENTITY_COUNT);
	Entity * e = game_state->entities + entity;
	u32 count = 0;
	for (u32 i = 0; i < e->component_count; ++i) {
		ComponentHandle component = e->components[i];
		if (count >= out_buffer_count) {
			break;
		}
		if (component.type_id == type) {
			out_buffer[count] = FetchComponent(&game_state->component_system, component);
			count++;
		}
	}
	return count;
}
static ComponentHandle
FindComponentHandle(GameState * game_state, ComponentTypeID type_id, void * component) {
	ComponentHandle result = {};

	ComponentSystem * system = &game_state->component_system;
#define COMPONENT_CASE(name) \
	{ for (u32 i = 0; i < system->name ## _count; ++i) { if ((void *)(system->name ## s + i) == component) { result.type_id = type_id; result.index = i; } } }
	switch (type_id) {
	case ComponentTypeID_Transform:					COMPONENT_CASE(transform); break;
	case ComponentTypeID_Collider:					COMPONENT_CASE(collider); break;
	case ComponentTypeID_SpriteRenderer:			COMPONENT_CASE(sprite_renderer); break;
	case ComponentTypeID_CircleRenderer:			COMPONENT_CASE(circle_renderer); break;
	case ComponentTypeID_RectangleRenderer:			COMPONENT_CASE(rectangle_renderer); break;
	case ComponentTypeID_TextRenderer:				COMPONENT_CASE(text_renderer); break;
	case ComponentTypeID_ParticleSystem:			COMPONENT_CASE(particle_system); break;
	case ComponentTypeID_AnimatedSpriteRenderer:	COMPONENT_CASE(animated_sprite_renderer); break;
	case ComponentTypeID_Player:					COMPONENT_CASE(player); break;
	case ComponentTypeID_PlayerDebris:				COMPONENT_CASE(player_debris); break;
	case ComponentTypeID_Powerup:					COMPONENT_CASE(powerup); break;
	case ComponentTypeID_Projectile:				COMPONENT_CASE(projectile); break;
	case ComponentTypeID_Invader:					COMPONENT_CASE(invader); break;
	case ComponentTypeID_Saucer:					COMPONENT_CASE(saucer); break;
	case ComponentTypeID_Laser:						COMPONENT_CASE(laser); break;
	default:
	case ComponentTypeID_None:
		assert(!"Invalid component type.");
		break;
	}
#undef COMPONENT_CASE

	return result;
}

static ComponentHandle
AllocComponent(GameState * game_state, ComponentTypeID type_id) {
	ComponentHandle result = {};

	bool allocated = false;
	ComponentSystem * system = &game_state->component_system;
#define COMPONENT_CASE(name) \
	{ if (system->name ## _count < system->name ## _capacity) { \
		result.index = system->name ## _count++; \
		(system->name ## s + result.index)->flags |= ComponentFlag_Alive; \
		allocated = true; } }

	switch (type_id) {
	case ComponentTypeID_Transform:					COMPONENT_CASE(transform); break;
	case ComponentTypeID_Collider:					COMPONENT_CASE(collider); break;
	case ComponentTypeID_SpriteRenderer:			COMPONENT_CASE(sprite_renderer); break;
	case ComponentTypeID_CircleRenderer:			COMPONENT_CASE(circle_renderer); break;
	case ComponentTypeID_RectangleRenderer:			COMPONENT_CASE(rectangle_renderer); break;
	case ComponentTypeID_TextRenderer:				COMPONENT_CASE(text_renderer); break;
	case ComponentTypeID_ParticleSystem:			COMPONENT_CASE(particle_system); break;
	case ComponentTypeID_AnimatedSpriteRenderer:	COMPONENT_CASE(animated_sprite_renderer); break;
	case ComponentTypeID_Player:					COMPONENT_CASE(player); break;
	case ComponentTypeID_PlayerDebris:				COMPONENT_CASE(player_debris); break;
	case ComponentTypeID_Powerup:					COMPONENT_CASE(powerup); break;
	case ComponentTypeID_Projectile:				COMPONENT_CASE(projectile); break;
	case ComponentTypeID_Invader:					COMPONENT_CASE(invader); break;
	case ComponentTypeID_Saucer:					COMPONENT_CASE(saucer); break;
	case ComponentTypeID_Laser:						COMPONENT_CASE(laser); break;
	default:
	case ComponentTypeID_None:
		assert(!"Invalid component type.");
		break;
	}
#undef COMPONENT_CASE

	if (allocated) {
		result.type_id = type_id;
	}

	return result;
}

void * 
AddComponent(GameState * game_state, ComponentTypeID type_id, EntityHandle entity) {
	assert(entity < MAX_ENTITY_COUNT);
	Entity * e = game_state->entities + entity;
	assert(e->component_count < array_count(e->components));

	ComponentHandle handle = AllocComponent(game_state, type_id);
	if (handle.type_id == ComponentTypeID_None) {
		// Failed to allocate this component.
		assert(0);

		return NULL;
	}

	e->components[e->component_count++] = handle;
	BaseComponent * comp = (BaseComponent *)FetchComponent(&game_state->component_system, handle);
	comp->host_entity = entity;
	comp->flags = ComponentFlag_Alive;
	return comp;
}

// TODO(bryan):  Having to pass type_id here is gross.
void 
RemoveComponent(GameState * game_state, ComponentTypeID type_id, void * component) {
	ComponentHandle handle = FindComponentHandle(game_state, type_id, component);

	RemoveComponent(game_state, handle);
}

void 
RemoveComponent(GameState * game_state, ComponentHandle handle) {
	BaseComponent * comp = (BaseComponent *)FetchComponent(&game_state->component_system, handle);

	Entity * e = game_state->entities + comp->host_entity;
	for (u32 i = 0; i < e->component_count; ++i) {
		if (e->components[i] == handle) {
			ComponentHandle last = e->components[(e->component_count) - 1];
			e->components[i] = last;
			e->components[(e->component_count) - 1].type_id = 0;
			e->components[(e->component_count) - 1].index = 0;
			e->component_count--;
			comp->host_entity = NULL_ENTITY;
			comp->flags = 0;
			return;
		}
	}
}

void
ClearDeadComponents(GameState * game_state) {
	ComponentSystem * system = &game_state->component_system;

	// This is ugly as fuck, but a lot easier to expand than writing
	// the loops out by hand for each type.

	// What this macro does:
	// - Iterate over a particular component array.
	// - When a dead component is found:
	//   - Search from back of array to find last live component
	//   - Swap them
	//   - Find the last component's entity's handle to it
	//     - Update the handle to point to the new location.

#define COMPONENT_CASE(type, tid, name) \
    { for (s32 i = 0; i < (s32)system->name ## _count; ++i) { \
            if ((system->name ## s[i].flags & ComponentFlag_Alive) == 0) { \
                s32 last = system->name ## _count - 1;\
                while (last > i) {\
                    if ((system->name ## s[last].flags & ComponentFlag_Alive) != 0) {\
                        break; }\
                    last--; }\
                type tmp = system->name ## s[last];\
                system->name ## s[i] = tmp;\
                memset(system->name ## s + last, 0, sizeof(type));\
                system->name ## _count--;\
                ComponentHandle handle;\
                handle.type_id = tid;\
                handle.index = last;\
                if (tmp.host_entity != NULL_ENTITY) {\
                    Entity * e = game_state->entities + tmp.host_entity;\
                    for (u32 comp = 0; comp < e->component_count; ++comp) {\
                        if (e->components[comp] == handle) {\
                            e->components[comp].index = i; \
                        } } } } } }

	COMPONENT_CASE(TransformComponent, ComponentTypeID_Transform, transform);
	COMPONENT_CASE(ColliderComponent, ComponentTypeID_Collider, collider);
	COMPONENT_CASE(SpriteRendererComponent, ComponentTypeID_SpriteRenderer, sprite_renderer);
	COMPONENT_CASE(CircleRendererComponent, ComponentTypeID_CircleRenderer, circle_renderer);
	COMPONENT_CASE(RectangleRendererComponent, ComponentTypeID_RectangleRenderer, rectangle_renderer);
	COMPONENT_CASE(TextRendererComponent, ComponentTypeID_TextRenderer, text_renderer);
	COMPONENT_CASE(ParticleSystemComponent, ComponentTypeID_ParticleSystem, particle_system);
	COMPONENT_CASE(AnimatedSpriteRendererComponent, ComponentTypeID_AnimatedSpriteRenderer, animated_sprite_renderer);
	COMPONENT_CASE(PlayerComponent, ComponentTypeID_Player, player);
	COMPONENT_CASE(PlayerDebrisComponent, ComponentTypeID_PlayerDebris, player_debris);
	COMPONENT_CASE(PowerupComponent, ComponentTypeID_Powerup, powerup);
	COMPONENT_CASE(ProjectileComponent, ComponentTypeID_Projectile, projectile);
	COMPONENT_CASE(InvaderComponent, ComponentTypeID_Invader, invader);
	COMPONENT_CASE(SaucerComponent, ComponentTypeID_Saucer, saucer);
	COMPONENT_CASE(LaserComponent, ComponentTypeID_Laser, laser);

#undef COMPONENT_CASE
}

void
ClearComponents(GameState * game_state) {
	ComponentSystem * system = &game_state->component_system;

#define COMPONENT_CASE(type, tid, name) \
	{ memset(system->name ## s, 0, sizeof(type) * system->name ## _capacity); system->name ## _count = 0; }

	COMPONENT_CASE(TransformComponent, ComponentTypeID_Transform, transform);
	COMPONENT_CASE(ColliderComponent, ComponentTypeID_Collider, collider);
	COMPONENT_CASE(SpriteRendererComponent, ComponentTypeID_SpriteRenderer, sprite_renderer);
	COMPONENT_CASE(CircleRendererComponent, ComponentTypeID_CircleRenderer, circle_renderer);
	COMPONENT_CASE(RectangleRendererComponent, ComponentTypeID_RectangleRenderer, rectangle_renderer);
	COMPONENT_CASE(TextRendererComponent, ComponentTypeID_TextRenderer, text_renderer);
	COMPONENT_CASE(ParticleSystemComponent, ComponentTypeID_ParticleSystem, particle_system);
	COMPONENT_CASE(AnimatedSpriteRendererComponent, ComponentTypeID_AnimatedSpriteRenderer, animated_sprite_renderer);
	COMPONENT_CASE(PlayerComponent, ComponentTypeID_Player, player);
	COMPONENT_CASE(PlayerDebrisComponent, ComponentTypeID_PlayerDebris, player_debris);
	COMPONENT_CASE(PowerupComponent, ComponentTypeID_Powerup, powerup);
	COMPONENT_CASE(ProjectileComponent, ComponentTypeID_Projectile, projectile);
	COMPONENT_CASE(InvaderComponent, ComponentTypeID_Invader, invader);
	COMPONENT_CASE(SaucerComponent, ComponentTypeID_Saucer, saucer);
	COMPONENT_CASE(LaserComponent, ComponentTypeID_Laser, laser);

#undef COMPONENT_CASE
}

void 
ClearEntityComponents(GameState *game_state, Entity * entity) {
	ComponentSystem * system = &game_state->component_system;
	for (u32 i = 0; i < entity->component_count; ++i) {
		BaseComponent * bc = (BaseComponent *)FetchComponent(system, entity->components[i]);
		entity->components[i].type_id = 0;
		entity->components[i].index = 0;
		bc->flags = 0;
		bc->host_entity = NULL_ENTITY;
	}
	entity->component_count = 0;
}
