//
// Invaders - component.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"
#include "render_command.h"

typedef u32 EntityHandle;
const EntityHandle NULL_ENTITY = 0xffffffff;

enum ComponentTypeID {
	ComponentTypeID_None = 0,
	ComponentTypeID_Transform,
	ComponentTypeID_Collider,
	ComponentTypeID_SpriteRenderer,
	ComponentTypeID_CircleRenderer,
	ComponentTypeID_RectangleRenderer,
	ComponentTypeID_TextRenderer,
	ComponentTypeID_ParticleSystem,
	ComponentTypeID_AnimatedSpriteRenderer,

	ComponentTypeID_Player,
	ComponentTypeID_PlayerDebris,
	ComponentTypeID_Powerup,
	ComponentTypeID_Projectile,
	ComponentTypeID_Invader,
	ComponentTypeID_Saucer,
	ComponentTypeID_Laser,

	ComponentTypeID_Count,
};

static_assert(ComponentTypeID_Count < (1 << 16), "Too many component IDs to pack into 16 bits. (Max: 65535)");

enum EntityType {
	EntityType_None = 0,
	EntityType_Player,
	EntityType_Invader,
	EntityType_Saucer,
	EntityType_Powerup,
	EntityType_Projectile,
	EntityType_Laser,
	EntityType_LaserHit,
	EntityType_TextEntity,
	EntityType_ParticleSystem,
	EntityType_PlayerDebris,
	EntityType_SaucerGlow,

	EntityType_Count,
};

enum EntityFlags {
	EF_Alive    = 1 << 0,
	EF_Collides = 1 << 1,
};

enum ComponentFlags {
	ComponentFlag_Alive = 1 << 0,
};

enum ColliderType {
	CT_Circle,
	CT_Rect,
};

struct CircleCollider {
	float radius;
};

struct RectCollider {
	vec2 half_size;
};

struct BaseComponent {
	EntityHandle host_entity;
	u32 flags;
};

struct ColliderComponent : BaseComponent {
	ColliderType type;
	union {
		CircleCollider circle;
		RectCollider rect;
	};
};

struct Sprite;

struct TransformComponent : BaseComponent  {
	vec2 position;
	vec2 scale;
	vec2 rotation;
};

struct PlayerDebrisComponent : BaseComponent {
	vec2 velocity;
	float time_to_live;
};

enum PowerupType {
	PowerupType_None   = 0,
	PowerupType_Shield = 1 << 0,
	PowerupType_Health = 1 << 1,
};

struct PowerupComponent : BaseComponent {
	PowerupType type;
};

struct ProjectileComponent : BaseComponent {
	EntityType owner;
	vec2 velocity;
};

struct LaserComponent : BaseComponent {
	float time_to_live;
};

struct PlayerComponent : BaseComponent {
	vec2 velocity;
	s32 health;
	u32 fire_state;
	u32 active_powerups;
	float shield_fade_time;
	bool hit_this_frame;
};

struct InvaderComponent : BaseComponent {
	float fire_timeout_remaining;	// Time between shots
	float fire_wait_remaining;		// Time a shot can be held before the invader *must* fire
	PowerupType drop_type;
};

struct SaucerComponent : BaseComponent {
	u32 health_remaining;
	float laser_fire_timeout;
	float laser_charge_wait;
	float laser_fire_wait;

	float move_moving_time;
	float move_wait_timeout;
	float move_target_x;
	float move_last_x;

	vec2 target;
	EntityHandle glow_entity;
};

struct SpriteRendererComponent : BaseComponent {
	Sprite * sprite;
	vec4 color;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
};

struct CircleRendererComponent : BaseComponent {
	float outer_radius;
	float inner_radius;
	vec4 color;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
};

struct RectangleRendererComponent : BaseComponent {
	vec2 size;
	vec4 color;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
};

struct TextRendererComponent : BaseComponent {
	char string[64];
	float time_to_live;
	vec4 color;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
};

#define PARTICLE_COUNT (64)
struct ParticleSystemComponent : BaseComponent {
	// Positions and angles are relative to the 
	// system's transform
	float pos_x[PARTICLE_COUNT];
	float pos_y[PARTICLE_COUNT];
	float vel_x[PARTICLE_COUNT];
	float vel_y[PARTICLE_COUNT];
	float angle[PARTICLE_COUNT];
	float rotation[PARTICLE_COUNT];
	float time_to_live[PARTICLE_COUNT];

	vec2 initial_force;
	float seconds_to_live;
	u32 live_count;
	Sprite * sprite;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
};

struct AnimatedSpriteRendererComponent : BaseComponent {
	Sprite * sprite_array;
	u32 sprite_count;
	float frame_time;
	float cur_time;
	vec4 color;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
};

#pragma pack(push, 1)
struct ComponentHandle {
	u16 type_id;
	u16 index;
};
#pragma pack(pop)

inline bool
operator==(ComponentHandle a, ComponentHandle b) {
	return (a.type_id == b.type_id) && (a.index == b.index);
}

struct Entity {
	EntityType type;
	u32 flags;
	ComponentHandle components[64];
	u32 component_count;
};

#define DYNAMIC_ARRAY(type, name) \
    (type) * name ## s; \
    u32 name ## _count; \
	u32 name ## _capacity;

struct ComponentSystem {
	DYNAMIC_ARRAY(TransformComponent, transform);
	DYNAMIC_ARRAY(ColliderComponent, collider);
	DYNAMIC_ARRAY(SpriteRendererComponent, sprite_renderer);
	DYNAMIC_ARRAY(CircleRendererComponent, circle_renderer);
	DYNAMIC_ARRAY(RectangleRendererComponent, rectangle_renderer);
	DYNAMIC_ARRAY(TextRendererComponent, text_renderer);
	DYNAMIC_ARRAY(ParticleSystemComponent, particle_system);
	DYNAMIC_ARRAY(AnimatedSpriteRendererComponent, animated_sprite_renderer);
	DYNAMIC_ARRAY(PlayerComponent, player);
	DYNAMIC_ARRAY(PlayerDebrisComponent, player_debris);
	DYNAMIC_ARRAY(PowerupComponent, powerup);
	DYNAMIC_ARRAY(ProjectileComponent, projectile);
	DYNAMIC_ARRAY(InvaderComponent, invader);
	DYNAMIC_ARRAY(SaucerComponent, saucer);
	DYNAMIC_ARRAY(LaserComponent, laser);
};

// Defining this unfucks Intellisense's idea of what ComponentSystem actually contains.
static_assert(sizeof(ComponentSystem) != 0, "");

//#define GetComponent(system, type, handle) (type *)FetchComponent(system, handle)
inline void * 
FetchComponent(ComponentSystem * system, ComponentHandle handle) {
#define COMPONENT_CASE(name) \
	{ assert(handle.index < system->name##_count); \
	return (void *)(system->name##s + handle.index); }

	switch (handle.type_id) {
	case ComponentTypeID_Transform:					COMPONENT_CASE(transform); break;
	case ComponentTypeID_Collider:					COMPONENT_CASE(collider); break;
	case ComponentTypeID_SpriteRenderer:			COMPONENT_CASE(sprite_renderer); break;
	case ComponentTypeID_CircleRenderer:			COMPONENT_CASE(circle_renderer); break;
	case ComponentTypeID_RectangleRenderer:			COMPONENT_CASE(rectangle_renderer); break;
	case ComponentTypeID_TextRenderer:				COMPONENT_CASE(text_renderer); break;
	case ComponentTypeID_ParticleSystem:			COMPONENT_CASE(particle_system); break;
	case ComponentTypeID_AnimatedSpriteRenderer:	COMPONENT_CASE(animated_sprite_renderer); break;
	case ComponentTypeID_Player:					COMPONENT_CASE(player); break;
	case ComponentTypeID_PlayerDebris:				COMPONENT_CASE(player_debris); break;
	case ComponentTypeID_Powerup:					COMPONENT_CASE(powerup); break;
	case ComponentTypeID_Projectile:				COMPONENT_CASE(projectile); break;
	case ComponentTypeID_Invader:					COMPONENT_CASE(invader); break;
	case ComponentTypeID_Saucer:					COMPONENT_CASE(saucer); break;
	case ComponentTypeID_Laser:						COMPONENT_CASE(laser); break;
	default:
	case ComponentTypeID_None:
		assert(!"Invalid component type.");
		break;
	}

#undef COMPONENT_CASE
	return NULL;
}

struct GameState;

void * GetComponentFromEntity(GameState * game_state, ComponentTypeID type, EntityHandle entity);

u32 GetComponentsOfTypeFromEntity(GameState * game_state, ComponentTypeID type, EntityHandle entity, void ** out_buffer, u32 out_buffer_count);

void * AddComponent(GameState * game_state, ComponentTypeID type_id, EntityHandle entity);

// TODO(bryan):  Having to pass type_id here is gross.
void RemoveComponent(GameState * game_state, ComponentTypeID type_id, void * component);
void RemoveComponent(GameState * game_state, ComponentHandle handle);

void ClearDeadComponents(GameState * game_state);
void ClearComponents(GameState * game_state);
void ClearEntityComponents(GameState *game_state, Entity * entity);
