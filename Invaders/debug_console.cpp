//
// Invaders - debug_console.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include "debug_console.h"
#include <string>

const u32 MAX_LINE_LENGTH = 512;
const u32 MAX_LINE_COUNT = 512;
const u32 DISPLAY_LINE_COUNT = 15;

struct DebugConsoleState {
	bool has_focus;
	bool active;
	char backlog[MAX_LINE_COUNT][MAX_LINE_LENGTH];
	char input_line[MAX_LINE_LENGTH];
	u32 input_line_length;
	u32 backlog_line_count;
	u32 scrollback_line_pos;
};

static DebugConsoleState gConsoleState;

bool 
DebugConsoleHasFocus() {
	return gConsoleState.active && gConsoleState.has_focus;
}

void
DebugConsoleOutput(char * string) {
	for (s32 i = MAX_LINE_COUNT - 1; i >= 0; --i) {
		strncpy(gConsoleState.backlog[i + 1], gConsoleState.backlog[i], MAX_LINE_LENGTH);
	}

	strncpy(gConsoleState.backlog[0], string, MAX_LINE_LENGTH);
	gConsoleState.backlog_line_count++;
}

typedef void DebugCommandFunc(void * data);

struct DebugCommandParamData {
	char strings[64][100];
	u32 param_count;
	GameState * game_state;
};

template <typename T>
static T GetParam(void * param_data, u32 index);

template <>
static char * GetParam(void * param_data, u32 index) {
	DebugCommandParamData * param_struct = (DebugCommandParamData *)param_data;
	char * param = param_struct->strings[index];
	return param;
}

template <>
static s32 GetParam(void * param_data, u32 index) {
	DebugCommandParamData * param_struct = (DebugCommandParamData *)param_data;
	char * param = param_struct->strings[index];
	char * out = NULL;
	s64 result = strtol(param, &out, 0);
	return (s32)result;
}

template <>
static u32 GetParam(void * param_data, u32 index) {
	DebugCommandParamData * param_struct = (DebugCommandParamData *)param_data;
	char * param = param_struct->strings[index];
	char * out = NULL;
	u64 result = strtoul(param, &out, 0);
	return (u32)result;
}

template <>
static u64 GetParam(void * param_data, u32 index) {
	DebugCommandParamData * param_struct = (DebugCommandParamData *)param_data;
	char * param = param_struct->strings[index];
	char * out = NULL;
	u64 result = strtoul(param, &out, 0);
	return result;
}

struct DebugCommand {
	char * cmd_string;
	u32 param_count;
	DebugCommandFunc * func;	
};

static void 
DebugCommandTest(void * data) {
	u32 param_int = GetParam<u32>(data, 0);
	char * param_string = GetParam<char *>(data, 1);
	char buffer[512];
	_snprintf(buffer, array_count(buffer), "This is a test.  Param: %d, %s", param_int, param_string); 
	DebugConsoleOutput(buffer);
}

static void
DebugCommandShowItem(void * data) {
#if 0
	char * item_name = GetParam<char *>(data, 0);
	GameState * game_state = ((DebugCommandParamData *)data)->game_state;

	u32 item_name_len = strnlen(item_name, 64); 
#define CMP_TOKEN(token, name, token_len) ((token_len == array_count(name) - 1) && (!strncmp(token, name, array_count(name) - 1)))
	if (CMP_TOKEN(item_name, "entity_count", item_name_len)) {
		char buffer[512];
		_snprintf(buffer, sizeof(buffer), "entity_count=%u", game_state->entity_count);
		DebugConsoleOutput(buffer);
	}
	else if (CMP_TOKEN(item_name, "entities", item_name_len)) {
		for (u32 i = 0; i < game_state->entity_count; ++i) {
			char buffer[512];
			char * type_str = "(NULL)";
			switch(game_state->entities[i].type) {
			case EntityType_Player:
				type_str = "Player";
				break;
			case EntityType_Invader:
				type_str = "Invader";
				break;
			case EntityType_Powerup:
				type_str = "Powerup";
				break;
			case EntityType_Projectile:
				type_str = "Projectile";
				break;
			}
			_snprintf(buffer, sizeof(buffer), "[%03u] Entity<type=%s>", i, type_str);
			DebugConsoleOutput(buffer);
		}
	}
#undef CMP_TOKEN

#endif
}

static void
DebugCommandEntityInfo(void * data) {
	u32 idx = GetParam<u32>(data, 0);
	GameState * game_state = ((DebugCommandParamData *)data)->game_state;
	if (idx >= MAX_ENTITY_COUNT) {
		DebugConsoleOutput("Entity not active.");
	}

	Entity * e = &game_state->entities[idx];
	switch (e->type) {
	case EntityType_Player:
		DebugConsoleOutput("Player entity");
		break;
	case EntityType_Invader:
		break;
	}

	if (e->flags & EF_Alive) {
		DebugConsoleOutput("Flags = EF_Alive");
	}
}

static void
DebugCommandSetLevel(void * data) {
	u32 new_level = GetParam<u32>(data, 0);
	GameState * game_state = ((DebugCommandParamData *)data)->game_state;
	game_state->level = new_level;
}

static void
DebugCommandGive(void * data) {
	char * item_name = GetParam<char *>(data, 0);
	GameState * game_state = ((DebugCommandParamData *)data)->game_state;
#if 0
	u32 item_name_len = strnlen(item_name, 64); 
#define CMP_TOKEN(token, name, token_len) ((token_len == array_count(name) - 1) && (!strncmp(token, name, array_count(name) - 1)))
	if (CMP_TOKEN(item_name, "shield", item_name_len)) {
		game_state->entities[game_state->player_entity].entity.player.active_powerups |= PowerupType_Shield;
	}
	if (CMP_TOKEN(item_name, "health", item_name_len)) {
		game_state->entities[game_state->player_entity].entity.player.health++;
	}
	if (CMP_TOKEN(item_name, "damage", item_name_len)) {
		game_state->entities[game_state->player_entity].entity.player.health--;
	}
#undef CMP_TOKEN

#endif
}

static void
DebugCommandShowStats(void * data) {
	GameState * game_state = ((DebugCommandParamData *)data)->game_state;
	char buffer[512] = {};
	_snprintf_s(buffer, array_count(buffer) - 1, "Kills: %u", game_state->player_stats.kills);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Shots fired: %u", game_state->player_stats.shots_fired);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Shots hit: %u", game_state->player_stats.shots_hit);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Best streak: %u", game_state->player_stats.best_streak);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Best streak: %u", game_state->player_stats.best_streak);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Damage taken: %u", game_state->player_stats.damage_taken);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Health picked up: %u", game_state->player_stats.health_picked_up);
	DebugConsoleOutput(buffer);
	_snprintf_s(buffer, array_count(buffer) - 1, "Shields picked up: %u", game_state->player_stats.shields_picked_up);
	DebugConsoleOutput(buffer);
}

static void
DebugCommandListCommands(void * data);

DebugCommand commands[] = {
	{ "test", 2, DebugCommandTest },
	{ "show-item", 1, DebugCommandShowItem },
	{ "give", 1, DebugCommandGive },
	{ "entity-info", 1, DebugCommandEntityInfo },
	{ "set-level", 1, DebugCommandSetLevel },
	{ "list-cmd", 0, DebugCommandListCommands },
	{ "show-stats", 0, DebugCommandShowStats },
};

static void
DebugCommandListCommands(void * data) {
	for (u32 i = 0; i < array_count(commands); ++i) {
		DebugConsoleOutput(commands[i].cmd_string);
	}
}

void 
ToggleDebugConsole() {
	gConsoleState.active = !gConsoleState.active;
	memset(gConsoleState.input_line, 0, sizeof(gConsoleState.input_line));
}

void 
SetDebugFocus(bool has_focus) {
	gConsoleState.has_focus = has_focus;
}

inline bool iswhite(char ch) {
	return ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n';
}

static bool
ReadToken(char ** token_start, char ** token_end, char ** line_cur, char * line_end) {
	while (*line_cur < line_end && iswhite(**line_cur)) {
		(*line_cur)++;
	}
	if (*line_cur >= line_end) {
		return false;
	}

	*token_start = *line_cur;
	while (*line_cur < line_end && !iswhite(**line_cur)) {
		(*line_cur)++;
	}
	*token_end = *line_cur;

	return *token_start != *token_end;
}

static void
ProcessCommand(GameState * game_state) {
	char * line_start = gConsoleState.input_line;
	char * line_end = gConsoleState.input_line;
	for (u32 i = 0; *line_end && i < MAX_LINE_LENGTH; ++i) ++line_end;

	char * cmd_start = NULL;
	char * cmd_end = NULL;
	char * line_cur = line_start;
	if (ReadToken(&cmd_start, &cmd_end, &line_cur, line_end)) {
		assert(line_cur != line_start);
		for (u32 i = 0; i < array_count(commands); ++i) {
			if (!strncmp(cmd_start, commands[i].cmd_string, cmd_end - cmd_start)) {
				DebugCommandParamData params = {};
				params.param_count = commands[i].param_count;
				params.game_state = game_state;
				if (commands[i].param_count > 0) {
					for (u32 p = 0; p < commands[i].param_count; ++p) {
						char * param_start = NULL;
						char * param_end = NULL;
						if (ReadToken(&param_start, &param_end, &line_cur, line_end)) {
							strncpy(params.strings[p], param_start, param_end - param_start);
						}
						else {
							DebugConsoleOutput("Insufficient parameters.");
							return;
						}
					}
				}
				commands[i].func((void *)&params);
				return;
			}
		}

		DebugConsoleOutput("Unrecognized command");
	}
}

// TODO: Define VK codes ourselves
#include <Windows.h>
static void
DebugConsoleProcessKeyState(GameState * game_state, KeyboardState * new_key_state, KeyboardState * prev_key_state) {
#define WENT_DOWN(c) ((new_key_state->keys[c] & 0x80) && !(prev_key_state->keys[c] & 0x80) != 0)
	
	bool shift_is_down = ((new_key_state->keys[VK_SHIFT] & 0x80) != 0);

	if (WENT_DOWN(VK_SPACE)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = ' ';
		}
	}
	else if (WENT_DOWN(VK_BACK)) {
		if (gConsoleState.input_line_length > 0) {
			gConsoleState.input_line[--gConsoleState.input_line_length] = 0;
		}
	}
	
	if (WENT_DOWN(VK_OEM_PLUS)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? '+' : '=';
		}
	}
	if (WENT_DOWN(VK_OEM_MINUS)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? '_' : '-';
		}
	}
	if (WENT_DOWN(VK_OEM_COMMA)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? '<' : ',';
		}
	}
	if (WENT_DOWN(VK_OEM_PERIOD)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? '>' : '.';
		}
	}
	if (WENT_DOWN(VK_OEM_2)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? '?' : '/';
		}
	}
	if (WENT_DOWN(VK_OEM_7)) {
		if (gConsoleState.input_line_length < MAX_LINE_LENGTH) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? '\"' : '\'';
		}
	}

	for (u32 c = 0x30; c < 0x5B; ++c) {
		if (gConsoleState.input_line_length >= MAX_LINE_LENGTH) break;

		if (WENT_DOWN(c)) {
			gConsoleState.input_line[gConsoleState.input_line_length++] = shift_is_down ? (u8)c : (u8)c | 0x20;
		}
	}

	if (WENT_DOWN(VK_DOWN)) {
		if (gConsoleState.scrollback_line_pos > 0) {
			gConsoleState.scrollback_line_pos--;
		}
	}
	if (WENT_DOWN(VK_UP)) {
		if (gConsoleState.scrollback_line_pos < gConsoleState.backlog_line_count - 1) {
			gConsoleState.scrollback_line_pos++;
		}
	}

	if (WENT_DOWN(VK_RETURN)) {
		ProcessCommand(game_state);

		memset(gConsoleState.input_line, 0, sizeof(gConsoleState.input_line));
		gConsoleState.input_line_length = 0;
		gConsoleState.scrollback_line_pos = 0;
	}

#undef WENT_DOWN
}


void 
UpdateDebugConsole(RenderCommandBuffer * render_buffer, KeyboardState * new_key_state, KeyboardState * prev_key_state, GameState * game_state) {
	if (!gConsoleState.active) return;

	if (DebugConsoleHasFocus()) {
		DebugConsoleProcessKeyState(game_state, new_key_state, prev_key_state);
	}

	float font_scale = 2.0f;
	vec2 font_scale_vec = Vec2(font_scale, font_scale);
	float line_height = 12.0f * font_scale;
	vec2 console_top_left = Vec2(-0.5f * gScreenWidth, 0.5f * gScreenHeight);
	vec2 console_bottom_right = console_top_left + Vec2((float)gScreenWidth, -line_height * ((float)DISPLAY_LINE_COUNT + 1.5f));

	float panel_alpha = gConsoleState.has_focus ? 0.85f : 0.25f;
	RenderRectangle(render_buffer, console_top_left, console_bottom_right, Vec4(0.0f, 0.075f, 0.05f, panel_alpha), RenderLayer_UI);
	
	float cur_x = console_top_left.x + 12.0f;
	float cur_y = console_bottom_right.y + line_height;

	// Input line
	vec2 input_box_min = Vec2(cur_x, cur_y) - Vec2(5.0f, -5.0f);
	vec2 input_box_max = input_box_min + Vec2((float)gScreenWidth - 12.0f, -line_height);
	RenderRectangle(render_buffer, input_box_min, input_box_max, Vec4(0.0f, 0.0f, 0.0f, 0.5f * panel_alpha), RenderLayer_UI);
	
	RenderText(render_buffer, Vec2(cur_x, cur_y), ">> ", font_scale_vec, Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer_UI);
	RenderText(render_buffer, Vec2(cur_x + 32.0f, cur_y), gConsoleState.input_line, font_scale_vec, Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer_UI);

	cur_y += line_height;

	u32 first_scrollback_line = gConsoleState.scrollback_line_pos;
	u32 last_scrollback_line = min(first_scrollback_line + DISPLAY_LINE_COUNT, gConsoleState.backlog_line_count);
	for (u32 i = first_scrollback_line; i < last_scrollback_line; ++i) {
		RenderText(render_buffer, Vec2(cur_x, cur_y), gConsoleState.backlog[i], font_scale_vec, Vec4(0.70f, 0.70f, 0.70f, 1.0f), RenderLayer_UI);
		cur_y += line_height;
	}
}