//
// Invaders - debug_console.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once
#include "invaders.h"
#include "game.h"
#include "render_command.h"
#include "sound_command.h"

#define DEBUG_CONSOLE 0

bool DebugConsoleHasFocus();
void ToggleDebugConsole();
void SetDebugFocus(bool has_focus);
void UpdateDebugConsole(RenderCommandBuffer * render_buffer, KeyboardState * new_key_state, KeyboardState * prev_key_state, GameState * game_state);