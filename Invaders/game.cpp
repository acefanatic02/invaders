//
// Invaders - game.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include "invaders.h"

#include "game.h"

#include "render_command.h"
#include "sound_command.h"
#include "rectangle.h"
#include "loader.h"
#include "text_util.h"
#include "colors.h"
#include "random.h"
#include "component.h"

#include "mat4x4.h"

#include <cstdio>
#include <cstring>

const float PLAYER_SHIELD_FADE_SECONDS = 0.25f;

inline bool
ControllerIsLive(GameInput * input, u32 index) {
	// A controller is "live" when it is attached and allowed to send
	// input to the game (active).
	// A controller that is still active but unplugged is *not* 
	// considered live.
	assert(index < ControllerIndex_Count && "Invalid controller index.");

	u32 controller_flags = input->controllers[index].flags;
	return ((controller_flags & ControllerFlag_IsActive) && (controller_flags & ControllerFlag_IsAttached));
}

static float
InputAxis(GameInput * input, GameVirtualInputs input_code) {
	// Check gamepads first, then keyboard, becuase keyboard is *always* live.
	for (u32 i = ControllerIndex_Gamepad0; i < ControllerIndex_Count; ++i) {
		if (ControllerIsLive(input, i)) {
			if ((input->controllers[i].input_states[input_code].flags & InputStateFlag_IsAxis) == 0) {
				assert(!"Cannot read button as axis.");
			}

			return input->controllers[i].input_states[input_code].axis.value;
		}
	}
	return input->controllers[ControllerIndex_Keyboard].input_states[input_code].axis.value;
}

static bool
InputButtonWentDown(GameInput * input, GameVirtualInputs input_code) {
	for (u32 i = 0; i < ControllerIndex_Count; ++i) {
		if (ControllerIsLive(input, i)) {
			if ((input->controllers[i].input_states[input_code].flags & InputStateFlag_IsAxis) != 0) {
				assert(!"Cannot read axis as button.");
			}

			if (input->controllers[i].input_states[input_code].button.is_down &&
				!input->controllers[i].input_states[input_code].button.was_down) {
					return true;
			}
		}
	}
	return false;
}

static bool
InputButtonIsDown(GameInput * input, GameVirtualInputs input_code) {
	for (u32 i = 0; i < ControllerIndex_Count; ++i) {
		if (ControllerIsLive(input, i)) {
			if (input->controllers[i].input_states[input_code].button.is_down) {
					return true;
			}
		}
	}
	return false;
}

Entity * GetEntity(GameState * game_state, EntityHandle handle);

inline TransformComponent * 
GetTransformFromEntity(GameState * game_state, EntityHandle entity) {
	void * transform = GetComponentFromEntity(game_state, ComponentTypeID_Transform, entity);
	return (TransformComponent *)transform;
}

inline bool Collides(GameState * game_state, Entity * a, Entity * b);
bool Collides(vec2 c_a, float r_a, vec2 c_b, float r_b);
bool Collides(vec2 min_a, vec2 max_a, vec2 min_b, vec2 max_b);

bool 
Collides(vec2 c_a, float r_a, vec2 c_b, float r_b) {
	vec2 dc = c_b - c_a;
	float r_sqr = (r_a + r_b) * (r_a + r_b);
	if (dot(dc, dc) <= r_sqr) {
		return true;
	}
	return false;
}

bool 
Collides(vec2 min_a, vec2 max_a, vec2 min_b, vec2 max_b) {
	if ((max_b.x <= min_a.x || max_a.x <= min_b.x) || (max_b.y <= min_a.y || max_a.y <= min_b.y)) {
		return false;
	}
	return true;
}

inline bool 
Collides(GameState * game_state, EntityHandle a, EntityHandle b) {
	ColliderComponent * col_a = (ColliderComponent * )GetComponentFromEntity(game_state, ComponentTypeID_Collider, a);
	ColliderComponent * col_b = (ColliderComponent * )GetComponentFromEntity(game_state, ComponentTypeID_Collider, b);

	if (col_a->type == col_b->type) {
		TransformComponent * trans_a = GetTransformFromEntity(game_state, a);
		TransformComponent * trans_b = GetTransformFromEntity(game_state, b);
		if (col_a->type == CT_Circle) {
			return Collides(trans_a->position, col_a->circle.radius, trans_b->position, col_b->circle.radius);
		}
		else if (col_a->type == CT_Rect) {
			return Collides(trans_a->position - col_a->rect.half_size, trans_a->position + col_a->rect.half_size,
							trans_b->position - col_b->rect.half_size, trans_b->position + col_b->rect.half_size);
		}
		else {
			assert(0);
			return false;
		}
	}
	else {
		// TODO: handle circle/rect collisions
		assert(0);
		return false;
	}
}

CollisionRule *
GetCollisionRule(GameState * game_state, EntityType a, EntityType b) {
	for (u32 i = 0; i < game_state->collision_table_count; ++i) {
		CollisionRule * rule = game_state->collision_table + i;
		if (rule->a == a && rule->b == b) {
			return rule;
		}
		if (rule->a == b && rule->b == a) {
			return rule;
		}
	}

	return NULL;
}

void
AddCollisionRule(GameState * game_state, EntityType a, EntityType b, CollisionRuleHandler * handler) {
	assert(game_state->collision_table_count < array_count(game_state->collision_table));

	if (GetCollisionRule(game_state, a, b)) {
		assert(0);
	}

	CollisionRule * rule = &game_state->collision_table[game_state->collision_table_count++];
	rule->a = a;
	rule->b = b;
	rule->handler = handler;
}

static u32
GatherEntitiesWithFlag(GameState * game_state, u32 flag_mask, EntityHandle * out_buffer, u32 out_buffer_count) {
	u32 count = 0;
	for (u32 i = 0; i < MAX_ENTITY_COUNT; ++i) {
		Entity * e = GetEntity(game_state, i);
		if (count >= out_buffer_count) {
			break;
		}
		if ((e->flags & flag_mask) == flag_mask) {
			out_buffer[count] = i;
			count++;
		}
	}
	return count;
}

#define CELL_ENTITY_COUNT 5

struct QuadTree {
	vec2 min;
	vec2 max;
	u32 entity_count;
	EntityHandle entities[CELL_ENTITY_COUNT];
	QuadTree * cells[4];
};

// Cells are numbered by quadrant:
//     |
// II  |  I
//-----+------
// III | IV
//     |
//
enum Quadrant {
	Quadrant_I   = 1 << 0,
	Quadrant_II  = 1 << 1,
	Quadrant_III = 1 << 2,
	Quadrant_IV  = 1 << 3,
};

inline u32
GetQuadrant(QuadTree * node, vec2 e_min, vec2 e_max) {
	float node_v_split = 0.5f * (node->max.x + node->min.x);
	float node_h_split = 0.5f * (node->max.y + node->min.y);

	u32 result = 0;
	if (e_max.x >= node_v_split) {
		if (e_max.y >= node_h_split) result |= Quadrant_I;
		if (e_min.y <= node_h_split) result |= Quadrant_IV;
	}
	if (e_min.x <= node_v_split) {
		if (e_max.y >= node_h_split) result |= Quadrant_II;
		if (e_min.y <= node_h_split) result |= Quadrant_III;
	}
	return result;
}

inline u32
GetQuadrant(GameState * game_state, QuadTree * node, EntityHandle handle) {
	TransformComponent * transform = GetTransformFromEntity(game_state, handle);
	ColliderComponent * collider = (ColliderComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Collider, handle);

	float r = collider->circle.radius;
	vec2 e_min = transform->position - Vec2(r, r);
	vec2 e_max = transform->position + Vec2(r, r);
	
	return GetQuadrant(node, e_min, e_max);
}

// TODO(bryan):  Make this iterative, rather than recursive.
static void
AddToQuadTree(GameState * game_state, QuadTree * root, EntityHandle handle, MemoryArena * arena) {
	QuadTree * node = root;
	if (node->entity_count < CELL_ENTITY_COUNT) {
		// Add to this node
		node->entities[node->entity_count++] = handle;
		return;
	}
	else {
		if (node->cells[0] == NULL) {
			// Split this node
			QuadTree * new_cells = (QuadTree *)arena->AllocateZeroed(sizeof(QuadTree) * 4);

			float min_x = node->min.x;
			float max_x = node->max.x;
			float split_x = 0.5f * (max_x + min_x);

			float min_y = node->min.y;
			float max_y = node->max.y;
			float split_y = 0.5f * (max_y + min_y);

			// TODO(bryan):  ENGINE  FIXME
			// If we hit these asserts, we're recursing infintely.
			// This can happen when CELL_ENTITY_COUNT colliders overlap
			// almost exactly.  The tree splits, distributes all the 
			// entities to all four cells, which then have to split, etc.
			//
			// NOTE(bryan):  This issue can also occur if a group of 
			// entities exist outside the bounds of the root quad.
			// 
			// Invaders will not (can not?) hit this behavior with a 
			// CELL_ENTITY_COUNT > 3.  However, if we reuse this code
			// we need to come up with a strategy for handling this 
			// gracefully.  (Simply aborting this fork of the tree is
			// likely the easiest option.)
			assert(min_x < max_x);
			assert(min_y < max_y);

			node->cells[0] = new_cells + 0;
			node->cells[0]->max = Vec2(max_x, max_y);
			node->cells[0]->min = Vec2(split_x, split_y);
			
			node->cells[1] = new_cells + 1;
			node->cells[1]->max = Vec2(split_x, max_y);
			node->cells[1]->min = Vec2(min_x, split_y);
			
			node->cells[2] = new_cells + 2;
			node->cells[2]->max = Vec2(split_x, split_y);
			node->cells[2]->min = Vec2(min_x, min_y);
			
			node->cells[3] = new_cells + 3;
			node->cells[3]->max = Vec2(max_x, split_y);
			node->cells[3]->min = Vec2(split_x, min_y);

			// Distribute existing entities to new cells
			for (u32 i = 0; i < node->entity_count; ++i) {
				u32 quadrant = GetQuadrant(game_state, node, node->entities[i]);
				if (quadrant & Quadrant_I) {
					QuadTree * cell = node->cells[0];
					cell->entities[cell->entity_count++] = node->entities[i];
				}
				if (quadrant & Quadrant_II) {
					QuadTree * cell = node->cells[1];
					cell->entities[cell->entity_count++] = node->entities[i];
				}
				if (quadrant & Quadrant_III) {
					QuadTree * cell = node->cells[2];
					cell->entities[cell->entity_count++] = node->entities[i];
				}
				if (quadrant & Quadrant_IV) {
					QuadTree * cell = node->cells[3];
					cell->entities[cell->entity_count++] = node->entities[i];
				}
				node->entities[i] = NULL_ENTITY;
			}
		}
		
		// Walk cells
		u32 quadrant = GetQuadrant(game_state, node, handle);
		if (quadrant & Quadrant_I) {
			AddToQuadTree(game_state, node->cells[0], handle, arena);
		}
		if (quadrant & Quadrant_II) {
			AddToQuadTree(game_state, node->cells[1], handle, arena);
		}
		if (quadrant & Quadrant_III) {
			AddToQuadTree(game_state, node->cells[2], handle, arena);
		}
		if (quadrant & Quadrant_IV) {
			AddToQuadTree(game_state, node->cells[3], handle, arena);
		}
	}
}

u32
QueryQuadTree(GameState * game_state, QuadTree * root, EntityHandle e, Rect aabb, EntityHandle * out_buffer, u32 out_buffer_count) {
	u32 used = 0;

	QuadTree * stack[MAX_ENTITY_COUNT / 2] = {};
	u32 stack_top = 0;
	Rect root_bounds = RectFromMinMax(root->min, root->max);
	if (!RectInsideRect(root_bounds, aabb)) {
		// We're querying outside the tree, we know there's nothing there.
		return 0;
	}

	QuadTree * node = root;
	while (node) {
		if (node->cells[0] == NULL) {
			// Leaf node
			for (u32 i = 0; i < node->entity_count; ++i) {
				if (used >= out_buffer_count) {
					// Out of space, no point continuing to walk the tree.
					stack_top = 0;
					break;
				}

				if (node->entities[i] != e) {
					out_buffer[used++] = node->entities[i];
				}
			}
		}
		else {
			// Push quadrants we overlap to stack
			u32 quadrant = GetQuadrant(node, GetRectMin(aabb), GetRectMax(aabb));
			if (quadrant & Quadrant_I) {
				stack[stack_top++] = node->cells[0];
			}
			if (quadrant & Quadrant_II) {
				stack[stack_top++] = node->cells[1];
			}
			if (quadrant & Quadrant_III) {
				stack[stack_top++] = node->cells[2];
			}
			if (quadrant & Quadrant_IV) {
				stack[stack_top++] = node->cells[3];
			}
		}
		if (stack_top != 0) {
			// Pop stack
			node = stack[--stack_top];
		}
		else {
			break;
		}
	}

	// Eliminate duplicates
	for (u32 i = 0; i < used; ++i) {
		for (u32 j = i + 1; j < used; ++j) {
			if (out_buffer[i] == out_buffer[j]) { 
				out_buffer[j] = out_buffer[--used];
			}
		}
	}

	return used;
}

void 
DrawQuadTree(QuadTree * root, RenderCommandBuffer * render_buffer) {
	RenderOutlineRect(render_buffer, root->min, root->max, COLOR_LIGHT_BLUE, 1.0f, RenderLayer_UI);
	if (root->cells[0] != NULL) {
		DrawQuadTree(root->cells[0], render_buffer);
		DrawQuadTree(root->cells[1], render_buffer);
		DrawQuadTree(root->cells[2], render_buffer);
		DrawQuadTree(root->cells[3], render_buffer);
	}
}


static QuadTree *
BuildQuadTree(GameState * game_state, MemoryArena * arena) {
	QuadTree * root = (QuadTree *)arena->AllocateZeroed(sizeof(QuadTree));

	root->min = Vec2(-0.5f * gScreenWidth, -0.5f * gScreenHeight);
	root->max = Vec2( 0.5f * gScreenWidth,  0.5f * gScreenHeight);

	for (u32 i = 0; i < game_state->component_system.collider_count; ++i) {
		ColliderComponent * col = game_state->component_system.colliders + i;
		if ((col->flags & ComponentFlag_Alive) == 0) continue;
		TransformComponent * transform = GetTransformFromEntity(game_state, col->host_entity);

		Rect bounds = RectFromMinMax(root->min, root->max);
		Rect aabb = RectFromCenterHalfExtent(transform->position, Vec2(col->circle.radius, col->circle.radius));
		if (!RectInsideRect(bounds, aabb)) {
			// This collider is outside the bounds of the quadtree.  This can
			// cause infinite recursion on add.  So let's not do that.
			continue;
		}

		if (col->flags & ComponentFlag_Alive) {
			Entity * e = GetEntity(game_state, col->host_entity);
			assert((e->flags & (EF_Alive | EF_Collides)) == (EF_Alive | EF_Collides));
			AddToQuadTree(game_state, root, col->host_entity, arena);
		}
	}

	return root;
}

struct CollisionPair {
	EntityHandle a;
	EntityHandle b;
};

static bool
CollisionAreadyResolved(EntityHandle a, EntityHandle b, CollisionPair * seen, u32 seen_count) {
	for (u32 i = 0; i < seen_count; ++i) {
		if (seen[i].a == a && seen[i].b == b) {
			return true;
		}
		if (seen[i].a == b && seen[i].b == a) {
			return true;
		}
	}
	return false;
}

void
DoCollisionAndDispatch(GameState * game_state, RenderCommandBuffer * render_buffer) {
	MemoryArenaBookmark qtree_bookmark = game_state->scratch_arena->Bookmark();
	QuadTree * qtree = BuildQuadTree(game_state, game_state->scratch_arena);

	CollisionPair collisions_seen[32] = {};
	u32 collisions_seen_count = 0;

	for (u32 i = 0; i < MAX_ENTITY_COUNT; ++i) {
		Entity * entity_a = game_state->entities + i;
		if ((entity_a->flags & (EF_Alive | EF_Collides)) == (EF_Alive | EF_Collides)) {
			TransformComponent * transform = GetTransformFromEntity(game_state, i);
			ColliderComponent * collider = (ColliderComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Collider, i);
				
			float r = collider->circle.radius;
			vec2 e_min = transform->position - Vec2(r, r);
			vec2 e_max = transform->position + Vec2(r, r);

			vec2 halfdim = (e_max - e_min) * 0.5f;
			vec2 center = (e_max + e_min) * 0.5f;

			Rect aabb = RectFromCenterHalfExtent(center, halfdim);
#if 0
			RenderOutlineRect(render_buffer, GetRectMin(aabb), GetRectMax(aabb), COLOR_RED, 1.0f, RenderLayer_UI);
#endif
			EntityHandle potential_collisions[128] = {};
			u32 potential_collision_count = QueryQuadTree(game_state, qtree, i, aabb, potential_collisions, array_count(potential_collisions));

			for (u32 j = 0; j < potential_collision_count; ++j) {
				EntityHandle a = i;
				EntityHandle b = potential_collisions[j];
				Entity * entity_b = GetEntity(game_state, b);

				// TODO(bryan):  This is being hit for entities that have been killed in a previous
				// collision but collide with another collider.
				// We'd really like to remove them from the pool when they die...
				if ((entity_a->flags & entity_b->flags & (EF_Alive | EF_Collides)) != (EF_Alive | EF_Collides)) {
					continue;
				}
				if (Collides(game_state, a, b)) {

					if (!CollisionAreadyResolved(a, b, collisions_seen, collisions_seen_count)) {
						assert(collisions_seen_count < array_count(collisions_seen));
						collisions_seen[collisions_seen_count].a = a;
						collisions_seen[collisions_seen_count].b = b;
						collisions_seen_count++;
					
						CollisionRule * rule = GetCollisionRule(game_state, entity_a->type, entity_b->type);
						if (rule) {
							if (entity_a->type == rule->a && entity_b->type == rule->b) {
								rule->handler(game_state, a, b);
							}
							else {
								rule->handler(game_state, b, a);
							}
						}
					}
				}
			}
		}
	}

#ifdef _DEBUG
	DrawQuadTree(qtree, render_buffer);
#endif
	game_state->scratch_arena->Reset(qtree_bookmark);
}

void 
TrySetTimer(GameState * game_state, u32 timer_idx, float ms) {
	assert(timer_idx < array_count(game_state->timers));
	assert(ms > 0.0f);
	Timer * timer = &game_state->timers[timer_idx];

	if (timer->time_remaining <= 0.0f) {
		timer->time_remaining = ms;
	}
}

void
ForceSetTimer(GameState * game_state, u32 timer_idx, float ms) {
	assert(timer_idx < array_count(game_state->timers));
	assert(ms > 0.0f);
	Timer * timer = &game_state->timers[timer_idx];

	timer->time_remaining = ms;
}

bool
WaitTimer(GameState * game_state, float dt_ms, u32 timer_idx) {
	assert(timer_idx < array_count(game_state->timers));

	Timer * timer = &game_state->timers[timer_idx];
	if (timer->time_remaining >= 0.0f) {
		timer->time_remaining -= dt_ms;
		if (timer->time_remaining <= 0.0f) {
			timer->time_remaining = 0.0f;
			return true;
		}
	}
	return false;
}

static bool
IsHighScore(GameState * game_state, u32 score) {
	for (u32 i = 0; i < array_count(game_state->high_scores); ++i) {
		HighScoreEntry * entry = &game_state->high_scores[i];

		if (entry->score < score) {
			return true;
		}
	}
	return false;
}

static void
InsertHighScore(GameState * game_state, HighScoreEntry entry) {
	u32 pos = array_count(game_state->high_scores);
	for (u32 i = 0; i < array_count(game_state->high_scores); ++i) {
		HighScoreEntry * cur = &game_state->high_scores[i];

		if (cur->score < entry.score) {
			pos = i;
			break;
		}
	}

	for (u32 i = array_count(game_state->high_scores) - 1; i > pos; --i) {
		game_state->high_scores[i] = game_state->high_scores[i - 1];
	}

	game_state->high_scores[pos] = entry;
}

static vec2
WorldCoordFromScreenCoord(vec2 screen_coord) {
	return (screen_coord * Vec2(1.0f, -1.0f)) - Vec2(gScreenWidth * 0.5f, -gScreenHeight * 0.5f);
}

static vec2
ScreenCoordFromWorldCoord(vec2 world_coord) {
	return (world_coord * Vec2(1.0f, -1.0f)) + Vec2(gScreenWidth * 0.5f, gScreenHeight * 0.5f);
}

void SwitchMenu(MenuId target, GameState * game_state) {
	game_state->menu_state.current_menu = target;
	game_state->menu_state.selected_item = 0;
	game_state->menu_state.anim_t = 0.0f;
	if (target == MenuId_Main && game_state->player_handle != NULL_ENTITY) {
		game_state->menu_state.selected_item = 1;
	}

	memset(game_state->menu_state.text_entry_buffer, 0, sizeof(game_state->menu_state.text_entry_buffer));
	ForceSetTimer(game_state, Timer_MenuTimeout, 50.0f);
}

struct MenuLayout {
	float font_scale;
	float font_size;
	float base_x;
	float base_y;
	float cur_x;
	float cur_y;

	u32 item_count;

	vec4 item_color;
	vec4 selected_color;

	bool selection_made;
	RenderCommandBuffer * render_buffer;
};

static bool
MenuTextOption(MenuLayout * menu_layout, char * text, MenuState * menu_state, bool has_bg=false) {
	bool is_selected = menu_state->selected_item == menu_layout->item_count;
	
	vec2 pos = WorldCoordFromScreenCoord(Vec2(menu_layout->cur_x, menu_layout->cur_y));
	vec2 scale = Vec2(menu_layout->font_scale, menu_layout->font_scale);

	if (has_bg) {
		float text_width = UITextWidth(text) * menu_layout->font_scale;
		float margin = 8.0f;
		vec4 bg_color = COLOR_BLACK;
		bg_color.a *= 0.9f;
		RenderRectangle(menu_layout->render_buffer, pos - Vec2(margin, -margin), pos + Vec2(text_width, -7.0f * menu_layout->font_scale - margin), bg_color, RenderLayer_UI);
	}
	RenderText(menu_layout->render_buffer, pos, text, scale, is_selected ? menu_layout->selected_color : menu_layout->item_color, RenderLayer_UI);
	if (is_selected) {
		vec4 emission_color = menu_layout->selected_color;
		emission_color.a *= 0.5f;
		RenderTextEmission(menu_layout->render_buffer, pos, text, scale, emission_color, RenderLayer_Foreground);
	}
	bool result = (menu_layout->selection_made && is_selected);
	menu_layout->cur_y += menu_layout->font_size;
	menu_layout->item_count++;
	
	return result;
}

static void
DrawRadioButton(RenderCommandBuffer * render_buffer, vec2 center, vec4 item_color, vec4 selected_color, bool toggle, bool selected) {
	if (toggle) {
		RenderCircle(render_buffer, WorldCoordFromScreenCoord(center), 12.0f, selected ? selected_color : item_color, 3.0f, RenderLayer_UI);
		RenderCircle(render_buffer, WorldCoordFromScreenCoord(center), 6.5f, selected ? selected_color : item_color, 0.0f, RenderLayer_UI);
	}
	else {
		RenderCircle(render_buffer, WorldCoordFromScreenCoord(center), 12.0f, item_color, 3.0f, RenderLayer_UI);
	}
}

static void
MenuBoolOption(MenuLayout * layout, char * label, char * on_label, char * off_label, MenuState * menu_state, bool * toggle) {
	bool is_selected = menu_state->selected_item == layout->item_count;

	vec2 pos = Vec2(layout->cur_x, layout->cur_y);
	vec2 scale = Vec2(layout->font_scale, layout->font_scale);
	RenderText(layout->render_buffer, WorldCoordFromScreenCoord(pos), label, scale, is_selected ? layout->selected_color : layout->item_color, RenderLayer_UI);
	if (is_selected) {
		vec4 emission_color = layout->selected_color;
		emission_color.a *= 0.5f;
		RenderTextEmission(layout->render_buffer, WorldCoordFromScreenCoord(pos), label, scale, emission_color, RenderLayer_UI);
	}
	RenderText(layout->render_buffer, WorldCoordFromScreenCoord(pos + Vec2(300.0f, 0.0f)), on_label, scale, layout->item_color, RenderLayer_UI);
	RenderText(layout->render_buffer, WorldCoordFromScreenCoord(pos + Vec2(575.0f, 0.0f)), off_label, scale, layout->item_color, RenderLayer_UI);

	DrawRadioButton(layout->render_buffer, pos + Vec2(300.0f - 25.0f, 14.0f), layout->item_color, layout->selected_color, *toggle, is_selected);
	DrawRadioButton(layout->render_buffer, pos + Vec2(575.0f - 25.0f, 14.0f), layout->item_color, layout->selected_color, !*toggle, is_selected);

	if (is_selected && layout->selection_made) {
		*toggle = !*toggle;
	}
	layout->cur_y += layout->font_size;
	layout->item_count++;
}

static void
DrawSlider(RenderCommandBuffer * render_buffer, vec2 start, vec2 end, float value, vec4 item_color, vec4 selected_color, bool selected) {
	// Slider bar
	RenderRectangle(render_buffer, WorldCoordFromScreenCoord(start), WorldCoordFromScreenCoord(end), item_color, RenderLayer_UI);
	if (selected) {
		vec2 glow_margin = Vec2(1.0f, 1.0f);
		RenderRectangleEmission(render_buffer, WorldCoordFromScreenCoord(start - glow_margin), WorldCoordFromScreenCoord(end + glow_margin), selected_color, RenderLayer_UI);
	}
	// Handle
	float slider_pos_x = start.x + (end.x - start.x) * value;

	vec2 slider_pos = WorldCoordFromScreenCoord(Vec2(slider_pos_x, 0.5f*(end.y + start.y)));
	Rect slider_rect = RectFromCenterHalfExtent(slider_pos, Vec2(4.0f, 15.0f));
	RenderRectangle(render_buffer, GetRectMin(slider_rect), GetRectMax(slider_rect), selected ? selected_color : item_color, RenderLayer_UI);
	if (selected) {
		RenderRectangleEmission(render_buffer, GetRectMin(slider_rect), GetRectMax(slider_rect), selected_color, RenderLayer_UI);
	
//		RenderOutlineRect(render_buffer, GetRectMin(slider_rect) - Vec2(5.0f, 5.0f), GetRectMax(slider_rect) + Vec2(5.0f, 5.0f), selected_color, 3.0f);
//		RenderRectangleEmission(render_buffer, GetRectMin(slider_rect) - Vec2(5.0f, 5.0f), GetRectMax(slider_rect) + Vec2(5.0f, 5.0f), selected_color);
	}
}

static bool
MenuSliderOption(MenuLayout * layout, GameInput * game_input, char * label, MenuState * menu_state, float * value) {
	bool is_selected = menu_state->selected_item == layout->item_count;

	RenderText(layout->render_buffer, WorldCoordFromScreenCoord(Vec2(layout->cur_x, layout->cur_y)), label, Vec2(layout->font_scale, layout->font_scale), is_selected ? layout->selected_color : layout->item_color, RenderLayer_UI);
	if (is_selected) {
		vec4 emission_color = layout->selected_color;
		emission_color.a *= 0.5f;
		RenderTextEmission(layout->render_buffer, WorldCoordFromScreenCoord(Vec2(layout->cur_x, layout->cur_y)), label, Vec2(layout->font_scale, layout->font_scale), emission_color, RenderLayer_UI);
	}
	float slider_start_x = layout->cur_x + 300.0f - 25.0f;
	float slider_end_x = layout->cur_x + 700.0f;

	float margin = 5.0f;
	float slider_start_y = layout->cur_y + margin;
	float slider_end_y = slider_start_y + (layout->font_size - 6.0f * margin);

	DrawSlider(layout->render_buffer, Vec2(slider_start_x, slider_start_y), Vec2(slider_end_x, slider_end_y), *value, layout->item_color, layout->selected_color, is_selected);

	char percent_buf[5] = {0};
	_snprintf(percent_buf, 5, "%3d%%", (u32)((*value) * 100.0f));
	RenderText(layout->render_buffer, WorldCoordFromScreenCoord(Vec2(slider_end_x + 50.0f, layout->cur_y)), percent_buf, Vec2(layout->font_scale, layout->font_scale), layout->item_color, RenderLayer_UI);

	bool result = false;
	float adjust_amount = 0.01f;
	if (is_selected && (InputButtonIsDown(game_input, GameVirtualInputs_MenuLeft) || InputButtonIsDown(game_input, GameVirtualInputs_Left))) {
		*value = max(0.0f, *value - adjust_amount);
		result = true;
	}
	else if (is_selected && (InputButtonIsDown(game_input, GameVirtualInputs_MenuRight) || InputButtonIsDown(game_input, GameVirtualInputs_Right))) {
		*value = min(1.0f, *value + adjust_amount);
		result = true;
	}

	layout->cur_y += layout->font_size;
	layout->item_count++;

	return result;
}

static void
InitMenuLayout(GameState * game_state, GameInput * game_input, MenuLayout * layout, 
			   RenderCommandBuffer * render_buffer, float font_scale, float base_x, float base_y) {
	layout->item_color = COLOR_WHITE;
	layout->selected_color = COLOR_YELLOW;
	layout->font_scale = font_scale;
	layout->font_size = 12.0f * layout->font_scale;
	layout->base_x = base_x;
	layout->base_y = base_y;
	layout->cur_x = layout->base_x;
	layout->cur_y = layout->base_y;
	layout->render_buffer = render_buffer;

	layout->item_count = 0;
	layout->selection_made = (WaitTimer(game_state, game_input->delta_time, Timer_MenuTimeout) &&
				   			 InputButtonWentDown(game_input, GameVirtualInputs_MenuSelect));
}
void ResetGameState(GameState *, MemoryArena *);

static void
RunMenu(GameState * game_state, GameInput * game_input, RenderCommandBuffer * render_buffer, SoundCommandBuffer * sound_buffer, MemoryArena * game_memory) {
	MenuState * menu_state = &game_state->menu_state;
	assert(menu_state->current_menu > 0);
	assert(menu_state->current_menu < MenuId_count);

	SetMusic(sound_buffer, DEBUG_Silence);

	menu_state->anim_t += game_input->delta_time;

	switch (menu_state->current_menu) 
	{
	case MenuId_QuitConfirm:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);

			char * quit_message = "Are you sure you want to quit?";
			RenderText(render_buffer, Vec2(0.0f, 2.0f * layout.font_size) - UITextCenterOffset(quit_message, layout.font_scale), quit_message, Vec2(layout.font_scale, layout.font_scale), layout.item_color, RenderLayer_UI);

			// TODO:  Need some better way to place UI text.  
			{
				float choices_spacing = 200.0f;
				float choices_width = UITextWidth("Yes") * layout.font_scale + UITextWidth("No") * layout.font_scale + choices_spacing;
				vec2 choices_base_pos = ScreenCoordFromWorldCoord(Vec2(-0.5f * choices_width, 0.0f));
				layout.base_y = layout.cur_y = choices_base_pos.y;
				layout.base_x = layout.cur_x = choices_base_pos.x;
				if (MenuTextOption(&layout, "Yes", menu_state)) {
					extern bool gRunning; 
					gRunning = false;
				}
				layout.cur_y = layout.base_y;
				layout.cur_x += choices_spacing;
				if (MenuTextOption(&layout, "No", menu_state)) {
					extern bool gQuitAttempt;
					gQuitAttempt = false;  // Clear the quit attempt flag.
					SwitchMenu(MenuId_Main, game_state);
				}
			}

			if (layout.selection_made) {
				TrySetTimer(game_state, Timer_MenuTimeout, 20.0f);
			}
			
			if (InputButtonWentDown(game_input, GameVirtualInputs_Down) || InputButtonWentDown(game_input, GameVirtualInputs_MenuDown) ||
				InputButtonWentDown(game_input, GameVirtualInputs_Right) || InputButtonWentDown(game_input, GameVirtualInputs_MenuRight)) {
				menu_state->selected_item++;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
				TrySetTimer(game_state, Timer_MenuTimeout, 20.0f);
			}
			else if (InputButtonWentDown(game_input, GameVirtualInputs_Up) || InputButtonWentDown(game_input, GameVirtualInputs_MenuUp) ||
				InputButtonWentDown(game_input, GameVirtualInputs_Left) || InputButtonWentDown(game_input, GameVirtualInputs_MenuLeft)) {
				if (menu_state->selected_item == 0) menu_state->selected_item = layout.item_count;
				menu_state->selected_item--;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
				TrySetTimer(game_state, Timer_MenuTimeout, 20.0f);
			}

			menu_state->selected_item %= layout.item_count;
		}
		break;
	case MenuId_Main:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);
		
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(32.0f, 32.0f)), "Invaders!", Vec2(10.0f, 10.0f), layout.item_color, RenderLayer_UI);
			// Start
			{
				if (MenuTextOption(&layout, "Start", menu_state)) {
					ResetGameState(game_state, game_memory);
					SwitchMenu(MenuId_None, game_state);
					PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, game_state->player_handle);
					player->health = 3;
					game_state->paused = false;
					SetMusic(sound_buffer, OGG_Music01);
				}
			}
			// Continue
			{
				if (game_state->player_handle != NULL_ENTITY) {
					if (MenuTextOption(&layout, "Continue", menu_state)) {
						SwitchMenu(MenuId_None, game_state);
						game_state->paused = false;
						SetMusic(sound_buffer, OGG_Music01);
					}
				}
				else {
					vec2 pos = Vec2(layout.cur_x, layout.cur_y);
					RenderText(render_buffer, WorldCoordFromScreenCoord(pos), "Continue", Vec2(layout.font_scale, layout.font_scale), COLOR_GREY, RenderLayer_UI);
					layout.cur_y += layout.font_size;
				}
			}
			// Controls
			{
				if (MenuTextOption(&layout, "Controls", menu_state)) SwitchMenu(MenuId_Controls, game_state);
			}
			// Options
			{
				if (MenuTextOption(&layout, "Options", menu_state)) SwitchMenu(MenuId_Options, game_state);
			}
			// High Scores
			{
				if (MenuTextOption(&layout, "High Scores", menu_state)) SwitchMenu(MenuId_HighScores, game_state);
			}
			// Credits
			{
				if (MenuTextOption(&layout, "Credits", menu_state)) SwitchMenu(MenuId_Credits, game_state);
			}
			// Exit
			{
				if (MenuTextOption(&layout, "Exit", menu_state)) SwitchMenu(MenuId_QuitConfirm, game_state);
			}

			{
				vec2 font_scale = Vec2(layout.font_scale * 0.5f, layout.font_scale * 0.5f);

				float controller_state_y = ((float)gScreenHeight) - (5.0f * 12.0f * font_scale.y - (12.0f - 7.0f));
				float controller_state_x = ((float)gScreenWidth) - (200.0f);

				vec2 position = WorldCoordFromScreenCoord(Vec2(controller_state_x, controller_state_y));

				// HACK:  Assumes this is the longest string displayed.
				float text_width = UITextWidth("1: Not Connected") * font_scale.x;
				float text_height = 12.0f * font_scale.y * 4 - (12.0f - 7.0f);
				float margin = 16.0f;
				vec4 bg_color = COLOR_BLACK;
				bg_color.a *= 0.9f;
				RenderRectangle(render_buffer, position - Vec2(margin, -margin), position + Vec2(text_width + margin, -text_height - margin), bg_color, RenderLayer_Foreground, 6);

				for (u32 controller = ControllerIndex_Gamepad0; controller < ControllerIndex_Count; ++controller) {
					char controller_status_buffer[64] = {};
					char * status_msg = "Not Connected";
					vec4 status_color = COLOR_RED;
					float status_emission_alpha = 0.0f;
					bool connected = ((game_input->controllers[controller].flags & ControllerFlag_IsAttached) != 0);
					if (connected) {
						if (ControllerIsLive(game_input, controller)) {
							status_msg = "Ready";
							status_color = COLOR_GREEN;
							status_emission_alpha = 1.0f;
						}
						else {
							status_msg = "Press Start";
							status_color = COLOR_YELLOW;
							status_emission_alpha = (sinf(menu_state->anim_t * 0.005f) + 1.0f) * 0.5f;
						}
					}

					_snprintf(controller_status_buffer, array_count(controller_status_buffer), "%d: %s", controller, status_msg);

					RenderText(render_buffer, position, controller_status_buffer, font_scale, status_color, RenderLayer_UI, 3);
					status_color.a *= status_emission_alpha;
					RenderTextEmission(render_buffer, position, controller_status_buffer, font_scale, status_color, RenderLayer_UI, 3);

					position.y -= 12.0f * font_scale.y;
				}
			}

			if (layout.selection_made) {
				TrySetTimer(game_state, Timer_MenuTimeout, 20.0f);
			}
			if (InputButtonWentDown(game_input, GameVirtualInputs_Down) || InputButtonWentDown(game_input, GameVirtualInputs_MenuDown)) {
				menu_state->selected_item++;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
			}
			else if (InputButtonWentDown(game_input, GameVirtualInputs_Up) || InputButtonWentDown(game_input, GameVirtualInputs_MenuUp)) {
				if (menu_state->selected_item == 0) menu_state->selected_item = layout.item_count;
				menu_state->selected_item--;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
			}

			menu_state->selected_item %= layout.item_count;
		}
		break;
	case MenuId_Controls:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(32.0f, 32.0f)), "Controls", Vec2(10.0f, 10.0f), layout.item_color, RenderLayer_UI);

			vec2 font_scale = Vec2(layout.font_scale, layout.font_scale);
//			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 200.0f, layout.cur_y)), "Keyboard", font_scale, game_input->is_controller ? layout.item_color : layout.selected_color, RenderLayer_UI);
//			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 500.0f, layout.cur_y)), "Controller", font_scale, game_input->is_controller ? layout.selected_color: layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size * 1.25f;
			
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Move: ", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 200.0f, layout.cur_y)), "Arrow Keys", font_scale, layout.item_color, RenderLayer_UI);
			Sprite * left_stick = game_state->button_sprite + 4;
			RenderSprite(render_buffer, left_stick, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 500.0f, layout.cur_y) + left_stick->size * 0.5f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			layout.cur_y += layout.font_size;

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Fire: ", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 200.0f, layout.cur_y)), "Space", font_scale, layout.item_color, RenderLayer_UI);
			Sprite * right_bumper = game_state->wide_button_sprite + 1;
			Sprite * a_button = game_state->button_sprite + 0;
			RenderSprite(render_buffer, a_button, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 500.0f, layout.cur_y) + a_button->size * 0.5f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSprite(render_buffer, right_bumper, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 550.0f, layout.cur_y) + right_bumper->size * 0.5f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			layout.cur_y += layout.font_size;

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Pause: ", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 200.0f, layout.cur_y)), "Escape", font_scale, layout.item_color, RenderLayer_UI);
			Sprite * start_button = game_state->button_sprite + 7;
			RenderSprite(render_buffer, start_button, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 500.0f, layout.cur_y) + start_button->size * 0.5f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			layout.cur_y += layout.font_size;

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Select: ", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 200.0f, layout.cur_y)), "Enter", font_scale, layout.item_color, RenderLayer_UI);
			RenderSprite(render_buffer, a_button, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 500.0f, layout.cur_y) + a_button->size * 0.5f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			layout.cur_y += layout.font_size;

			if (layout.selection_made) {
				SwitchMenu(MenuId_Main, game_state);
			}
		}
		break;
	case MenuId_Options:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);

			{
				float margin = 16.0f;
				vec2 position = WorldCoordFromScreenCoord(Vec2(margin * 2.0f, margin * 2.0f)) + Vec2(0.0f, -50.0f);
				float bg_width = (float)gScreenWidth - 4.0f * margin;
				float bg_height = (float)gScreenHeight * 0.75f - 4.0f * margin;
				vec4 bg_color = COLOR_BLACK;
				bg_color.a *= 0.9f;
				RenderRectangle(render_buffer, position - Vec2(margin, -margin + 60.0f), position + Vec2(bg_width + margin, -bg_height - margin), bg_color, RenderLayer_UI, ZOrder_Player - 2);
			}
			
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(32.0f, 32.0f)), "Options", Vec2(10.0f, 10.0f), layout.item_color, RenderLayer_UI);

			// Windowed/Fullscreen
			{
				extern bool gIsFullscreen;
				MenuBoolOption(&layout, "Display", "Fullscreen", "Windowed", menu_state, &gIsFullscreen);
			}
			// Audio Mix
			{
				MenuBoolOption(&layout, "Audio Mix", "Headphone", "Speaker", menu_state, &game_state->options.headphone_mix);
			}
			// Mute
			{
				MenuBoolOption(&layout, "Mute", "On", "Off", menu_state, &game_state->options.mute);
			}
			// Volume
			{
				RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Volume:", Vec2(layout.font_scale, layout.font_scale), layout.item_color, RenderLayer_UI);
				layout.cur_y += layout.font_size;
				layout.cur_x += 64.0f;
				if (MenuSliderOption(&layout, game_input, "Master", menu_state, &game_state->options.master_volume)) {
					//PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, DEBUG_Square500);
				}
				if (MenuSliderOption(&layout, game_input, "Music", menu_state, &game_state->options.music_volume)) {
					//PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, DEBUG_Square500);
				}
				if (MenuSliderOption(&layout, game_input, "Effect", menu_state, &game_state->options.effect_volume)) {
					//PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, DEBUG_Square500);
				}
				layout.cur_x -= 64.0f;
			}
			// Screenshake
			{
				layout.cur_y += 32.0f; // Section break
				if (layout.item_count == menu_state->selected_item) {
					SetPostEffectProperty(render_buffer, PostEffect_ScreenShakeIntensity, 16.0f * game_state->options.screenshake);
				}
				if (MenuSliderOption(&layout, game_input, "Screenshake", menu_state, &game_state->options.screenshake)) {
					//PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, DEBUG_Square500);
				}
			}

			// Back
			{
				layout.cur_y += 32.0f; // Section break
				if (MenuTextOption(&layout, "Back", menu_state, true)) {
					SaveGameOptions(game_state, game_state->scratch_arena);
					SwitchMenu(MenuId_Main, game_state);
				}
			}
			if (layout.selection_made) {
				TrySetTimer(game_state, Timer_MenuTimeout, 20.0f);
			}
			if (InputButtonWentDown(game_input, GameVirtualInputs_Down) || InputButtonWentDown(game_input, GameVirtualInputs_MenuDown)) {
				menu_state->selected_item++;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
			}
			else if (InputButtonWentDown(game_input, GameVirtualInputs_Up) || InputButtonWentDown(game_input, GameVirtualInputs_MenuUp)) {
				if (menu_state->selected_item == 0) menu_state->selected_item = layout.item_count;
				menu_state->selected_item--;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
			}

			if (InputButtonWentDown(game_input, GameVirtualInputs_MenuEscape)) {
				SwitchMenu(MenuId_Main, game_state);
			}

			menu_state->selected_item %= layout.item_count;
		}
		break;
	case MenuId_GameOver:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);
			
			{
				char game_over[] = "Game Over";

				float scale = 10.0f;
				vec2 offset = Vec2(0.0f, scale * 12.0f * 2.0f);
				offset.x += -0.5f * UITextWidth(game_over) * scale;
				offset.y += 0.5f * 12.0f * scale;

				RenderText(render_buffer, offset, game_over, Vec2(scale, scale), COLOR_WHITE, RenderLayer_UI);
			}
			bool is_high_score = IsHighScore(game_state, game_state->score); 
			if (is_high_score) {
				char high_score_msg[] = "New High Score!"; 

				float scale = 8.0f;

				vec2 offset = Vec2(0.0f, scale * 12.0f);
				offset.x += -0.5f * UITextWidth(high_score_msg) * scale;
				offset.y += 0.5f * 12.0f * scale;

				RenderText(render_buffer, offset, high_score_msg, Vec2(scale, scale), layout.selected_color, RenderLayer_UI);
				RenderTextEmission(render_buffer, offset, high_score_msg, Vec2(scale, scale), layout.selected_color * 0.5f, RenderLayer_UI);
			}

			{
				char score_buf[64];
				float scale = 8.0f;
				_snprintf(score_buf, array_count(score_buf), "Score: %d", game_state->score);

				vec2 offset = Vec2();
				offset.x += -0.5f * UITextWidth(score_buf) * scale;
				offset.y += 0.5f * 12.0f * scale;

				RenderText(render_buffer, offset, score_buf, Vec2(scale, scale), layout.item_color, RenderLayer_UI);
			}

			if (layout.selection_made) {
				if (is_high_score) {
					SwitchMenu(MenuId_HighScoreNameEntry, game_state);
				}
				else {
					SwitchMenu(MenuId_PlayerStats, game_state);
				}
			}
		}
		break;
	case MenuId_HighScoreNameEntry:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);

			u32 char_count = array_count(menu_state->text_entry_buffer) - 1;

			vec2 scale = Vec2(layout.font_scale, layout.font_scale);


			char name_msg[] = "Please enter your name:";
			RenderText(render_buffer, Vec2(-0.5f * UITextWidth(name_msg) * scale.x, layout.font_size * 2.0f), name_msg, scale, layout.item_color, RenderLayer_UI);

			scale *= 2.0f;
			vec2 offset = Vec2((-40.0f * char_count) * 0.5f, 0.0f);
			for (u32 i = 0; i < char_count; ++i) {
				char * cur = &game_state->menu_state.text_entry_buffer[i];
				char buf[2] = {};
				if (*cur && *cur != ' ') {
					buf[0] = *cur;
				}
				else {
					buf[0] = '_';
				}
				vec4 color = menu_state->selected_item == i ? layout.selected_color : layout.item_color;
				RenderText(render_buffer, offset, buf, scale, color, RenderLayer_UI);
				offset.x += 40.0f;
			}
#define WENT_DOWN(c) ((game_input->current->keys[c] & 0x80) && !(game_input->previous->keys[c] & 0x80) != 0)

			bool shift_is_down = ((game_input->current->keys[0x10] & 0x80) != 0);
			if (menu_state->selected_item < char_count - 1) {
				for (u32 c = 0x30; c < 0x5B; ++c) {
					if (WENT_DOWN(c)) {
						menu_state->text_entry_buffer[menu_state->selected_item++] = shift_is_down ? (u8)c : (u8)c | 0x20;
						break;
					}
				}
			}
			if (menu_state->selected_item < char_count) {
				if (WENT_DOWN(0x20)) {
					// Space
					menu_state->text_entry_buffer[menu_state->selected_item++] = ' ';
				}
			}
			if (menu_state->selected_item > char_count) {
				menu_state->selected_item = char_count;
			}
			
			if (menu_state->selected_item != 0) {
				if (WENT_DOWN(0x08)) {
					// Backspace
					menu_state->text_entry_buffer[--menu_state->selected_item] = ' ';
					for (u32 i = menu_state->selected_item + 1; i != 0 && i < char_count - 1; ++i) {
						menu_state->text_entry_buffer[i - 1] = menu_state->text_entry_buffer[i];
					}
				}
			}
#undef WENT_DOWN

			if (InputButtonWentDown(game_input, GameVirtualInputs_Right) || InputButtonWentDown(game_input, GameVirtualInputs_MenuRight)) {
				menu_state->selected_item++;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
			}
			else if (InputButtonWentDown(game_input, GameVirtualInputs_Left) || InputButtonWentDown(game_input, GameVirtualInputs_MenuLeft)) {
				if (menu_state->selected_item == 0) menu_state->selected_item = char_count;
				menu_state->selected_item--;
				PlaySFXBark(sound_buffer, Vec2(0.0f, 1.0f), 1.0f, GEN_MenuSound);
			}
			if (menu_state->selected_item >= char_count) {
				menu_state->selected_item = char_count - 1;
			}

			if (layout.selection_made) {
				bool name_entry_is_blank = true;
				for (u32 i = 0; i < array_count(menu_state->text_entry_buffer) - 1; ++i) {
					if (menu_state->text_entry_buffer[i] != '\0' && menu_state->text_entry_buffer[i] != ' ') {
						name_entry_is_blank = false;
						break;
					}
				}

				if (!name_entry_is_blank) {
					// Only log the score if we actually have a name.
					// Prevents a loading bug that wipes high scores.
					HighScoreEntry entry;
					entry.level = game_state->level;
					entry.score = game_state->score;
					strncpy(entry.name, menu_state->text_entry_buffer, array_count(entry.name) - 1);
					InsertHighScore(game_state, entry);
					SaveHighScores(game_state, game_state->scratch_arena);
				}

				SwitchMenu(MenuId_PlayerStats, game_state);
			}
		}
		break;
	case MenuId_PlayerStats:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(32.0f, 32.0f)), "Post-game Stats", Vec2(10.0f, 10.0f), layout.item_color, RenderLayer_UI);

			float zero_width = UITextWidth("0") * layout.font_scale;

			PlayerStats stats = game_state->player_stats;
			float accuracy = 0.0f;
			if (stats.shots_fired > 0) {
				accuracy = 100.0f * ((float)stats.shots_hit / (float)stats.shots_fired);
			}

			MemoryArenaBookmark bookmark = game_memory->Bookmark();
			{
				char * stat_formats[8] = {
					"Kills:",
					"Saucer Kills:",
					"Shots Fired:",
					"Shots Hit:",
					"Accuracy (%):",
					"Best Streak:",
					"Damage Taken:",
					"Pickups (Health/Shield):",
				};
				char stat_buffers[8][64] = {};
				_snprintf(stat_buffers[0], array_count(stat_buffers[0]), "%d", stats.kills);
				_snprintf(stat_buffers[1], array_count(stat_buffers[1]), "%d", stats.saucer_kills);
				_snprintf(stat_buffers[2], array_count(stat_buffers[2]), "%d", stats.shots_fired);
				_snprintf(stat_buffers[3], array_count(stat_buffers[3]), "%d", stats.shots_hit);

				_snprintf(stat_buffers[4], array_count(stat_buffers[4]), "%3.2f", accuracy);
				_snprintf(stat_buffers[5], array_count(stat_buffers[5]), "%d", stats.best_streak);
				_snprintf(stat_buffers[6], array_count(stat_buffers[6]), "%d", stats.damage_taken);
				_snprintf(stat_buffers[7], array_count(stat_buffers[7]), "%d / %d", stats.health_picked_up, stats.shields_picked_up);

				float margin = 16.0f;
				float bg_width = gScreenWidth - 2.0f * layout.cur_x;
				float bg_height = 12.0f * layout.font_scale * 8.0f - (12.0f - 7.0f);
				vec2 position = WorldCoordFromScreenCoord(Vec2(layout.base_x, layout.cur_y));
				{
					vec4 bg_color = COLOR_BLACK;
					bg_color.a *= 0.9f;
					RenderRectangle(render_buffer, position - Vec2(margin, -margin), position + Vec2(bg_width + margin, -bg_height - margin), bg_color, RenderLayer_UI);
				}

				vec2 right_side_offset = Vec2(bg_width, 0.0f);
				vec2 font_scale = Vec2(layout.font_scale, layout.font_scale);
				for (u32 i = 0; i < array_count(stat_formats); ++i) {
					RenderText(render_buffer, position, stat_formats[i], font_scale, layout.item_color, RenderLayer_UI);

					float x_align_adjust = UITextWidth(stat_buffers[i]) * layout.font_scale;
					vec2 number_pos = position + (right_side_offset - Vec2(x_align_adjust, 0.0f));
					RenderText(render_buffer, number_pos, stat_buffers[i], font_scale, layout.item_color, RenderLayer_UI);

					position.y -= layout.font_size;
				}
			}
			game_memory->Reset(bookmark);
			if (layout.selection_made) {
				SwitchMenu(MenuId_HighScores, game_state);
			}
		} break;
	case MenuId_HighScores:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(32.0f, 32.0f)), "High Scores", Vec2(10.0f, 10.0f), layout.item_color, RenderLayer_UI);
			
			float zero_width = UITextWidth("0") * layout.font_scale;
			{
				vec2 position = WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y));
				float bg_width = (600.0f - layout.cur_x) + zero_width * 11.0f;
				float bg_height = 12.0f * layout.font_scale * 10.0f - (12.0f - 7.0f);
				float margin = 16.0f;
				vec4 bg_color = COLOR_BLACK;
				bg_color.a *= 0.9f;
				RenderRectangle(render_buffer, position - Vec2(margin, -margin), position + Vec2(bg_width + margin, -bg_height - margin), bg_color, RenderLayer_UI);
			}

			char buffer[128] = {};
			HighScoreEntry * high_scores = game_state->high_scores;
			for (u32 i = 0; i < 10; ++i) {
				_snprintf(buffer, array_count(buffer), "%02d: %s", i + 1, high_scores[i].name);
				RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), buffer, Vec2(layout.font_scale, layout.font_scale), layout.item_color, RenderLayer_UI);
				
				_snprintf(buffer, array_count(buffer), "%d", high_scores[i].score);
				float zero_skip = zero_width * 9.0f;
				if (high_scores[i].score > 0) {
					zero_skip = zero_width * (10.0f - (floorf(log10f((float)high_scores[i].score))) - 1.0f);
				}
				RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 600 + zero_skip, layout.cur_y)), buffer, Vec2(layout.font_scale, layout.font_scale), layout.item_color, RenderLayer_UI);
				layout.cur_y += layout.font_size;
			}

			if (layout.selection_made) {
				SwitchMenu(MenuId_Main, game_state);
			}
		}
		break;
	case MenuId_Credits:
		{
			MenuLayout layout = {};
			InitMenuLayout(game_state, game_input, &layout, render_buffer, 4.0f, 32.0f, 150.0f);

			vec2 font_scale = Vec2(layout.font_scale, layout.font_scale);

			{
				float margin = 16.0f;
				vec2 position = WorldCoordFromScreenCoord(Vec2(margin * 2.0f, margin * 2.0f));
				float bg_width = (float)gScreenWidth - 4.0f * margin;
				float bg_height = (float)gScreenHeight - 4.0f * margin;
				vec4 bg_color = COLOR_BLACK;
				bg_color.a *= 0.9f;
				RenderRectangle(render_buffer, position - Vec2(margin, -margin), position + Vec2(bg_width + margin, -bg_height - margin), bg_color, RenderLayer_UI);
			}

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(32.0f, 32.0f)), "Credits", Vec2(10.0f, 10.0f), layout.item_color, RenderLayer_UI);

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Art/Design/Programming:", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f, layout.cur_y)), "Bryan Taylor", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			
			layout.cur_y += layout.font_size;

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Testing:", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f, layout.cur_y)), "Eric West", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f + 300.0f, layout.cur_y)), "Shawn Sloan", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f + 750.0f, layout.cur_y)), "Kanmuri", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f, layout.cur_y)), "Ritobito", font_scale, layout.item_color, RenderLayer_UI);
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f + 300.0f, layout.cur_y)), "BlackDragonHunt", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;

			layout.cur_y += layout.font_size;

			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Special Thanks:", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f, layout.cur_y)), "Sean Barrett   - stb libraries", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x + 32.0f, layout.cur_y)), "Casey Muratori - Handmade Hero", font_scale, layout.item_color, RenderLayer_UI);
			layout.cur_y += layout.font_size;
			
			layout.cur_y += layout.font_size * 0.9f;
			RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(layout.cur_x, layout.cur_y)), "Copyright (c) 2015 asobou Interactive", font_scale * 0.5f, layout.item_color, RenderLayer_UI);

			if (layout.selection_made) {
				SwitchMenu(MenuId_Main, game_state);
			}
		}
		break;
	default:
		assert(0);
	}
}

inline void KillEntity(GameState *, Entity *);
inline void KillEntity(GameState *, EntityHandle);

void
UpdateParticles(GameState * game_state, ParticleSystemComponent * particle_system, float delta_seconds) {
	for (u32 particle_idx = 0; particle_idx < particle_system->live_count; ++particle_idx) {
		particle_system->pos_x[particle_idx] += particle_system->vel_x[particle_idx] * delta_seconds;
		particle_system->pos_y[particle_idx] += particle_system->vel_y[particle_idx] * delta_seconds;
		if (abs(particle_system->initial_force.x) > 0.0f && abs(particle_system->initial_force.y) > 0.0f) {
//			particle_system->vel_x[particle_idx] += 1.0f / (particle_system->initial_force.x * delta_seconds);
//			particle_system->vel_y[particle_idx] += 1.0f / (particle_system->initial_force.y * delta_seconds);
		}
		particle_system->time_to_live[particle_idx] -= delta_seconds;

		particle_system->angle[particle_idx] = fmodf(particle_system->rotation[particle_idx] + particle_system->angle[particle_idx], 2.0f * PI32);

		if (particle_system->time_to_live[particle_idx] < 0.0f) {
			u32 last_idx = --particle_system->live_count;
			particle_system->pos_x[particle_idx] = particle_system->pos_x[last_idx];
			particle_system->pos_y[particle_idx] = particle_system->pos_y[last_idx];
			particle_system->vel_x[particle_idx] = particle_system->vel_x[last_idx];
			particle_system->vel_y[particle_idx] = particle_system->vel_y[last_idx];
			particle_system->angle[particle_idx] = particle_system->angle[last_idx];
			particle_system->rotation[particle_idx] = particle_system->rotation[last_idx];
			particle_system->time_to_live[particle_idx] = particle_system->time_to_live[last_idx];
			--particle_idx;
		}
	}
	if (particle_system->live_count == 0) {
		KillEntity(game_state, particle_system->host_entity);
	}
}

EntityHandle 
AddEntity(GameState * game_state, EntityType type) {
	for (u32 i = 0; i < MAX_ENTITY_COUNT; ++i) {
		Entity * cur = game_state->entities + i;
		if ((cur->flags & EF_Alive) == 0) {
			cur->type = type;
			cur->component_count = 0;
			cur->flags = (EF_Alive);
			return i;
		}
	}
	return NULL_ENTITY;
}

Entity *
GetEntity(GameState * game_state, EntityHandle handle) {
	Entity * e = &game_state->entities[handle];
	return e;
}

static TransformComponent *
AddTransform(GameState * game_state, EntityHandle handle, vec2 position, vec2 scale = Vec2(1.0f, 1.0f), vec2 rotation = Vec2(1.0f, 0.0f)) {
	TransformComponent * transform = (TransformComponent *)AddComponent(game_state, ComponentTypeID_Transform, handle);
	transform->position = position;
	transform->scale = scale;
	transform->rotation = rotation;
	return transform;
}

static ColliderComponent *
AddCircleCollider(GameState * game_state, EntityHandle handle, float radius) {
	ColliderComponent * collider = (ColliderComponent *)AddComponent(game_state, ComponentTypeID_Collider, handle);
	collider->type = CT_Circle;
	collider->circle.radius = radius;

	Entity * entity = GetEntity(game_state, handle);
	entity->flags |= EF_Collides;

	return collider;
}

void SpawnPlayerDebris(GameState *);
void SpawnTextEntity(GameState *, vec2, char *, vec4);
 
static void
SpawnParticleEmitter(GameState * game_state, Sprite * sprite, u32 particle_count, vec2 position, vec2 initial_velocity, vec2 velocity_range, float seconds_to_live = 1.0f, vec2 rotation = Vec2(1.0f, 0.0f), float jitter = 0.5f) {
	EntityHandle handle = AddEntity(game_state, EntityType_ParticleSystem);

	TransformComponent * transform = AddTransform(game_state, handle, position, Vec2(1.0f, 1.0f), rotation);

	ParticleSystemComponent * particles = (ParticleSystemComponent *)AddComponent(game_state, ComponentTypeID_ParticleSystem, handle);

	assert(particle_count < array_count(particles->pos_x));

	// TODO(bryan):  Move particle systems over to vector rotation.
	float angle = atan2f(initial_velocity.y, initial_velocity.x);
	for (u32 i = 0; i < particle_count; ++i) {
		float r = Random_NextFloat01(&game_state->effects_rng);
		particles->pos_x[i] = jitter * r;
		r = Random_NextFloat01(&game_state->effects_rng);
		particles->pos_y[i] = jitter * r;
		r = Random_NextFloat11(&game_state->effects_rng);
		particles->vel_x[i] = initial_velocity.x + velocity_range.x * r;
		r = Random_NextFloat11(&game_state->effects_rng);
		particles->vel_y[i] = initial_velocity.y + velocity_range.y * r;
		r = Random_NextFloat01(&game_state->effects_rng);
		particles->angle[i] = angle + jitter * r * PI32;
		particles->rotation[i] = jitter * r * PI32;

		particles->time_to_live[i] = seconds_to_live;
	}
	particles->live_count = particle_count;
	particles->initial_force = initial_velocity;
	particles->seconds_to_live = seconds_to_live;
	particles->sprite = sprite;
}


COLLISION_RULE_HANDLER(PlayerCollisionHandler) {
	Entity * entity_a = GetEntity(game_state, a);
	Entity * entity_b = GetEntity(game_state, b);
	assert(entity_a->type == EntityType_Player);

	switch (entity_b->type) {
	case EntityType_Powerup:
		{
			PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, a);
			TransformComponent * player_transform = GetTransformFromEntity(game_state, a);
			PowerupComponent * powerup = (PowerupComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Powerup, b);
			if (powerup->type == PowerupType_Shield) {
				bool had_shield = ((player->active_powerups & PowerupType_Shield) != 0);
				player->active_powerups |= powerup->type;

				SpawnTextEntity(game_state, player_transform->position, "Shield!", COLOR_LIGHT_TEAL);

				if (!had_shield) {
					PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_ShieldPickup);
					CircleRendererComponent * shield_renderer = (CircleRendererComponent *)AddComponent(game_state, ComponentTypeID_CircleRenderer, a);
					shield_renderer->layer = RenderLayer_Foreground;
					shield_renderer->target = RenderTargetBuffer_Main;
					shield_renderer->zorder = ZOrder_Player + 1;
					CircleRendererComponent * shield_emission_renderer = (CircleRendererComponent *)AddComponent(game_state, ComponentTypeID_CircleRenderer, a);
					shield_emission_renderer->layer = RenderLayer_Foreground;
					shield_emission_renderer->target = RenderTargetBuffer_Emission;
					shield_emission_renderer->zorder = ZOrder_Player + 1;
				}
				game_state->player_stats.shields_picked_up++;
			}
			else if (powerup->type == PowerupType_Health) {
				if (player->health < 9) {
					player->health++;
				}

				SpawnTextEntity(game_state, player_transform->position, "Health!", COLOR_LIGHT_BLUE);
				game_state->player_stats.health_picked_up++;
			}

			KillEntity(game_state, entity_b);
		}
		break;
	case EntityType_Projectile:
		{
			PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, a);
			TransformComponent * player_transform = GetTransformFromEntity(game_state, a);
			ProjectileComponent * projectile = (ProjectileComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Projectile, b);
			TransformComponent * projectile_transform = GetTransformFromEntity(game_state, b);
			if (projectile->owner != EntityType_Player) {
				if (!(player->active_powerups & PowerupType_Shield)) {
					player->health--;
					game_state->player_stats.damage_taken++;
					game_state->screen_shake = max(4.0f, game_state->screen_shake);
					PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_HitSound);
				}
				else {
					// Shielded
					vec2 normal = normalize(projectile_transform->position - player_transform->position);
					SpawnParticleEmitter(game_state, NULL, 12, projectile_transform->position, Vec2(0.0f, 120.0f), Vec2(60.0f, 50.0f), 0.5f, normal);
					PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_ShieldBreak);
					game_state->screen_shake = max(2.0f, game_state->screen_shake);
				}
				player->hit_this_frame = true;

				SpawnTextEntity(game_state, player_transform->position, "Hit!", COLOR_RED);
				game_state->current_streak = 0;

				if (player->health <= 0) {
					ForceSetTimer(game_state, Timer_GameOver, 2000.0f);
					vec2 player_velocity = player->velocity * 0.5f;

					SpawnParticleEmitter(game_state, game_state->debris_sprite, 32, player_transform->position, player_velocity + Vec2(0.0f, 120.0f), Vec2(100.0f, 50.0f), 0.5f, Vec2(1.0f, 0.0f));
					SpawnPlayerDebris(game_state);

					PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_Explosion);

					KillEntity(game_state, entity_a);
					game_state->player_handle = NULL_ENTITY;
					game_state->screen_shake = max(8.0f, game_state->screen_shake);
				}

				KillEntity(game_state, entity_b);
			}
		}
		break;
	case EntityType_LaserHit:
		{
			// TODO(bryan):  Merge the common code (player hit / player killed) in
			// these two handlers.  
			PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, a);
			TransformComponent * player_transform = GetTransformFromEntity(game_state, a);
			if ((player->active_powerups & PowerupType_Shield) != 0) {
				// Player is shielded.
				TransformComponent * hit_transform = GetTransformFromEntity(game_state, b);
				vec2 normal = normalize(hit_transform->position - player_transform->position);
				SpawnParticleEmitter(game_state, NULL, 12, hit_transform->position, Vec2(0.0f, 120.0f), Vec2(60.0f, 50.0f), 0.5f, normal);

				game_state->player_stats.damage_taken++;
				player->health--;
				PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_ShieldBreak);
			}
			else {
				game_state->player_stats.damage_taken += 2;
				player->health -= 2;
				PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_HitSound);
			}
			player->hit_this_frame = true;
			game_state->screen_shake = max(4.0f, game_state->screen_shake);

			SpawnTextEntity(game_state, player_transform->position, "Hit!", COLOR_RED);
			game_state->current_streak = 0;

			if (player->health <= 0) {
				ForceSetTimer(game_state, Timer_GameOver, 2000.0f);
				vec2 player_velocity = player->velocity * 0.5f;

				SpawnParticleEmitter(game_state, game_state->debris_sprite, 32, player_transform->position, player_velocity + Vec2(0.0f, 120.0f), Vec2(100.0f, 50.0f), 0.5f, Vec2(1.0f, 0.0f));
				SpawnPlayerDebris(game_state);

				PlaySFXBark(game_state->sound_buffer, player_transform->position, 1.0f, GEN_Explosion);

				KillEntity(game_state, entity_a);
				game_state->player_handle = NULL_ENTITY;
				game_state->screen_shake = max(8.0f, game_state->screen_shake);
			}

			KillEntity(game_state, entity_b);
		}
		break;
	default:
		assert(0);
	}
}

static bool
PlayerNeedsDropType(GameState * game_state, PowerupType drop_type) {
	if (game_state->player_handle == NULL_ENTITY) return false;
	PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, game_state->player_handle);
	if (drop_type == PowerupType_Shield) {
		if (player->active_powerups & PowerupType_Shield) {
			return false;
		}
		else {
			return true;
		}
	}
	else if (drop_type == PowerupType_Health) {
		return player->health < 9;
	}

	return false;
}

EntityHandle SpawnPowerupEntity(GameState *, PowerupType, vec2);

COLLISION_RULE_HANDLER(InvaderCollisionHandler) {
	Entity * entity_a = GetEntity(game_state, a);
	Entity * entity_b = GetEntity(game_state, b);
	assert(entity_a->type == EntityType_Invader);

	switch (entity_b->type) {
	case EntityType_Projectile:
		{
			InvaderComponent * invader = (InvaderComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Invader, a);
			TransformComponent * invader_transform = GetTransformFromEntity(game_state, a);
			ProjectileComponent * projectile = (ProjectileComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Projectile, b);
			if (projectile->owner != EntityType_Invader) {
				game_state->screen_shake = max(8.0f, game_state->screen_shake);
				SpawnParticleEmitter(game_state, game_state->debris_sprite, 16, invader_transform->position, Vec2(0.0f, 50.0f), Vec2(175.0f, 100.0f), 1.0f, Vec2(1.0f, 0.0f));
				PlaySFXBark(game_state->sound_buffer, invader_transform->position, 1.0f, GEN_Explosion);

				if (invader->drop_type) {
					if (PlayerNeedsDropType(game_state, invader->drop_type)) {
						SpawnPowerupEntity(game_state, invader->drop_type, invader_transform->position);
					}
				}

				game_state->current_streak++;
				game_state->current_combo++;

				game_state->player_stats.best_streak = max(game_state->current_streak, game_state->player_stats.best_streak);

				ForceSetTimer(game_state, Timer_Combo, game_state->combo_interval);

				{
					char point_buf[64];

					u32 points = 100 + 50 * (game_state->current_combo - 1);
					game_state->score += points;

					_snprintf(point_buf, array_count(point_buf), "+%d", points);
					SpawnTextEntity(game_state, invader_transform->position, point_buf, COLOR_YELLOW);
				}
				if ((game_state->current_streak % 5) == 0 && (game_state->player_handle != NULL_ENTITY)) {
					char streak_buf[64];
					_snprintf(streak_buf, array_count(streak_buf), "STREAK %d", game_state->current_streak);

					TransformComponent * player_tranform = GetTransformFromEntity(game_state, game_state->player_handle);

					SpawnTextEntity(game_state, player_tranform->position, streak_buf, COLOR_WHITE);

					u32 point_bonus = 100 * (game_state->current_streak / 5);
					game_state->score += point_bonus;
					_snprintf(streak_buf, array_count(streak_buf), "+%d", point_bonus);
					SpawnTextEntity(game_state, player_tranform->position - Vec2(0.0f, 2.0f * 12.0f), streak_buf, COLOR_YELLOW);

				}

				game_state->player_stats.shots_hit++;
				game_state->player_stats.kills++;

				KillEntity(game_state, entity_a);
				KillEntity(game_state, entity_b);
			}
		}
		break;
	default:
		assert(0);
	}
}

const float SAUCER_LASER_TIMEOUT_MS = 3500.0f;
const float SAUCER_LASER_CHARGE_MS = 2000.0f;
const float SAUCER_LASER_FIRE_WAIT_MS = 250.0f;

const u32 SAUCER_CHARGE_BONUS = 1000;

COLLISION_RULE_HANDLER(SaucerCollisionHandler) {
	Entity * entity_a = GetEntity(game_state, a);
	Entity * entity_b = GetEntity(game_state, b);
	assert(entity_a->type == EntityType_Saucer);

	switch (entity_b->type) {
	case EntityType_Projectile:
		{
			SaucerComponent * saucer = (SaucerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Saucer, a);
			ProjectileComponent * projectile = (ProjectileComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Projectile, b);
			if (projectile->owner != EntityType_Invader) {
				game_state->screen_shake = max(4.0f, game_state->screen_shake);

				saucer->health_remaining--;
				game_state->player_stats.shots_hit++;
				if (saucer->health_remaining > 0) {
					TransformComponent * saucer_transform = GetTransformFromEntity(game_state, a);
					PlaySFXBark(game_state->sound_buffer, saucer_transform->position, 1.0f, GEN_HitSound);
				}
				else {
					// TODO(bryan):  Should saucer kills count toward combo / streak?  It causes
					// issues with the Perfect / Full Combo messages.  Also may allow for chaining
					// combos between levels.  (Though, that may be worth keeping for the skill
					// involved?)
//					game_state->current_streak++;
//					game_state->current_combo++;
//					game_state->player_stats.best_streak = max(game_state->current_streak, game_state->player_stats.best_streak);
					game_state->player_stats.kills++;
					game_state->player_stats.saucer_kills++;
//					ForceSetTimer(game_state, Timer_Combo, game_state->combo_interval);

					game_state->screen_shake = max(8.0f, game_state->screen_shake);
					{
						TransformComponent * saucer_transform = GetTransformFromEntity(game_state, a);
						char point_buf[64] = {};

						// TODO(bryan):  Bonus points for killing a saucer as it's charging.
						// Closer to firing, higher bonus.
						u32 points = 500 + 75 * (game_state->current_combo - 1);

						float charge_percent = saucer->laser_fire_wait / SAUCER_LASER_FIRE_WAIT_MS;
						u32 bonus = (u32)roundf(SAUCER_CHARGE_BONUS * charge_percent);
						game_state->score += points;

						_snprintf(point_buf, array_count(point_buf), "+%d", points);
						SpawnTextEntity(game_state, saucer_transform->position, point_buf, COLOR_YELLOW);
						PlaySFXBark(game_state->sound_buffer, saucer_transform->position, 0.75f, GEN_Explosion);

						SpawnParticleEmitter(game_state, game_state->debris_sprite, 16, saucer_transform->position, Vec2(0.0f, 50.0f), Vec2(175.0f, 100.0f), 1.0f, Vec2(1.0f, 0.0f));
						if (PlayerNeedsDropType(game_state, PowerupType_Health)) {
							SpawnPowerupEntity(game_state, PowerupType_Health, saucer_transform->position);
						}
					}
#if 0
					if ((game_state->current_streak % 5) == 0 && (game_state->player_handle != NULL_ENTITY)) {
						TransformComponent * player_transform = GetTransformFromEntity(game_state, game_state->player_handle);
						char streak_buf[64];
						_snprintf(streak_buf, array_count(streak_buf), "STREAK %d", game_state->current_streak);
						vec2 player_pos = player_transform->position;
						SpawnTextEntity(game_state, player_pos, streak_buf, COLOR_WHITE);

						u32 point_bonus = 100 * (game_state->current_streak / 5);
						game_state->score += point_bonus;
						_snprintf(streak_buf, array_count(streak_buf), "+%d", point_bonus);
						SpawnTextEntity(game_state, player_pos - Vec2(0.0f, 2.0f * 12.0f), streak_buf, COLOR_YELLOW);
					}
#endif
					KillEntity(game_state, saucer->glow_entity);
					KillEntity(game_state, entity_a);
				}

				KillEntity(game_state, entity_b);
			}
		}
		break;
	default:
		assert(0);
	}
}

static SpriteRendererComponent *
AddSpriteRenderer(GameState * game_state, EntityHandle handle, Sprite * sprite, vec4 color=COLOR_WHITE, RenderLayer layer=RenderLayer_Foreground, u32 zorder=ZOrder_Player) {
	SpriteRendererComponent * sprite_renderer = (SpriteRendererComponent *)AddComponent(game_state, ComponentTypeID_SpriteRenderer, handle);
	sprite_renderer->sprite = sprite;
	sprite_renderer->color = color;
	sprite_renderer->layer = layer;
	sprite_renderer->target = RenderTargetBuffer_Main;

	return sprite_renderer;
}

static SpriteRendererComponent *
AddSpriteRendererEmission(GameState * game_state, EntityHandle handle, Sprite * sprite, vec4 color = COLOR_WHITE, RenderLayer layer = RenderLayer_Foreground, u32 zorder = ZOrder_Player) {
	SpriteRendererComponent * sprite_renderer = (SpriteRendererComponent *)AddComponent(game_state, ComponentTypeID_SpriteRenderer, handle);
	sprite_renderer->sprite = sprite;
	sprite_renderer->color = color;
	sprite_renderer->layer = layer;
	sprite_renderer->target = RenderTargetBuffer_Emission;

	return sprite_renderer;
}

static void
SpawnTextEntity(GameState * game_state, vec2 position, char * text, vec4 color) {
	EntityHandle handle = AddEntity(game_state, EntityType_TextEntity);

	AddTransform(game_state, handle, position);

	TextRendererComponent * text_renderer = (TextRendererComponent *)AddComponent(game_state, ComponentTypeID_TextRenderer, handle);
	strncpy(text_renderer->string, text, array_count(text_renderer->string));
	text_renderer->time_to_live = 1.0f;
	text_renderer->color = color;
	text_renderer->layer = RenderLayer_Foreground;
	text_renderer->target = RenderTargetBuffer_Main;
	text_renderer->zorder = ZOrder_Player + 2;

	TextRendererComponent * emission_renderer = (TextRendererComponent *)AddComponent(game_state, ComponentTypeID_TextRenderer, handle);
	strncpy(emission_renderer->string, text, array_count(emission_renderer->string));
	emission_renderer->time_to_live = 1.0f;
	emission_renderer->color = color;
	emission_renderer->layer = RenderLayer_Foreground;
	emission_renderer->target = RenderTargetBuffer_Emission;
	text_renderer->zorder = ZOrder_Player + 2;
}

static void
SpawnPlayerDebris(GameState * game_state) {
	TransformComponent * player_transform = (TransformComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Transform, game_state->player_handle);

	EntityHandle debris[3] = {};
	for (u32 i = 0; i < array_count(debris); ++i) {
		debris[i] = AddEntity(game_state, EntityType_PlayerDebris);

		float angle = 2.0f * PI32 * Random_NextFloat01(&game_state->effects_rng);
		AddTransform(game_state, debris[i], player_transform->position, Vec2(1.0f, 1.0f), Vec2(cosf(angle), sinf(angle)));

		PlayerDebrisComponent * debris_component = (PlayerDebrisComponent *)AddComponent(game_state, ComponentTypeID_PlayerDebris, debris[i]);
		debris_component->velocity = NextRandVec2() * 75.0f;
		debris_component->time_to_live = 3.0f;
		
		AddSpriteRenderer(game_state, debris[i], game_state->player_debris_sprite + i);
	}
}

#define INVADER_FIRE_TIMEOUT (7.0f)
#define INVADER_FIRE_WAIT (5.0f)

static EntityHandle
SpawnInvader(GameState * game_state, vec2 position, PowerupType drop_type, float fire_timeout_remaining) {
	EntityHandle handle = AddEntity(game_state, EntityType_Invader);

	AddTransform(game_state, handle, position);

	AddCircleCollider(game_state, handle, 30.0f);

	InvaderComponent * invader = (InvaderComponent *)AddComponent(game_state, ComponentTypeID_Invader, handle);
	invader->fire_timeout_remaining = fire_timeout_remaining;
	invader->fire_wait_remaining = INVADER_FIRE_WAIT;
	invader->drop_type = drop_type;

	float anim_length = 0.25f; // Seconds
	float t = Random_NextFloat01(&game_state->effects_rng) * anim_length;

	AnimatedSpriteRendererComponent * anim_sprite = (AnimatedSpriteRendererComponent *)AddComponent(game_state, ComponentTypeID_AnimatedSpriteRenderer, handle);
	anim_sprite->sprite_array = game_state->invader_sprite;
	anim_sprite->sprite_count = 2;
	anim_sprite->frame_time = anim_length / anim_sprite->sprite_count;
	anim_sprite->cur_time = t;
	anim_sprite->color = COLOR_WHITE;
	anim_sprite->layer = RenderLayer_Foreground;
	anim_sprite->target = RenderTargetBuffer_Main;
	anim_sprite->zorder = ZOrder_Player;

	AnimatedSpriteRendererComponent * emit_sprite = (AnimatedSpriteRendererComponent *)AddComponent(game_state, ComponentTypeID_AnimatedSpriteRenderer, handle);
	emit_sprite->sprite_array = game_state->invader_emission_sprite;
	emit_sprite->sprite_count = 2;
	emit_sprite->frame_time = anim_length / emit_sprite->sprite_count;
	emit_sprite->cur_time = t;
	emit_sprite->color = COLOR_WHITE;
	emit_sprite->layer = RenderLayer_Foreground;
	emit_sprite->target = RenderTargetBuffer_Emission;
	emit_sprite->zorder = ZOrder_Player;

	return handle;
}

void
SetupInvaders(GameState * game_state, RandomState * rng_state) {
	float invaders_bounds_width = (game_state->invader_count_x - 1) * 96.0f + 64.0f;
	float invaders_bounds_height = (game_state->invader_count_y - 1) * 96.0f + 64.0f;

	Rect invader_bounds = RectFromCenterHalfExtent(Vec2(0.0f, 150.0f), Vec2(invaders_bounds_width * 0.5f, invaders_bounds_height * 0.5f));

	// one health, two shields
	u32 drop_index[3] = {};
	Random_ChooseN(rng_state, drop_index, 3, game_state->invader_count_x * game_state->invader_count_y);

	vec2 invader_base_pos = GetRectMin(invader_bounds) + Vec2(16.0f, 16.0f);
	for (u32 y = 0; y < game_state->invader_count_y; ++y) {
		for (u32 x = 0; x < game_state->invader_count_x; ++x) {
			u32 index = y * game_state->invader_count_x + x;
			vec2 position = invader_base_pos + Vec2(x * 96.0f, y * 96.0f);
			PowerupType drop_type = PowerupType_None;
			float fire_timeout_remaining = INVADER_FIRE_TIMEOUT * Random_NextFloat01(rng_state);
			if (index == drop_index[0] || index == drop_index[1] || index == drop_index[2]) {
				drop_type = PowerupType_Shield;
			}
			SpawnInvader(game_state, position, drop_type, fire_timeout_remaining); 
		}
	}
}

void
InitGameState(GameState * game_state, MemoryArena * game_memory) {
	assert(!game_state->initialized);
	game_state->menu_state.current_menu = MenuId_Main;
	game_state->menu_state.selected_item = 0;
	game_state->current_controller = ControllerIndex_Keyboard;
		
	// NOTE(bryan):  This can be removed once keyboard input is no longer polled.
	// On game startup, if the game is started by selecting the executable and 
	// pressing Enter, it will try to select the current menu item:  Start.
	// Stop that from happening with a timeout:
	ForceSetTimer(game_state, Timer_MenuTimeout, 1000.0f);

	game_state->entities = (Entity *)game_memory->AllocateZeroed(sizeof(Entity) * MAX_ENTITY_COUNT);
	game_state->player_handle = NULL_ENTITY;

	AddCollisionRule(game_state, EntityType_Player, EntityType_Powerup, PlayerCollisionHandler);
	AddCollisionRule(game_state, EntityType_Player, EntityType_Projectile, PlayerCollisionHandler);
	AddCollisionRule(game_state, EntityType_Player, EntityType_LaserHit, PlayerCollisionHandler);
	AddCollisionRule(game_state, EntityType_Invader, EntityType_Projectile, InvaderCollisionHandler);
	AddCollisionRule(game_state, EntityType_Saucer, EntityType_Projectile, SaucerCollisionHandler);

	ComponentSystem * cs = &game_state->component_system;
#define ALLOC_DYNAMIC_ARRAY(type, name, count)\
	cs->name ## s = (type *)game_memory->AllocateZeroed(sizeof(type) * count);\
	cs->name ## _count = 0;\
	cs->name ## _capacity = count;

	ALLOC_DYNAMIC_ARRAY(TransformComponent, transform, MAX_ENTITY_COUNT);
	ALLOC_DYNAMIC_ARRAY(ColliderComponent, collider, MAX_ENTITY_COUNT);
	ALLOC_DYNAMIC_ARRAY(SpriteRendererComponent, sprite_renderer, MAX_ENTITY_COUNT);
	ALLOC_DYNAMIC_ARRAY(CircleRendererComponent, circle_renderer, (MAX_ENTITY_COUNT / 2));
	ALLOC_DYNAMIC_ARRAY(RectangleRendererComponent, rectangle_renderer, (MAX_ENTITY_COUNT / 2));
	ALLOC_DYNAMIC_ARRAY(TextRendererComponent, text_renderer, (MAX_ENTITY_COUNT / 2));
	ALLOC_DYNAMIC_ARRAY(ParticleSystemComponent, particle_system, 25);
	ALLOC_DYNAMIC_ARRAY(AnimatedSpriteRendererComponent, animated_sprite_renderer, MAX_ENTITY_COUNT / 4);
	ALLOC_DYNAMIC_ARRAY(PlayerComponent, player, 1);
	ALLOC_DYNAMIC_ARRAY(PlayerDebrisComponent, player_debris, 3);
	ALLOC_DYNAMIC_ARRAY(PowerupComponent, powerup, 10);
	ALLOC_DYNAMIC_ARRAY(ProjectileComponent, projectile, (MAX_ENTITY_COUNT / 2));
	ALLOC_DYNAMIC_ARRAY(InvaderComponent, invader, 20);
	ALLOC_DYNAMIC_ARRAY(SaucerComponent, saucer, 10);
	ALLOC_DYNAMIC_ARRAY(LaserComponent, laser, MAX_ENTITY_COUNT / 2);
#undef ALLOC_DYNAMIC_ARRAY

	game_state->initialized = true;
	game_state->paused = true;
}

EntityHandle SpawnPlayer(GameState * game_state, vec2 position);
EntityHandle SpawnSaucerEntity(GameState *, vec2);

inline void
StartNextLevel(GameState * game_state) {
	SetupInvaders(game_state, &game_state->gen_rng);

	game_state->invader_move_dir = 1;
	game_state->level++;

	bool should_spawn_saucer = (game_state->level > 0 && (game_state->level % 5) == 0);
	should_spawn_saucer = should_spawn_saucer || (game_state->round_was_perfect);
	should_spawn_saucer = should_spawn_saucer || 
		(game_state->round_was_fc ? (Random_NextFloat01(&game_state->gen_rng)) >= 0.5f : false);
	if (should_spawn_saucer) {
		SpawnSaucerEntity(game_state, Vec2(0.0f, gScreenHeight * 0.5f - 100.0f));
	}

	game_state->round_was_fc = false;
	game_state->round_was_perfect = false;
}

void
ResetGameState(GameState * game_state, MemoryArena * game_memory) {
	memset(&game_state->player_stats, 0, sizeof(PlayerStats));
	
	memset(game_state->entities, 0, MAX_ENTITY_COUNT * sizeof(Entity));
	ClearComponents(game_state);

	game_state->player_handle = SpawnPlayer(game_state, Vec2(0.0f, -250.0f));

	game_state->invader_count_x = 10;
	game_state->invader_count_y = 2;

	Random_Seed(&game_state->effects_rng, 0xfeedabad39155573LL);
	Random_Seed(&game_state->gen_rng, 0x98397072abad1deaLL);

	for (u32 i = 0; i < array_count(game_state->timers); ++i) {
		game_state->timers[i].time_remaining = 0.0f;
	}

	game_state->invader_move_dir = 1;

	game_state->level = 0;
	game_state->score = 0;
	game_state->current_streak = 0;
	game_state->current_combo = 0;
	game_state->combo_interval = 1.5f * 1000.0f;

	game_state->screen_shake = 0.0f;

	StartNextLevel(game_state);
	game_state->initialized = true;
	game_state->paused = true;
}

static u32
GatherEntitiesOfType(GameState * game_state, EntityType type, EntityHandle * out_buffer, u32 out_buffer_count) {
	u32 used = 0;
	for (u32 i = 0; i < MAX_ENTITY_COUNT; ++i) {
		if (used >= out_buffer_count) break;

		Entity * e = game_state->entities + i;
		if (e->type == type && ((e->flags & EF_Alive) != 0)) {
			out_buffer[used++] = i;
		}
	}

	return used;
}

static void 
SpawnLaserEntity(GameState * game_state, vec2 shooter_position, vec2 target_position, EntityHandle parent) {
	EntityHandle handle = AddEntity(game_state, EntityType_Laser);

	vec2 target_vec = target_position - shooter_position;

	vec2 laser_start = shooter_position + Vec2(0.0f, -16.0f);
	vec2 laser_end = target_position;

	bool hit_player = false;
	if (game_state->player_handle != NULL_ENTITY) {
		// TODO(bryan):  ENGINE
		// Should probably pull raycast out into its own set of library 
		// functions.
		TransformComponent * player_transform = GetTransformFromEntity(game_state, game_state->player_handle);
		ColliderComponent * player_collider = (ColliderComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Collider, game_state->player_handle);

		// Raycast against player collider, see if we hit:
		float r = player_collider->circle.radius;

		vec2 d = target_vec;
		vec2 f = laser_start - player_transform->position;

		float a = dot(d, d);
		float b = 2.0f * dot(f, d);
		float c = dot(f, f) - (r * r);

		float discriminant = b*b - (4.0f * a * c);
		if (discriminant < 0) {
			// We missed the player, laser should continue off-screen.
			float screen_bottom_y = -0.5f * gScreenHeight;
			float ratio = (d.y - screen_bottom_y) / screen_bottom_y;
		
			laser_end.x = target_vec.x * ratio;
			laser_end.y = screen_bottom_y;
		}
		else {
			// We hit the player.  Recalculate length.
			hit_player = true;
			discriminant = sqrtf(discriminant);

			float t1 = (-b - discriminant) / (2.0f*a);
			float t2 = (-b + discriminant) / (2.0f*a);

			float t = min(t1, t2);

			laser_end = laser_start + d * t;
		}
	}

	target_vec = laser_end - laser_start;
	float target_vec_length = sqrtf(dot(target_vec, target_vec));

	vec2 position = (laser_end + laser_start) * 0.5f;

	target_vec /= target_vec_length;
	vec2 facing = perp(target_vec);  // Facing is to the right side of the rectangle.
	TransformComponent * transform = AddTransform(game_state, handle, position, Vec2(1.0f, 1.0f), facing);

	RectangleRendererComponent * rect_renderer = (RectangleRendererComponent *)AddComponent(game_state, ComponentTypeID_RectangleRenderer, handle);
	rect_renderer->size = Vec2(6.0f, target_vec_length);
	rect_renderer->color = COLOR_LIGHT_RED;
	rect_renderer->layer = RenderLayer_Foreground;
	rect_renderer->target = RenderTargetBuffer_Main;
	rect_renderer->zorder = ZOrder_Player - 2;

	RectangleRendererComponent * emission_renderer = (RectangleRendererComponent *)AddComponent(game_state, ComponentTypeID_RectangleRenderer, handle);
	emission_renderer->size = Vec2(8.0f, target_vec_length);
	emission_renderer->color = COLOR_LIGHT_RED;
	emission_renderer->layer = RenderLayer_Foreground;
	emission_renderer->target = RenderTargetBuffer_Emission;
	emission_renderer->zorder = ZOrder_Player - 2;

	LaserComponent * laser = (LaserComponent *)AddComponent(game_state, ComponentTypeID_Laser, handle);
	laser->time_to_live = 0.5f;

	if (hit_player) {
		// NOTE(bryan):  Because colliders are affected by the entity 
		// transform, we need to add a new entity to collide with the
		// player.  Dirty hack?  If melee attacks can be projectiles
		// I think I'm justified.  ;)
		EntityHandle hit_entity = AddEntity(game_state, EntityType_LaserHit);
		AddTransform(game_state, hit_entity, laser_end);
		AddCircleCollider(game_state, hit_entity, 5.0f);
		// TODO(bryan):  Particle burst
	}
}

#define MAX_SAUCERS (5)

const float SAUCER_TARGET_XS[] = {
	// For simplicity, screen mapped to range [-1, 1]
	-1.0f,
	-0.5f,
	-0.25f,
	0.0f,
	0.25f,
	0.5f,
	1.0f,
};

const float TARGET_X_EPSILON = 16.0f;

static bool
CollidesWithSaucerTargetX(GameState * game_state, float move_target_x) {
	for (u32 i = 0; i < game_state->component_system.saucer_count; ++i) {
		SaucerComponent * saucer = game_state->component_system.saucers + i;
		if (saucer->flags & ComponentFlag_Alive) {
			if (saucer->move_target_x - move_target_x < TARGET_X_EPSILON) {
				return true;
			}
		}
	}
	return false;
}

static float
GetSaucerMoveTargetX(GameState * game_state, SaucerComponent * saucer) {
	ColliderComponent * saucer_collider = (ColliderComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Collider, saucer->host_entity);
	float move_target_x = 0.0f;
	u32 iters_remaining = MAX_SAUCERS * MAX_SAUCERS;
	do {
		float base_target_x = SAUCER_TARGET_XS[(Random_Next(&game_state->gen_rng) % array_count(SAUCER_TARGET_XS))];
		// Ensure we don't move off-screen.
		move_target_x = base_target_x * ((0.5f * gScreenWidth) - 2.5f * saucer_collider->circle.radius);
		--iters_remaining;
	} while (iters_remaining > 0 && CollidesWithSaucerTargetX(game_state, move_target_x));

	return move_target_x;
}

static void
UpdateSaucers(GameState * game_state, float delta_ms) {
	ComponentSystem * component_system = &game_state->component_system;
	for (u32 i = 0; i < component_system->saucer_count; ++i) {
		SaucerComponent * saucer = component_system->saucers + i;
		
		if ((saucer->flags & ComponentFlag_Alive) == 0) {
			continue;
		}

		TransformComponent * saucer_transform = GetTransformFromEntity(game_state, saucer->host_entity);
		saucer->laser_fire_timeout -= delta_ms;
		if (saucer->laser_fire_timeout <= 0.0f && saucer->move_moving_time <= 0.0f) {
			saucer->laser_charge_wait += delta_ms;
			if (saucer->laser_charge_wait > SAUCER_LASER_CHARGE_MS) {
				saucer->laser_fire_wait += delta_ms;
				if (saucer->laser_fire_wait > SAUCER_LASER_FIRE_WAIT_MS && 
					saucer->move_moving_time <= 0.0f) // Only fire when stopped.
				{
					vec2 target_vec = saucer->target - saucer_transform->position;

					SpawnLaserEntity(game_state, saucer_transform->position, saucer->target, saucer->host_entity);
					PlaySFXBark(game_state->sound_buffer, saucer_transform->position, 1.0f, GEN_LaserFire);

					saucer->laser_charge_wait = 0.0f;
					saucer->laser_fire_wait = 0.0f;
					saucer->laser_fire_timeout = SAUCER_LASER_TIMEOUT_MS;
				}
			}
			else {
				// Not charged, target player
				if (game_state->player_handle != NULL_ENTITY) {
					TransformComponent * player_transform = GetTransformFromEntity(game_state, game_state->player_handle);
					saucer->target = player_transform->position;
				}
			}
		}

		// Movement
		{
			// Saucer should pick one of a few different set positions, wait there for a few seconds, 
			// and then move to another.
			const float SAUCER_MOVE_WAIT_MS = 1250.0f;
			const float SAUCER_MOVE_SPEED = 120.0f / 1000.0f;  // Pixels/second

			if (saucer->move_moving_time >= 0.0f) {
				saucer->move_moving_time -= delta_ms;

				float move_dx = saucer->move_target_x - saucer->move_last_x;
				float a = saucer->move_last_x;
				float b = saucer->move_last_x + 0.35f * move_dx;
				float c = saucer->move_target_x - 0.35f * move_dx;
				float d = saucer->move_target_x;

				float t = (saucer->move_moving_time * SAUCER_MOVE_SPEED) / abs(move_dx);
				t = 1.0f - clamp(t, 0.0f, 1.0f);
				saucer_transform->position.x = Bezier(a, b, c, d, t);

				if (saucer->move_moving_time <= 0.0f) {
					saucer->move_wait_timeout = SAUCER_MOVE_WAIT_MS;
				}
			}
			else {
				saucer->move_wait_timeout -= delta_ms;
				if (saucer->move_wait_timeout <= 0.0f && saucer->laser_charge_wait <= 0.0f) {
					saucer->move_last_x = saucer_transform->position.x;
					saucer->move_target_x = GetSaucerMoveTargetX(game_state, saucer);
					saucer->move_moving_time = (1.0f / SAUCER_MOVE_SPEED) * abs(saucer_transform->position.x - saucer->move_target_x);
				}
			}
		}

		if (saucer->glow_entity) {
			TransformComponent * glow_trans = GetTransformFromEntity(game_state, saucer->glow_entity);
			glow_trans->position = saucer_transform->position + Vec2(0.0f, -12.0f);

			CircleRendererComponent * glow_render = (CircleRendererComponent *)GetComponentFromEntity(game_state, ComponentTypeID_CircleRenderer, saucer->glow_entity);
			glow_render->color.a = clamp(saucer->laser_charge_wait / SAUCER_LASER_CHARGE_MS, 0.0f, 1.0f);

			if (glow_render->color.a == 1.0f) {
				float s = sinf(16.0f * saucer->laser_fire_wait);
				s += 1.0f;
				s *= 0.5f;
				glow_render->color.a -= s;
			}
			glow_render->color.a *= glow_render->color.a;
		}
	}
}

void
MoveInvaders(GameState * game_state, GameInput * game_input, RenderCommandBuffer * render_buffer) {

	u32 live_invader_count = game_state->component_system.invader_count;

	// TODO(bryan):  This doesn't have to live in here anymore, it's not an iteration now.
	if (!live_invader_count) {
		TrySetTimer(game_state, Timer_NextLevel, 2000.0f);

		if (WaitTimer(game_state, game_input->delta_time, Timer_NextLevel)) {
			StartNextLevel(game_state);
		}
	}
	else {
		TransformComponent * transform = GetTransformFromEntity(game_state, game_state->component_system.invaders[0].host_entity);
		vec2 real_min = transform->position;
		vec2 real_max = real_min;
		for (u32 i = 0; i < live_invader_count; ++i) {
			transform = GetTransformFromEntity(game_state, game_state->component_system.invaders[i].host_entity);

			vec2 pos = transform->position;

			real_min.x = min(real_min.x, pos.x);
			real_min.y = min(real_min.y, pos.y);

			real_max.x = max(real_max.x, pos.x);
			real_max.y = max(real_max.y, pos.y);
		}
		real_min -= Vec2(16.0f, 16.0f);
		real_max += Vec2(16.0f, 16.0f);

		Rect invader_bounds = RectFromMinMax(real_min, real_max);

		const float INVADER_MOVE_SPEED = 64.0f;  // Pixels/sec
	
		vec2 old_center = invader_bounds.center;
		float delta_seconds = game_input->delta_time / 1000.0f;
		float invader_total_move_dist = INVADER_MOVE_SPEED * delta_seconds;

		while (invader_total_move_dist > 0.0f) {
			float dist_moved = 0.0f;
			vec2 screen_min = Vec2(-0.5f * gScreenWidth + 16.0f, -0.5f * gScreenHeight);
			vec2 screen_max = Vec2( 0.5f * gScreenWidth - 16.0f,  0.5f * gScreenHeight);

			if (game_state->invader_move_dir > 0) {
				dist_moved = min(screen_max.x - real_max.x, invader_total_move_dist);
			}
			else {
				dist_moved = min(real_min.x - screen_min.x, invader_total_move_dist);
			}

			invader_total_move_dist -= dist_moved;
			invader_bounds.center += Vec2(game_state->invader_move_dir * dist_moved, 0.0f);
			if (dist_moved < invader_total_move_dist) {
				// We hit the edge, need to bounce:
				game_state->invader_move_dir *= -1;
			}
		}

		vec2 position_delta = invader_bounds.center - old_center;
		for (u32 i = 0; i < live_invader_count; ++i) {
			transform = GetTransformFromEntity(game_state, game_state->component_system.invaders[i].host_entity);
			transform->position += position_delta;
		}
	}
}

inline EntityHandle
SpawnPowerupEntity(GameState * game_state, PowerupType type , vec2 position) {
	EntityHandle handle = AddEntity(game_state, EntityType_Powerup);

	AddTransform(game_state, handle, position);

	ColliderComponent * collider = AddCircleCollider(game_state, handle, 5.0f);

	PowerupComponent * powerup = (PowerupComponent *)AddComponent(game_state, ComponentTypeID_Powerup, handle);
	powerup->type = type;
	Sprite * sprite = (type == PowerupType_Shield) ? game_state->shield_pickup_sprite : game_state->health_pickup_sprite;
	AddSpriteRenderer(game_state, handle, sprite, COLOR_WHITE, RenderLayer_Foreground, ZOrder_Player - 1);

	CircleRendererComponent * emission_renderer = (CircleRendererComponent *)AddComponent(game_state, ComponentTypeID_CircleRenderer, handle);
	emission_renderer->outer_radius = collider->circle.radius;
	emission_renderer->inner_radius = 0.0f;
	emission_renderer->color = (type == PowerupType_Shield) ? COLOR_LIGHT_TEAL : COLOR_LIGHT_BLUE * 0.75f;
	emission_renderer->layer = RenderLayer_Foreground;
	emission_renderer->target = RenderTargetBuffer_Emission;
	emission_renderer->zorder = ZOrder_Player - 1;

	return handle;
}

inline EntityHandle
SpawnProjectileEntity(GameState * game_state, EntityType owner, vec2 position, vec2 velocity) {
	EntityHandle handle = AddEntity(game_state, EntityType_Projectile);

	AddTransform(game_state, handle, position);

	AddCircleCollider(game_state, handle, 4.0f);

	ProjectileComponent * projectile = (ProjectileComponent *)AddComponent(game_state, ComponentTypeID_Projectile, handle);
	projectile->owner = owner;
	projectile->velocity = velocity;

	SpriteRendererComponent * sprite_renderer = AddSpriteRenderer(game_state, handle, owner == EntityType_Player ? game_state->lazer_sprite : game_state->plasma_sprite, COLOR_WHITE, RenderLayer_Foreground, ZOrder_Player - 2);

	AddSpriteRendererEmission(game_state, handle, sprite_renderer->sprite + 1, COLOR_WHITE, RenderLayer_Foreground, ZOrder_Player - 2);

	return handle;
}

inline EntityHandle
SpawnSaucerEntity(GameState * game_state, vec2 position) {
	EntityHandle handle = AddEntity(game_state, EntityType_Saucer);

	ColliderComponent * collider = AddCircleCollider(game_state, handle, 25.0f);

	SaucerComponent * saucer = (SaucerComponent *)AddComponent(game_state, ComponentTypeID_Saucer, handle);
	saucer->health_remaining = 2;
	saucer->laser_fire_timeout = SAUCER_LASER_TIMEOUT_MS;
	saucer->laser_charge_wait = 0.0f;
	saucer->laser_fire_wait = 0.0f;

	float move_target_x = GetSaucerMoveTargetX(game_state, saucer);

	saucer->move_wait_timeout = 1.0f;
	saucer->move_moving_time = 0.0f;
	saucer->move_target_x = move_target_x;
	saucer->move_last_x = move_target_x;
	saucer->target = Vec2();

	TransformComponent * transform = AddTransform(game_state, handle, position);
	transform->position.x = move_target_x;
	transform->scale.y = 0.5f;

	AnimatedSpriteRendererComponent * anim_sprite = (AnimatedSpriteRendererComponent *)AddComponent(game_state, ComponentTypeID_AnimatedSpriteRenderer, handle);
	anim_sprite->sprite_array = game_state->saucer_sprite;
	anim_sprite->sprite_count = 7;
	float anim_length = 1.5f; // Seconds
	anim_sprite->frame_time = anim_length / anim_sprite->sprite_count;
	anim_sprite->cur_time = 0.0f;
	anim_sprite->color = COLOR_WHITE;
	anim_sprite->layer = RenderLayer_Foreground;
	anim_sprite->target = RenderTargetBuffer_Main;
	anim_sprite->zorder = ZOrder_Player + 2;

	AnimatedSpriteRendererComponent * emit_sprite = (AnimatedSpriteRendererComponent *)AddComponent(game_state, ComponentTypeID_AnimatedSpriteRenderer, handle);
	emit_sprite->sprite_array = game_state->saucer_emission_sprite;
	emit_sprite->sprite_count = 7;
	emit_sprite->frame_time = anim_length / emit_sprite->sprite_count;
	emit_sprite->cur_time = 0.0f;
	emit_sprite->color = COLOR_WHITE;
	emit_sprite->layer = RenderLayer_Foreground;
	emit_sprite->target = RenderTargetBuffer_Emission;
	emit_sprite->zorder = ZOrder_Player + 2;

	// Glow entity
	{
		EntityHandle glow_entity = AddEntity(game_state, EntityType_SaucerGlow);

		TransformComponent * glow_transform = AddTransform(game_state, glow_entity, transform->position);
		glow_transform->scale = Vec2(0.5f, 0.25f);

		CircleRendererComponent * glow_render = (CircleRendererComponent *)AddComponent(game_state, ComponentTypeID_CircleRenderer, glow_entity);
		glow_render->outer_radius = 25.0f;
		glow_render->inner_radius = 0.0f;
		glow_render->color = COLOR_LIGHT_RED;
		glow_render->color.a = 0.0f;
		glow_render->layer = RenderLayer_Background;
		glow_render->target = RenderTargetBuffer_Emission;

		saucer->glow_entity = glow_entity;
	}

	return handle;
}

static EntityHandle
SpawnPlayer(GameState * game_state, vec2 position) {
	EntityHandle handle = AddEntity(game_state, EntityType_Player);

	AddTransform(game_state, handle, position);

	AddCircleCollider(game_state, handle, 24.0f);

	PlayerComponent * player = (PlayerComponent *)AddComponent(game_state, ComponentTypeID_Player, handle);
	player->velocity = Vec2();
	player->health = 3;
	player->fire_state = 0;
	player->active_powerups = 0;
	player->hit_this_frame = false;

	AddSpriteRenderer(game_state, handle, game_state->player_sprite);
	AddSpriteRendererEmission(game_state, handle, game_state->player_emission_sprite);

	return handle;
}

inline void
KillEntity(GameState * game_state, EntityHandle handle) {
	Entity * e = GetEntity(game_state, handle);
	KillEntity(game_state, e);
}

inline void
KillEntity(GameState * game_state, Entity * e) {
	assert(e != NULL);
	assert(e->flags & EF_Alive);

	if (e->component_count > 0) {
		ClearEntityComponents(game_state, e);
	}

	e->flags = 0;
	e->component_count = 0;
}

inline bool
EntityIsAlive(GameState * game_state, EntityHandle handle) {
	assert(handle < MAX_ENTITY_COUNT);
	Entity * e = &game_state->entities[handle];

	return (e->flags & EF_Alive) != 0;
}

void 
GameUpdate(GameState * game_state, GameInput * game_input, RenderCommandBuffer * render_buffer, SoundCommandBuffer * sound_buffer, MemoryArena * game_memory) {
	if (!game_state->initialized) {
		InitGameState(game_state, game_memory);
	}

	game_state->sound_buffer = sound_buffer;

	float delta_seconds = game_input->delta_time / 1000.0f;

	if (!ControllerIsLive(game_input, game_state->current_controller)) {
		// TODO(bryan):  Rather than track "current" controller, just check for active
		// controllers that are unplugged?
		// Controller got unplugged from under us.
		SwitchMenu(MenuId_Main, game_state);
		game_state->paused = true;
		// Fall back to keyboard
		game_state->current_controller = 0;
	}

	if (InputButtonWentDown(game_input, GameVirtualInputs_MenuEscape)) {
		SwitchMenu(MenuId_Main, game_state);
		game_state->paused = true;
	}

	vec4 bg_shade = Vec4(0.75f, 0.75f, 0.75f, 1.0f);
	RenderSprite(render_buffer, game_state->background_sprite, Vec2(), bg_shade, Vec2(1.0f, 0.0f), RenderLayer_Background);
	RenderSpriteEmission(render_buffer, game_state->background_emission, Vec2(), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_Background);

	if (game_state->menu_state.current_menu != MenuId_None) {
		RunMenu(game_state, game_input, render_buffer, sound_buffer, game_memory);
		return;
	}

	// Player movement
	vec2 delta_position = Vec2();
	if (InputButtonIsDown(game_input, GameVirtualInputs_Left)) {
		delta_position += Vec2(-1.0f, 0.0f);
	}
	if (InputButtonIsDown(game_input, GameVirtualInputs_Right)) {
		delta_position += Vec2(1.0f, 0.0f);
	}

	if (game_state->current_controller > 0) {
		delta_position.x = InputAxis(game_input, GameVirtualInputs_HorizontalAxis);
	}

	if (game_state->player_handle != NULL_ENTITY) {
		PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, game_state->player_handle);
		player->velocity = Vec2();

		TransformComponent * player_transform = GetTransformFromEntity(game_state, game_state->player_handle);
		if (magnitude(delta_position) >= 0.01f) {
			vec2 player_velocity = normalize(delta_position) * 1250.0f * 0.5f;
			player->velocity = player_velocity;
			player_transform->position += player_velocity * delta_seconds;
			float player_max_x = (0.5f * gScreenWidth - 34.0f);
			player_transform->position.x = max(min(player_transform->position.x, player_max_x), -player_max_x);
		}

		player->hit_this_frame = false;

		// Player action input
		if (game_state->timers[Timer_NextLevel].time_remaining <= 0.0f) {
			if (InputButtonWentDown(game_input, GameVirtualInputs_Fire)) {
				player->fire_state++;
				if (player->fire_state > 2) { player->fire_state = 1; }

				float offset_x;
				if (player->fire_state == 1) {
					offset_x = 18.0f;
				}
				else {
					offset_x = -18.0f;
				}

				game_state->player_stats.shots_fired++;

				SpawnProjectileEntity(game_state, EntityType_Player, player_transform->position + Vec2(offset_x, 0.0f), Vec2(0.0f, 650.0f));
				PlaySFXBark(sound_buffer, player_transform->position, 1.0f, GEN_Shoot);
			}
		}
	}
	else {
		// Player is dead.
		if (WaitTimer(game_state, game_input->delta_time, Timer_GameOver)) {
			SwitchMenu(MenuId_GameOver, game_state);
			game_state->paused = true;
		}
	}

	DoCollisionAndDispatch(game_state, render_buffer);
	ClearDeadComponents(game_state);

	{
		for (u32 i = 0; i < game_state->component_system.projectile_count; ++i) {
			ProjectileComponent * projectile = game_state->component_system.projectiles + i;
			if ((projectile->flags & ComponentFlag_Alive) == 0) {
				continue;
			}

			TransformComponent * projectile_transform = GetTransformFromEntity(game_state, projectile->host_entity);
			projectile_transform->position += projectile->velocity * delta_seconds;

			if (projectile_transform->position.y < -0.5f * gScreenHeight ||
				projectile_transform->position.y > 0.5f * gScreenHeight) {
				KillEntity(game_state, projectile->host_entity);
				continue;
			}

			// Debug:  Draw colliders
			//			RenderCircle(render_buffer, projectile_transform->position, 5.0f, COLOR_WHITE, 1.0f, RenderLayer_UI);

			if (projectile->owner == EntityType_Invader) {
				if (game_state->player_handle != NULL_ENTITY) {
					PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, game_state->player_handle);
					TransformComponent * player_transform = GetTransformFromEntity(game_state, game_state->player_handle);

					bool player_has_shield = ((player->active_powerups & PowerupType_Shield) != 0);
					vec2 vec_to_player = player_transform->position - projectile_transform->position;
					float dist_squared = dot(vec_to_player, vec_to_player);
					float v_seek_dist = player_has_shield ? 500.0f : 300.0f;
					float h_seek_dist = player_has_shield ? 150.0f : 100.0f;

					if (abs(vec_to_player.x) < h_seek_dist && abs(vec_to_player.y) < v_seek_dist) {
						// Close to player, try to track.
						vec2 dir_to_player = vec_to_player / sqrtf(dist_squared);
						float speed = magnitude(projectile->velocity);
						vec2 velocity_dir = projectile->velocity / speed;
						
						// cos(t) > 0.5
						// t is within 60 degrees of the reference angle
						float dot_player_vel = dot(dir_to_player, velocity_dir);
						if (dot_player_vel > 0.5f) {
							vec2 new_velocity_dir = (velocity_dir) + (dir_to_player * 0.025f * dot_player_vel);
							velocity_dir = normalize(new_velocity_dir);
							projectile->velocity = velocity_dir * speed;
							projectile_transform->rotation = Vec2(-velocity_dir.y, velocity_dir.x);
						}
					}
				}
			}
		}
	}

	MoveInvaders(game_state, game_input, render_buffer);

	for (u32 i = 0; i < game_state->component_system.invader_count; ++i) {
		InvaderComponent * invader = game_state->component_system.invaders + i;
		if (EntityIsAlive(game_state, invader->host_entity)) {
			invader->fire_timeout_remaining -= delta_seconds;
			if (invader->fire_timeout_remaining <= 0.0f) {
				invader->fire_wait_remaining -= delta_seconds;
				TransformComponent * transform = GetTransformFromEntity(game_state, invader->host_entity);

				bool fire = false;
				if (invader->fire_wait_remaining <= 0.0f) {
					// Held shot too long, fire anyway.
					fire = true;
				}
				else if (game_state->player_handle != NULL_ENTITY) {
					TransformComponent * player_transform = GetTransformFromEntity(game_state, game_state->player_handle);
					if (abs(player_transform->position.x - transform->position.x) < 20.0f) {
						// Player under us, open fire!
						fire = true;
					}
				}

				if (fire) {
					invader->fire_timeout_remaining = INVADER_FIRE_TIMEOUT;
					invader->fire_wait_remaining = INVADER_FIRE_WAIT;
					SpawnProjectileEntity(game_state, EntityType_Invader, transform->position, Vec2(0.0f, -500.0f));
					float vol = 0.7f + 0.3f * Random_NextFloat01(&game_state->effects_rng);
					PlaySFXBark(game_state->sound_buffer, transform->position, vol, GEN_PlasmaFire);
				}
			}
		}
	}

	// Update lasers
	{
		for (u32 i = 0; i < game_state->component_system.laser_count; ++i) {
			LaserComponent * laser = game_state->component_system.lasers + i;

			TransformComponent * laser_transform = GetTransformFromEntity(game_state, laser->host_entity);
			laser_transform->scale.x = Lerp(0.0f, 1.0f, laser->time_to_live);
			laser->time_to_live -= delta_seconds;
			if (laser->time_to_live <= 0.0f) {
				KillEntity(game_state, laser->host_entity);
			}
		}
	}

	UpdateSaucers(game_state, game_input->delta_time);

	// Reset combo meter
	if (WaitTimer(game_state, game_input->delta_time, Timer_Combo)) {
		game_state->current_combo = 0;
	}

	if (game_state->current_combo > 1) {

		float alpha = (game_state->timers[Timer_Combo].time_remaining / game_state->combo_interval);
		float scale = 4.0f;

		vec2 offset = Vec2();
		char combo_buf[64];
 		if (game_state->current_combo == 20) {
			scale += 1.0f - alpha;
			alpha *= 2.0f;
			static char full_combo[] = "20x FULL COMBO";
			static char perfect[] = "20x PERFECT";
			if (game_state->current_streak >= 20) {
				game_state->round_was_perfect = true;
				strncpy(combo_buf, perfect, array_count(combo_buf) - 1);
			}
			else {
				game_state->round_was_fc = true;
				strncpy(combo_buf, full_combo, array_count(combo_buf) - 1);
			}
			offset = Vec2(-0.5f * UITextWidth(combo_buf) * scale, 0.5f * scale * 12.0f);
		}
		else {
			scale -= alpha*-0.5f;
			_snprintf(combo_buf, array_count(combo_buf), "%dx", game_state->current_combo);
			offset = Vec2(-0.5f * UITextWidth(combo_buf) * scale, 0.5f * scale * 12.0f);
		}
		alpha = clamp(alpha, 0.0f, 1.0f);

		vec4 color = COLOR_WHITE;
		color.a = alpha;
		RenderText(render_buffer, offset, combo_buf, Vec2(scale, scale), color, RenderLayer_UI);
		RenderTextEmission(render_buffer, offset, combo_buf, Vec2(scale, scale), color, RenderLayer_UI);
	}

	if (game_state->player_handle != NULL_ENTITY) {
		PlayerComponent * player = (PlayerComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Player, game_state->player_handle);
		ColliderComponent * player_collider = (ColliderComponent *)GetComponentFromEntity(game_state, ComponentTypeID_Collider, game_state->player_handle);

		CircleRendererComponent * shield_renderers[2];
		u32 count = GetComponentsOfTypeFromEntity(game_state, ComponentTypeID_CircleRenderer, game_state->player_handle, (void **)shield_renderers, array_count(shield_renderers));

		if (count == 2) {
			vec4 color = COLOR_LIGHT_TEAL;
			if (player->active_powerups & PowerupType_Shield)
			{
				player_collider->circle.radius = Lerp(player_collider->circle.radius, 32.0f, 5.0f * delta_seconds);

				if (player->hit_this_frame) {
					player->active_powerups ^= PowerupType_Shield;
					player->shield_fade_time = PLAYER_SHIELD_FADE_SECONDS;
				}
			}
			else {
				player_collider->circle.radius = Lerp(player_collider->circle.radius, 24.0f, 5.0f * delta_seconds);
			}

			if (player->shield_fade_time > 0.0f) {
				player->shield_fade_time -= delta_seconds;

				color = COLOR_RED;
				color.a = player->shield_fade_time / PLAYER_SHIELD_FADE_SECONDS;

				if (player->shield_fade_time <= 0.0f) {
					player_collider->circle.radius = 24.0f;

					RemoveComponent(game_state, ComponentTypeID_CircleRenderer, shield_renderers[1]);
					RemoveComponent(game_state, ComponentTypeID_CircleRenderer, shield_renderers[0]);
				}
			}
			shield_renderers[0]->outer_radius = player_collider->circle.radius;
			shield_renderers[0]->inner_radius = shield_renderers[0]->outer_radius - 1.5f;
			shield_renderers[0]->color = color;
			shield_renderers[0]->target = RenderTargetBuffer_Main;

			shield_renderers[1]->outer_radius = player_collider->circle.radius;
			shield_renderers[1]->inner_radius = shield_renderers[1]->outer_radius - 2.0f;
			shield_renderers[1]->color = color;
			shield_renderers[1]->target = RenderTargetBuffer_Emission;
		}
		// Fake some "ambient occlusion" of the background emission.  This should (hopefully) make the player easier to see against the lights.
//		RenderCircle(render_buffer, player->position, player->collider.circle.radius, COLOR_BLACK * 0.75f, 0.0f, RenderLayer_Background);
	}

	// Powerup update loop
	for (u32 i = 0; i < game_state->component_system.powerup_count; ++i) {
		PowerupComponent * powerup = game_state->component_system.powerups + i;
		if ((powerup->flags & ComponentFlag_Alive) == 0) {
			continue;
		}
		TransformComponent * transform = GetTransformFromEntity(game_state, powerup->host_entity);
		transform->position += Vec2(0.0f, -100.0f) * delta_seconds;
		if (transform->position.y < -0.5f * gScreenHeight) {
			KillEntity(game_state, powerup->host_entity);
		}
	}

	// Particle update and render
	{
		for (u32 system_idx = 0; system_idx < game_state->component_system.particle_system_count; ++system_idx) {
			ParticleSystemComponent * particles = game_state->component_system.particle_systems + system_idx;
			if ((particles->flags & ComponentFlag_Alive) == 0) continue;

			TransformComponent * particles_transform = GetTransformFromEntity(game_state, particles->host_entity);

			for (u32 i = 0; i < particles->live_count; ++i) {
				// TODO(bryan):  ENGINE OPTIMIZE
				// Really good candidate here for a SIMD transform.
				// Not sure we'll have enough of these to warrant the effort for
				// Invaders, though.
				vec2 particle_position = {};
				particle_position.x = particles->pos_x[i] * particles_transform->rotation.x - particles->pos_y[i] * particles_transform->rotation.y;
				particle_position.y = particles->pos_x[i] * particles_transform->rotation.y + particles->pos_y[i] * particles_transform->rotation.x;
				particle_position += particles_transform->position;

				vec2 particle_rotation = {};
				particle_rotation.x = cosf(particles->angle[i]);
				particle_rotation.y = sinf(particles->angle[i]);

				vec4 color = COLOR_WHITE;
				color.a = particles->time_to_live[i] / particles->seconds_to_live;
				if (particles->sprite) {
					RenderSprite(render_buffer, particles->sprite, particle_position, color, particle_rotation, RenderLayer_Foreground);
				}
				else {
					// No sprite -> sparks
					// HACK
					TransformComponent transform = {};
					transform.position = particle_position;
					transform.rotation = particle_rotation;
					transform.scale = Vec2(1.0f, 1.0f);
					RenderRectangle(render_buffer, Vec2(2.0f, 8.0f), COLOR_LIGHT_RED, &transform, RenderLayer_Foreground);
					RenderRectangleEmission(render_buffer, Vec2(2.0f, 8.0f), COLOR_LIGHT_RED, &transform, RenderLayer_Foreground);
				}
			//	RenderCircle(render_buffer, particle_position, 10.0f, COLOR_WHITE, 0.0f, RenderLayer_Foreground);
			}

			UpdateParticles(game_state, particles, delta_seconds);
		}
	}

	for (u32 i = 0; i < game_state->component_system.animated_sprite_renderer_count; ++i) {
		AnimatedSpriteRendererComponent * anim = game_state->component_system.animated_sprite_renderers + i;
		if ((anim->flags & ComponentFlag_Alive) == 0) continue;

		anim->cur_time += delta_seconds;
		float anim_length = anim->frame_time * anim->sprite_count;
		if (anim->cur_time >= anim_length) {
			anim->cur_time -= anim_length;
		}

		u32 sprite_idx = (u32)(anim->cur_time / anim->frame_time);
		sprite_idx = clamp(sprite_idx, 0, anim->sprite_count);

		TransformComponent * transform = GetTransformFromEntity(game_state, anim->host_entity);
		switch (anim->target) {
		case RenderTargetBuffer_Main:
		{
			RenderSprite(render_buffer, anim->sprite_array + sprite_idx, transform->position, anim->color, transform->rotation, anim->layer);
		}
		break;
		case RenderTargetBuffer_Emission:
		{
			RenderSpriteEmission(render_buffer, anim->sprite_array + sprite_idx, transform->position, anim->color, transform->rotation, anim->layer);
		}
		break;
		default:
			assert(0);
		}
	}

	for (u32 i = 0; i < game_state->component_system.player_debris_count; ++i) {
		PlayerDebrisComponent * debris = game_state->component_system.player_debriss + i;
		if ((debris->flags & ComponentFlag_Alive) == 0) {
			continue;
		}
		TransformComponent * transform = GetTransformFromEntity(game_state, debris->host_entity);
		transform->position += debris->velocity * delta_seconds;
		float rot_angle = PI32 * delta_seconds;
		vec2 rotation = {};
		rotation.x = transform->rotation.x * cosf(rot_angle) - transform->rotation.y * sinf(rot_angle);
		rotation.y = transform->rotation.x * sinf(rot_angle) + transform->rotation.y * cosf(rot_angle);

		transform->rotation = rotation;
		debris->time_to_live -= delta_seconds;

		if (debris->time_to_live < 0.0f) {
			KillEntity(game_state, debris->host_entity);
		}
	}

	for (u32 i = 0; i < game_state->component_system.text_renderer_count; ++i) {
		TextRendererComponent * text_renderer = game_state->component_system.text_renderers + i;
		if ((text_renderer->flags & ComponentFlag_Alive) == 0) {
			continue;
		}
		TransformComponent * transform = GetTransformFromEntity(game_state, text_renderer->host_entity);

		float dx = sinf((1.0f - text_renderer->time_to_live) * PI32);
		dx = (dx * 2.0f) - 1.0f;

		transform->position += Vec2(dx, 100.0f * delta_seconds);
		float scale = 2.0f;
		float width = UITextWidth(text_renderer->string);

		vec2 draw_position = transform->position - Vec2(0.5f * width * scale, -12.0f);
		// Untextured objects get premultiplied in the shader, so don't do that here, just set alpha.
		vec4 draw_color = text_renderer->color;
		draw_color.a = text_renderer->time_to_live;

		RenderText(render_buffer, draw_position, text_renderer->string, Vec2(scale, scale), draw_color, RenderLayer_Foreground);
		RenderTextEmission(render_buffer, draw_position, text_renderer->string, Vec2(scale, scale), draw_color * 0.75f, RenderLayer_Foreground);

		text_renderer->time_to_live -= delta_seconds;
		if (text_renderer->time_to_live < 0.0f) {
			KillEntity(game_state, text_renderer->host_entity);
		}
	}

	for (u32 i = 0; i < game_state->component_system.rectangle_renderer_count; ++i) {
		RectangleRendererComponent * rect_renderer = game_state->component_system.rectangle_renderers + i;
		if ((rect_renderer->flags & ComponentFlag_Alive) == 0) {
			continue;
		}

		TransformComponent * transform = GetTransformFromEntity(game_state, rect_renderer->host_entity);

		switch (rect_renderer->target) {
		case RenderTargetBuffer_Main:
			{
				RenderRectangle(render_buffer, rect_renderer->size, rect_renderer->color, transform, rect_renderer->layer);
			}
			break;
		case RenderTargetBuffer_Emission:	
			{
				RenderRectangleEmission(render_buffer, rect_renderer->size, rect_renderer->color, transform, rect_renderer->layer);
			}
			break;
		default:
			assert(0);
		}
	}

	for (u32 i = 0; i < game_state->component_system.sprite_renderer_count; ++i) {
		SpriteRendererComponent * sprite_renderer = game_state->component_system.sprite_renderers + i;
		if ((sprite_renderer->flags & ComponentFlag_Alive) == 0) continue;
		TransformComponent * transform = GetTransformFromEntity(game_state, sprite_renderer->host_entity);
		switch (sprite_renderer->target) {
		case RenderTargetBuffer_Main:
			{
				RenderSprite(render_buffer, sprite_renderer->sprite, transform->position, sprite_renderer->color, transform->rotation, sprite_renderer->layer);
			}
			break;
		case RenderTargetBuffer_Emission:
			{
				RenderSpriteEmission(render_buffer, sprite_renderer->sprite, transform->position, sprite_renderer->color, transform->rotation, sprite_renderer->layer);
			}
			break;
		default:
			assert(0);
		}
	}
	
	for (u32 i = 0; i < game_state->component_system.circle_renderer_count; ++i) {
		CircleRendererComponent * circle_renderer = game_state->component_system.circle_renderers + i;
		if ((circle_renderer->flags & ComponentFlag_Alive) == 0) continue;
		TransformComponent * transform = GetTransformFromEntity(game_state, circle_renderer->host_entity);
		switch (circle_renderer->target) {
		case RenderTargetBuffer_Main:
			{
				float thickness = circle_renderer->outer_radius - circle_renderer->inner_radius;
				RenderCircle(render_buffer, circle_renderer->outer_radius, circle_renderer->color, transform, thickness, circle_renderer->layer);
			}
			break;
		case RenderTargetBuffer_Emission:
			{
				float thickness = circle_renderer->outer_radius - circle_renderer->inner_radius;
				RenderCircleEmission(render_buffer, circle_renderer->outer_radius, circle_renderer->color, transform, thickness, circle_renderer->layer);
			}
			break;
		default:
			assert(0);
		}
	}
	{
		char score_buf[64] = {};
		_snprintf(score_buf, array_count(score_buf), "Score: %04d", game_state->score);
		RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(16.0f, 16.0f)), score_buf, Vec2(5.0f, 5.0f));

		_snprintf(score_buf, array_count(score_buf), "Level: %03d", game_state->level);
		RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(400.0f, 16.0f)), score_buf, Vec2(5.0f, 5.0f));

		if (game_state->player_handle == NULL_ENTITY) {
			_snprintf(score_buf, array_count(score_buf), "Health: %1d", 0);
		}
		else {
			ComponentHandle player_component_handle = {};
			player_component_handle.type_id = ComponentTypeID_Player;
			player_component_handle.index = 0; // Assume to always be the first and only member of this component array.

			PlayerComponent * player = (PlayerComponent *)FetchComponent(&game_state->component_system, player_component_handle);
			_snprintf(score_buf, array_count(score_buf), "Health: %1d", player->health);
		}
		RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(800.0f - 16.0f, 16.0f)), score_buf, Vec2(5.0f, 5.0f));
	}
#if 0
	{
		char frame_time_buf[60];
		_snprintf(frame_time_buf, array_count(frame_time_buf), "Frame time: %llums", game_state->DEBUG_last_frame_dt);
		vec4 color = game_state->DEBUG_last_frame_dt < 17 ? COLOR_WHITE : COLOR_LIGHT_RED;
		RenderText(render_buffer, WorldCoordFromScreenCoord(Vec2(20.0f, gScreenHeight - 36.0f)), frame_time_buf, Vec2(2.0f, 2.0f), color, RenderLayer_UI);
	}
#endif
	// Post effects
	SetPostEffectProperty(render_buffer, PostEffect_ScreenShakeIntensity, game_state->screen_shake * game_state->options.screenshake);
	if (game_state->screen_shake >= 0.0f) {
		game_state->screen_shake -= 10.0f * delta_seconds;
	}
	else {
		game_state->screen_shake = 0.0f;
	}

}
