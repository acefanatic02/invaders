//
// Invaders - game.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"
#include "rectangle.h"
#include "memory_arena.h"
#include "render_command.h"
#include "sound_command.h"
#include "random.h"
#include "component.h"

struct GameState;

enum MenuId {
	MenuId_None,
	MenuId_Main,
	MenuId_Controls,
	MenuId_Options,
	MenuId_Credits,
	MenuId_GameOver,
	MenuId_PlayerStats,
	MenuId_HighScores,
	MenuId_HighScoreNameEntry,
	MenuId_QuitConfirm,

	MenuId_count,
};

struct MenuState {
	MenuId current_menu;
	u32 selected_item;
	float anim_t;
	char text_entry_buffer[20];
};

enum {
	Timer_NextLevel,
	Timer_GameOver,
	Timer_MenuTimeout,
	Timer_Combo,

	Timer_count,
};

struct Timer {
	float time_remaining;
};

struct Sprite;

#define COLLISION_RULE_HANDLER(name) void name(GameState * game_state, EntityHandle a, EntityHandle b)
typedef COLLISION_RULE_HANDLER(CollisionRuleHandler);

// Collision rules are canonical.  Entities will always be passed in the
// same order.  Two rules may not be defined that cover the same collision
// with different orderings.
//
// ex:  Player / Projectile will always be passed with Player as the first
//      parameter.  Defining Projectile / Player would then be an error.
//
struct CollisionRule {
	EntityType a;
	EntityType b;
	CollisionRuleHandler * handler;
};
//	Post-game stats
//	- Kill count
//	- Accuracy (shots fired, shots hit, percentage)
//	- Best combo
//	- Best streak
//	- Damage taken
//	- Powerups picked up
//	- (Possibly do global stat tracking?)
struct PlayerStats {
	u32 kills;
	u32 saucer_kills;
	u32 shots_fired;
	u32 shots_hit;
	u32 best_streak;
	u32 damage_taken;
	u32 health_picked_up;
	u32 shields_picked_up;
};

struct GameOptions {
	bool mute;
	bool headphone_mix;
	float master_volume;
	float music_volume;
	float effect_volume;
	float screenshake;
};

#define HIGH_SCORE_MAX_NAME 20
struct HighScoreEntry {
	u32 score;
	u32 level;
	char name[HIGH_SCORE_MAX_NAME + 1];
};

#define HIGH_SCORE_COUNT 10
struct GameState {
	bool initialized;
	bool paused;
	u32 current_controller;
	MenuState menu_state;

	GameOptions options;
	HighScoreEntry high_scores[HIGH_SCORE_COUNT];
	SoundCommandBuffer * sound_buffer;
	RandomState gen_rng;
	RandomState effects_rng;

	Sprite * background_sprite;
	Sprite * background_emission;
	Sprite * invader_sprite;
	Sprite * invader_emission_sprite;
	Sprite * saucer_sprite;
	Sprite * saucer_emission_sprite;
	Sprite * player_sprite;
	Sprite * player_emission_sprite;
	Sprite * player_debris_sprite;
	Sprite * lazer_sprite;
	Sprite * plasma_sprite;
	Sprite * debris_sprite;
	Sprite * shield_pickup_sprite;
	Sprite * health_pickup_sprite;
	Sprite * button_sprite;
	Sprite * wide_button_sprite;

#define MAX_ENTITY_COUNT (512)
	Entity * entities;
	ComponentSystem component_system;

	EntityHandle player_handle;
	PlayerStats player_stats;

	u32 invader_count_x;
	u32 invader_count_y;
	s32 invader_move_dir;

	u32 level;
	u32 score;
	u32 current_streak;
	u32 current_combo;
	float combo_interval;
	bool round_was_fc;
	bool round_was_perfect;

	CollisionRule collision_table[EntityType_Count * EntityType_Count];
	u32 collision_table_count;

	Timer timers[Timer_count];
	
	float screen_shake;

	MemoryArena * scratch_arena;

	u64 DEBUG_last_frame_dt;
};

struct ButtonState {
	bool is_down;
	bool was_down;
};

struct AxisState {
	float value;
};

enum InputStateFlag {
	InputStateFlag_IsAxis = 1 << 0,
};

struct InputState {
	u32 flags;
	union {
		ButtonState button;
		AxisState axis;
	};
};

struct KeyboardState {
	u8 keys[256];
};

enum ControllerFlag {
	ControllerFlag_IsController = 1 << 0,
	ControllerFlag_IsAttached   = 1 << 1,
	ControllerFlag_IsActive     = 1 << 2,
};

enum GameVirtualInputs {
	GameVirtualInputs_Up = 0,
	GameVirtualInputs_Down,
	GameVirtualInputs_Left,
	GameVirtualInputs_Right,
	GameVirtualInputs_HorizontalAxis,
	GameVirtualInputs_VerticalAxis,
	GameVirtualInputs_Fire,
	GameVirtualInputs_MenuUp,
	GameVirtualInputs_MenuDown,
	GameVirtualInputs_MenuLeft,
	GameVirtualInputs_MenuRight,
	GameVirtualInputs_MenuSelect,
	GameVirtualInputs_MenuEscape,

	GameVirtualInputs_Count,
};

struct ControllerState {
	u32 flags;
	InputState input_states[GameVirtualInputs_Count];
};

enum ControllerIndex {
	ControllerIndex_Keyboard = 0,
	ControllerIndex_Gamepad0,
	ControllerIndex_Gamepad1,
	ControllerIndex_Gamepad2,
	ControllerIndex_Gamepad3,

	ControllerIndex_Count,
};

struct GameInput {
	float delta_time;
	u32 attached_controller_count;
	ControllerState controllers[ControllerIndex_Count];

	KeyboardState * previous;
	KeyboardState * current;
};

void GameUpdate(GameState * game_state, GameInput * game_input, RenderCommandBuffer * render_buffer, SoundCommandBuffer * sound_buffer, MemoryArena * game_memory);

void SwitchMenu(MenuId target, GameState * menu_state);

void ForceSetTimer(GameState * game_state, u32 timer_idx, float ms);