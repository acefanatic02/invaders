//
// Invaders - invaders.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <cmath>
#include <cstdint>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

#define U32_MAX ((1ULL << 32ULL) - 1ULL)

#ifdef BUILD_x64
#define SafeCastToU32(s) SafeCastToU32_(s)
#else
#define SafeCastToU32(s) (s)
#endif

#define GLSL(src) "#version 140\n\n" # src

#define PTR_BYTE_OFFSET(ptr, offset) ((char *)ptr + (offset))

#define PI32 (3.1415927f)

#define DEG2RAD(d) ((d / 180.0f) * PI32)

#ifndef NULL
#define NULL (0)
#endif

void Win32AssertHandler(char * assertion, char * file, u32 line, char * function);

#ifdef _DEBUG
#define assert(e) do { if(!(e)) __debugbreak(); } while(0)
#else
#define assert(e) do { if(!(e)) Win32AssertHandler(#e, __FILE__,  __LINE__, __FUNCTION__); } while(0)
#endif
#define array_count(a) (sizeof(a) / sizeof(a[0]))

inline u32
SafeCastToU32_(size_t s) {
	assert(s <= U32_MAX);
	return (u32)s;
}

#define Kilobytes(n) (n * 1024)
#define Megabytes(n) (Kilobytes(n) * 1024)
#define Gigabytes(n) (Megabytes(n) * 1024)

const int gScreenWidth = 1024;
const int gScreenHeight = 720;

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

#define clamp(n, a, b) (min(max(n,a), b))

inline int
sgnf(float f) {
	return (0 < f) - (f < 0);
}

inline float
Lerp(float a, float b, float t) {
	return a + (b-a) * t;
}

inline float
Bezier(float a, float b, float c, float d, float t) {
	float w0 = t;
	float w1 = (1.0f - t);

	return w1*w1*w1 * a + 3.0f * w1*w1*w0 * b + 3.0f * w1*w0*w0 * c + w0*w0*w0 * d;
}