//
// Invaders - loader.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>

#define GLEW_STATIC
#include <GL/glew.h>

#include "stb_image.h"

#include "stb_vorbis.c"

#include <xmmintrin.h>
#include <cstdlib>

#undef min
#undef max
#undef assert
#include "invaders.h"

#include "loader.h"

static bool
IsSpriteAsset(u32 id) {
    return id < AssetID_OggGameMusic;
}

static bool
IsOggAsset(u32 id) {
    return id == AssetID_OggGameMusic;
}

static Texture *
LoadTextureFromMemory(u8 * data_ptr, u32 data_size, MemoryArena * dst_arena, MemoryArena * scratch_arena, bool premultiply=true) {
    u32 width;
    u32 height;
    int comp;
#if 1
    float * pixels = stbi_loadf_from_memory(data_ptr, data_size, (s32 *)&width, (s32 *)&height, &comp, 4);
    if (!pixels) {
		const char * cause = stbi_failure_reason();
        assert(!"Failed to load texture.");
    }
#else
	MemoryArenaBookmark pixel_bookmark = scratch_arena->Bookmark();
	u8 * raw_pixels = stbi_load_from_memory(data_ptr, data_size, (s32 *)&width, (s32 *)&height, &comp, 4);
	float * pixels = (float *)scratch_arena->Allocate(sizeof(float) * width * height * 4);
	for (u32 i = 0; i < (width * height * 4); ++i) {
		pixels[i] = ((float)raw_pixels[i] / 255.0f);
	}
#endif

    if (comp == 4 && premultiply) {
        u32 pixel_count = width * height;
        for (u32 pixel = 0; pixel < pixel_count; ++pixel) {
            u32 offset = pixel * 4;
            float a = pixels[offset + 3];

            pixels[offset + 0] *= a;
            pixels[offset + 1] *= a;
            pixels[offset + 2] *= a;
        }
    }

    u32 texture_id;
    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 32);
	glGenerateMipmap(GL_TEXTURE_2D);

#if 1
	stbi_image_free(pixels);
#else
    stbi_image_free(raw_pixels);
#endif

    Texture * rv = (Texture *)dst_arena->Allocate(sizeof(Texture));
    rv->width = width;
    rv->height = height;
    rv->texture_id = texture_id;

#if 0
	scratch_arena->Reset(pixel_bookmark);
#endif
    return rv;
}

static SoundBuffer
LoadOggFromMemory(u8 * data_ptr, u32 data_size, MemoryArena * dst_arena, MemoryArena * scratch_arena) {
    SoundBuffer result = {};

    int error = 0;
    stb_vorbis * vorbis = stb_vorbis_open_memory(data_ptr, data_size, &error, NULL);

    stb_vorbis_info info = stb_vorbis_get_info(vorbis);
    u32 sample_count = stb_vorbis_stream_length_in_samples(vorbis);

    result.sample_count = sample_count;
    result.samples = (float *)dst_arena->AllocateZeroed((((result.sample_count + 7) / 8) * 8) * sizeof(float));
    result.play_position = 0;

    int read = stb_vorbis_get_samples_float(vorbis, 1, &result.samples, result.sample_count);

    assert(read == result.sample_count);

    stb_vorbis_close(vorbis);

    return result;
}

void 
LoadAssets(GameAssets * assets, MemoryArena * dst_arena, MemoryArena * scratch_arena) {
    MemoryArenaBookmark scratch_bookmark = scratch_arena->Bookmark();
	wchar_t * packfile_path = L"resources.dat";

    FILE * fp = NULL;
    errno_t error = _wfopen_s(&fp, packfile_path, L"rb");
    if (!fp) {
        assert(!"Failed to load asset file.");
    }

    // We go ahead and load the entire file -- it's not *that* large.
    fseek(fp, 0, SEEK_END);
    u32 byte_len = SafeCastToU32(ftell(fp));
    fseek(fp, 0, SEEK_SET);

    u8 * file_data = (u8 *)scratch_arena->AllocateZeroed(byte_len + 1);
    byte_len = SafeCastToU32(fread(file_data, 1, byte_len, fp));
    fclose(fp);

	u32 metadata_len = *(u32 *)file_data;
    u32 current_offset = 4;
    while (current_offset < byte_len && current_offset < metadata_len) {
        u32 id = *(u32 *)PTR_BYTE_OFFSET(file_data, current_offset);
        if (IsSpriteAsset(id)) {
            PackedSpriteSet * pack_sprite_set = (PackedSpriteSet *)PTR_BYTE_OFFSET(file_data, current_offset);
            current_offset += sizeof(PackedSpriteSet);

            u8 * texture_ptr = (u8 *)PTR_BYTE_OFFSET(file_data, pack_sprite_set->png_data_offset);
            Texture * texture = LoadTextureFromMemory(texture_ptr, pack_sprite_set->png_data_size, dst_arena, scratch_arena);

            SpriteSet * dst = &assets->assets[id].sprite_set;
            dst->count = pack_sprite_set->sprite_count;
            dst->sprites = (Sprite *)dst_arena->Allocate(sizeof(Sprite) * dst->count);

            for (u32 sprite_idx = 0; sprite_idx < dst->count; ++sprite_idx) {
                PackedSprite * pack_sprite = (PackedSprite *)PTR_BYTE_OFFSET(file_data, current_offset);
                current_offset += sizeof(PackedSprite);

                dst->sprites[sprite_idx].size = Vec2(pack_sprite->size_x, pack_sprite->size_y);
                dst->sprites[sprite_idx].uv_min = Vec2(pack_sprite->uv_min_x, pack_sprite->uv_min_y);
                dst->sprites[sprite_idx].uv_max = Vec2(pack_sprite->uv_max_x, pack_sprite->uv_max_y);
                dst->sprites[sprite_idx].texture = texture;
            }
        }
        else if (IsOggAsset(id)) {
            PackedOgg * pack_ogg = (PackedOgg *)PTR_BYTE_OFFSET(file_data, current_offset);
            current_offset += sizeof(PackedOgg);

            u8 * ogg_ptr = (u8 *)PTR_BYTE_OFFSET(file_data, pack_ogg->ogg_data_offset);
            assets->assets[id].music = LoadOggFromMemory(ogg_ptr, pack_ogg->ogg_data_size, dst_arena, scratch_arena);
        }
        else {
            assert(!"Invalid asset ID.");
        }
    }

    scratch_arena->Reset(scratch_bookmark);
}

inline bool iswhite(char ch) {
	return ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n';
}

static bool ReadLine(char ** line_start, char ** line_end, char ** file_cur) {
	char *cur = *file_cur;
	if (!*cur) {
		return false; // EOF
	}
 
	while (*cur == ' ' || *cur == '\t') {
		++cur;
	}
 
	*line_start = cur;
 
	char * end = cur;
	while (true) {
		char ch = *cur++;
		if (!ch) {
			--cur;  // Preserve terminating NUL
			break;
		}
		else if (ch == '\n') {
			break;
		}
		else if (!iswhite(ch)) {
			end = cur;
		}
	}
 
	*line_end = end;
	*file_cur = cur;
 
	return true;
}

static char * FindFirst(char * start, char * end, char c) {
    char * cur = start;
    while (cur != end) {
        if (*cur == c) {
            break;
        }
        cur++;
    }
    return cur;
}
static char * FindLast(char * start, char * end, char c) {
    char * cur = end;
    while (cur != start) {
        if (*cur == c) {
            break;
        }
        --cur;
    }
    return cur;
}

struct LoadedFile {
	u8 * data;
	u32 length;
};

LoadedFile
Win32LoadEntireFileFullPath(const wchar_t * path, MemoryArena * dst_arena) {
	LoadedFile rv = {};

	FILE * fp;
	errno_t error = _wfopen_s(&fp, path, L"rb");
	if (fp) {
		fseek(fp, 0, SEEK_END);
		u32 byte_len = SafeCastToU32(ftell(fp));
		fseek(fp, 0, SEEK_SET);

		rv.data = (u8 *)dst_arena->AllocateZeroed(byte_len);
		byte_len = SafeCastToU32(fread(rv.data, 1, byte_len, fp));
		rv.length = byte_len;
		fclose(fp);
	}
	return rv;
}

static void
TrimWhitespace(char ** start, char ** end) {
	while(iswhite(**start) && *start < *end) (*start)++;
	while(iswhite(**end) && *start < *end) (*end)--;
}

/*

OPTIONS FILE FORMAT:

Key/value pairs:

key=value

Unknown keys are ignored.

Repeated keys overwrite.

*/
void 
LoadGameOptions(GameState * game_state, MemoryArena * scratch_arena) {
	MemoryArenaBookmark scratch_bookmark = scratch_arena->Bookmark();
	wchar_t cfg_filename[] = L"options.cfg";
	LoadedFile file = Win32LoadEntireFileFullPath(cfg_filename, scratch_arena);
	if (file.data == NULL || file.length == 0) {
		game_state->options.headphone_mix = true;
		game_state->options.mute = false;
		game_state->options.master_volume = 1.0f;
		game_state->options.effect_volume = 1.0f;
		game_state->options.music_volume = 1.0f;
		game_state->options.screenshake = 0.5f;
	}
	else {
		char * line_start = NULL;
		char * line_end = NULL;
		char * file_cur = (char *)file.data;
		while (ReadLine(&line_start, &line_end, &file_cur)) {
			char * equals = FindFirst(line_start, line_end, '=');
			if (equals >= line_end) continue;

			// TODO: DEBUG THIS!!!
			char * key_start = line_start;
			char * key_end = equals;
			char * value_start = equals + 1;
			char * value_end = line_end;
			TrimWhitespace(&key_start, &key_end);
			TrimWhitespace(&value_start, &value_end);

			size_t key_len = key_end - key_start;

#define CMP_TOKEN(token, name, token_len) ((token_len == array_count(name) - 1) && (!strncmp(token, name, array_count(name) - 1)))
			if (CMP_TOKEN(key_start, "headphone_mix", key_len)) {
				game_state->options.headphone_mix = *value_start != '0';
			}
			if (CMP_TOKEN(key_start, "mute", key_len)) {
				game_state->options.mute = *value_start != '0';
			}
			if (CMP_TOKEN(key_start, "master_volume", key_len)) {
				u32 v = strtol(value_start, NULL, 10);
				game_state->options.master_volume = clamp((float)v / 100.0f, 0.0f, 100.0f);
			}
			if (CMP_TOKEN(key_start, "music_volume", key_len)) {
				u32 v = strtol(value_start, NULL, 10);
				game_state->options.music_volume = clamp((float)v / 100.0f, 0.0f, 100.0f);
			}
			if (CMP_TOKEN(key_start, "effect_volume", key_len)) {
				u32 v = strtol(value_start, NULL, 10);
				game_state->options.effect_volume = clamp((float)v / 100.0f, 0.0f, 100.0f);
			}
			if (CMP_TOKEN(key_start, "screenshake", key_len)) {
				u32 v = strtol(value_start, NULL, 10);
				game_state->options.screenshake = clamp((float)v / 100.0f, 0.0f, 100.0f);
			}
#undef CMP_TOKEN

		}
	}
	scratch_arena->Reset(scratch_bookmark);
}

void 
SaveGameOptions(GameState * game_state, MemoryArena * scratch_arena) {
	MemoryArenaBookmark scratch_bookmark = scratch_arena->Bookmark();
	wchar_t cfg_filename[] = L"options.cfg";
	FILE * file = _wfopen(cfg_filename, L"w");
	if (file) {
		fprintf(file, "headphone_mix=%d\n", game_state->options.headphone_mix ? 1 : 0);
		fprintf(file, "mute=%d\n", game_state->options.mute ? 1 : 0);
		fprintf(file, "master_volume=%d\n", (u32)(game_state->options.master_volume * 100.0f));
		fprintf(file, "music_volume=%d\n", (u32)(game_state->options.music_volume * 100.0f));
		fprintf(file, "effect_volume=%d\n", (u32)(game_state->options.effect_volume * 100.0f));
		fprintf(file, "screenshake=%d\n", (u32)(game_state->options.screenshake * 100.0f));

		fclose(file);
	}
	scratch_arena->Reset(scratch_bookmark);
}

static HighScoreEntry starting_high_scores[] = {
	{ 1000000, 0, "Millionaire" },
	{  750000, 0, "Ace Shot" },
	{  500000, 0, "Dont Miss" },
	{  250000, 0, "Defender" },
	{  100000, 0, "Saucer Hunter" },
	{   75000, 0, "Dodge The Lasers" },
	{   50000, 0, "Qualified" },
	{   25000, 0, "Intermediate" },
	{   10000, 0, "E For Effort" },
	{    5000, 0, "Beginner" },
	{    1000, 0, "On The List" },
};

void
LoadHighScores(GameState * game_state, MemoryArena * scratch_arena) {
	MemoryArenaBookmark scratch_bookmark = scratch_arena->Bookmark();
	wchar_t high_score_filename[] = L"highscores.txt";
	LoadedFile file = Win32LoadEntireFileFullPath(high_score_filename, scratch_arena);
	if (file.data == NULL || file.length == 0) {
		for (u32 i = 0; i < 10; ++i) {
			game_state->high_scores[i] = starting_high_scores[i];
		}
	}
	else {
		char * line_start = NULL;
		char * line_end = NULL;
		char * line_cur = (char *)file.data;

		u32 i = 0;

		// TODO(bryan):  BUGFIX:  Switch to using quoted tokens.
		// For some reason, loads are failing occasionally, likely due to 
		// empty names confusing the parser.
		while (ReadLine(&line_start, &line_end, &line_cur)) {
			char * space = FindLast(line_start, line_end, ' ');
			if (space <= line_start || space >= line_end) continue;

			char * name_start = line_start;
			char * name_end = space;
			char * score_start = space + 1;
			char * score_end = line_end;
			TrimWhitespace(&name_start, &name_end);
			TrimWhitespace(&score_start, &score_end);

			HighScoreEntry * entry = &game_state->high_scores[i];
			strncpy(entry->name, name_start, min(array_count(entry->name) - 1, (name_end - name_start) + 1));
			entry->score = strtol(score_start, NULL, 10);
			++i;
		}
	}

	scratch_arena->Reset(scratch_bookmark);
}

void
SaveHighScores(GameState * game_state, MemoryArena * scratch_arena) {
	MemoryArenaBookmark scratch_bookmark = scratch_arena->Bookmark();
	wchar_t high_score_filename[] = L"highscores.txt";
	FILE * file = _wfopen(high_score_filename, L"w");

	if (file) {
		for (u32 i = 0; i < array_count(game_state->high_scores); ++i) {
			fprintf(file, "%s %d\n", game_state->high_scores[i].name, game_state->high_scores[i].score);
		}
        fclose(file);
	}

	scratch_arena->Reset(scratch_bookmark);
}