//
// Invaders - loader.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"
#include "sound.h"
#include "memory_arena.h"
#include "game.h"
#include "asset.h"

struct Texture {
	u32 width;
	u32 height;

	u32 texture_id;
};

struct Sprite {
	vec2 size;
	vec2 uv_min;
	vec2 uv_max;

	Texture * texture;
	Texture * emission;
};

struct SpriteSet {
	u32 count;
	Sprite * sprites;
};

union Asset {
	SpriteSet sprite_set;
	SoundBuffer music;
};

struct GameAssets {
	Asset assets[AssetID_Count];
};

void LoadAssets(GameAssets * assets, MemoryArena * dst_arena, MemoryArena * scratch_arena);

void SaveGameOptions(GameState * game_state, MemoryArena * scratch_arena);
void LoadGameOptions(GameState * game_state, MemoryArena * scratch_arena);

void SaveHighScores(GameState * game_state, MemoryArena * scratch_arena);
void LoadHighScores(GameState * game_state, MemoryArena * scratch_arena);
