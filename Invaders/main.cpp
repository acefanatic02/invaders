//
// Invaders - main.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include <Windows.h>
#undef min
#undef max

#define GLEW_STATIC
#include <GL/glew.h>

#include "debug_console.h"

#include "invaders.h"
#include "memory_arena.h"
#include "vectypes.h"
#include "mat4x4.h"
#include "loader.h"
#include "rectangle.h"
#include "game.h"
#include "render_command.h"
#include "sound.h"
#include "sound_command.h"

#include "stb_easy_font.h"

// For render sort.
#include <algorithm>

#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cmath>

#include <xmmintrin.h>
#include <emmintrin.h>

#include <mmdeviceapi.h>
#include <audioclient.h>

#include <dsound.h>
//#include <Audiosessiontypes.h>

#include <xinput.h>

// Used to prevent rendering to the invalid framebuffer when window is minimized.
static bool gIsMinimized = false;

#define X_INPUT_GET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_STATE * pState)
typedef X_INPUT_GET_STATE(x_input_get_state);
X_INPUT_GET_STATE(XInputGetStateStub) {
	return ERROR_DEVICE_NOT_CONNECTED;
}

x_input_get_state * XInputGetState_ = XInputGetStateStub;
#define XInputGetState XInputGetState_

#define X_INPUT_SET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_VIBRATION * pVibration)
typedef X_INPUT_SET_STATE(x_input_set_state);
X_INPUT_SET_STATE(XInputSetStateStub) {
	return ERROR_DEVICE_NOT_CONNECTED;
}
x_input_set_state * XInputSetState_ = XInputSetStateStub;
#define XInputSetState XInputSetState_

#define X_INPUT_GET_CAPABILITIES(name) DWORD WINAPI name(DWORD dwUserIndex, DWORD dwFlags, XINPUT_CAPABILITIES * pCapabilities)
typedef X_INPUT_GET_CAPABILITIES(x_input_get_capabilities);
X_INPUT_GET_CAPABILITIES(XInputGetCapabilitiesStub) {
	return ERROR_DEVICE_NOT_CONNECTED;
}
x_input_get_capabilities * XInputGetCapabilities_ = XInputGetCapabilitiesStub;
#define XInputGetCapabilities XInputGetCapabilities_

static void
Win32InitXInput() {
	static char * xinput_dlls[] = {
		"Xinput1_4.dll",		// Win8
		"Xinput9_1_0.dll",		// Win8/7/Vista
		"Xinput1_3.dll",		// Earlier DirectX?
	};

	for (u32 i = 0; i < array_count(xinput_dlls); ++i) {
		HMODULE xinput_lib = LoadLibraryA(xinput_dlls[i]);
		if (xinput_lib) {
			XInputGetState = (x_input_get_state *)GetProcAddress(xinput_lib, "XInputGetState");
			if (!XInputGetState) {
				XInputGetState = XInputGetStateStub;
			}

			XInputSetState = (x_input_set_state *)GetProcAddress(xinput_lib, "XInputSetState");
			if (!XInputSetState) {
				XInputSetState = XInputSetStateStub;
			}

			XInputGetCapabilities = (x_input_get_capabilities *)GetProcAddress(xinput_lib, "XInputGetCapabilities");
			if (!XInputGetCapabilities) {
				XInputGetCapabilities = XInputGetCapabilitiesStub;
			}

			break;
		}
	}
}

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID lpcGuidDevice, LPDIRECTSOUND * ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

DIRECT_SOUND_CREATE(DirectSoundCreateStub) {
	return S_FALSE;
}

direct_sound_create * DirectSoundCreate_ = DirectSoundCreateStub;

static void
Win32InitDSoundLib() {
	HMODULE dsound_lib = LoadLibraryA("dsound.dll");
	if (dsound_lib) {
		DirectSoundCreate_ = (direct_sound_create *)GetProcAddress(dsound_lib, "DirectSoundCreate8");
		if (!DirectSoundCreate_) {
			DirectSoundCreate_ = DirectSoundCreateStub;
		}
	}
}

#define DirectSoundCreate DirectSoundCreate_

void
Win32AssertHandler(char * assertion, char * file, u32 line, char * function) {
	char buffer[512] = {};
	_snprintf_s(buffer, 511, "Assertion failed (%s:%s %u):\n %s", file, function, line, assertion);
	MessageBoxA(0, buffer, "Error", MB_OK);
}

static vec4 gScreenViewport = Vec4(0.0f, 0.0f, (float)gScreenWidth, (float)gScreenHeight);

#define SC_STRUCT(type) SoundCommand_##type
#define SC_ENUM(type) SoundCommandType_##type

enum SoundCommandType {
	SC_ENUM(Bark),
	SC_ENUM(OneShot),
	SC_ENUM(Music),
};

struct SC_STRUCT(Bark) {
	vec2 position;
	float volume;
	SFXHandle sfx;
};

struct SC_STRUCT(OneShot) {
	vec2 position;
	float volume;
	SFXHandle sfx;
};

struct SC_STRUCT(Music) {
	SFXHandle sfx;
};

void *
PushSize_(SoundCommandBuffer * buffer, u32 size, SoundCommandType type) {
	size += sizeof(type);

	assert(buffer->top + size <= buffer->size);

	void * ptr = PTR_BYTE_OFFSET(buffer->buffer, buffer->top);
	buffer->top += size;
	SoundCommandType * cmd_type = (SoundCommandType *)ptr;
	*cmd_type = type;
	ptr = PTR_BYTE_OFFSET(ptr, sizeof(*cmd_type));

	return ptr;
}

#define PushSoundCommand(buffer, type) ((SC_STRUCT(type) *)PushSize_(buffer, sizeof(SC_STRUCT(type)), SC_ENUM(type)))

void
PlaySFXBark(SoundCommandBuffer * buffer, vec2 position, float volume, SFXHandle sfx) {
	SC_STRUCT(Bark) * cmd = PushSoundCommand(buffer, Bark);

	cmd->position = position;
	cmd->volume = volume;
	cmd->sfx = sfx;
}

void
PlaySFXOneShot(SoundCommandBuffer * buffer, vec2 position, float volume, SFXHandle sfx) {
	SC_STRUCT(OneShot) * cmd = PushSoundCommand(buffer, OneShot);

	cmd->position = position;
	cmd->volume = volume;
	cmd->sfx = sfx;
}

void 
SetMusic(SoundCommandBuffer * buffer, SFXHandle handle) {
	SC_STRUCT(Music) * cmd = PushSoundCommand(buffer, Music);

	cmd->sfx = handle;
}

#define RC_STRUCT(type) RenderCommand_##type
#define RC_ENUM(type) RenderCommandType_##type

enum RenderCommandType {
	RC_ENUM(Rect),
	RC_ENUM(Circle),
	RC_ENUM(Sprite),
	RC_ENUM(Text),
	RC_ENUM(EffectProperty)
};
struct RenderCommandHeader {
	RenderCommandType type;
	RenderLayer layer;
	RenderTargetBuffer target;
	u32 zorder;
	u32 sort_key;
};

struct RC_STRUCT(Rect) {
	vec2 size;
	vec4 color;
	u32 transform;
};

struct RC_STRUCT(Circle) {
	float radius;
	vec4 color;
	float thickness;
	u32 transform;
};

struct RC_STRUCT(Sprite) {
	vec2 size;
	vec2 uv_min;
	vec2 uv_max;
	vec4 color;
	u32 transform;

	Texture * tex;
};

struct RC_STRUCT(Text) {
	vec4 color;
	u32 transform;

	char text[64];
};

struct RC_STRUCT(EffectProperty) {
	PostEffectProperty property;
	float value;
};

inline u32
MakeRenderSortKey(RenderCommandHeader * header) {
	u32 key = (u32)(header->layer)  << 26 |
	 		  (u32)(header->target) << 24 |
			  header->zorder;
	return key;
}

void *
PushSize_(RenderCommandBuffer * buffer, u32 size, RenderCommandType type, RenderLayer layer, RenderTargetBuffer target, u32 zorder) {
	size += sizeof(RenderCommandHeader);

 	assert(buffer->top + size <= buffer->size);

	void * ptr = PTR_BYTE_OFFSET(buffer->buffer, buffer->top);
	buffer->top += size;
	RenderCommandHeader * header = (RenderCommandHeader *)ptr;
	header->type = type;
	header->layer = layer;
	header->target = target;
	header->zorder = zorder;
	header->sort_key = MakeRenderSortKey(header);
	ptr = PTR_BYTE_OFFSET(ptr, sizeof(*header));

	return ptr;
}

#define PushCommand(buffer, type, layer, target, z) ((RC_STRUCT(type) *)PushSize_(buffer, sizeof(RC_STRUCT(type)), RC_ENUM(type), layer, target, z))

inline u32
AddRenderTransform(RenderCommandBuffer * buffer, vec2 position, vec2 scale, vec2 rotation) {
	assert(buffer->transforms.count < buffer->transforms.capacity);
	u32 index = buffer->transforms.count++;

	buffer->transforms.px[index] = position.x;
	buffer->transforms.py[index] = position.y;
	buffer->transforms.sx[index] = scale.x;
	buffer->transforms.sy[index] = scale.y;
	buffer->transforms.rx[index] = rotation.x;
	buffer->transforms.ry[index] = rotation.y;

	return index;
}

void
RenderRectangle(RenderCommandBuffer * buffer, vec2 size, vec4 color, TransformComponent * transform, RenderLayer layer, u32 zorder) {
	RC_STRUCT(Rect) * cmd = PushCommand(buffer, Rect, layer, RenderTargetBuffer_Main, zorder);

	cmd->size = size;
	cmd->color = color;
	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void 
RenderRectangleEmission(RenderCommandBuffer * buffer, vec2 size, vec4 color, TransformComponent * transform, RenderLayer layer, u32 zorder) {
	RC_STRUCT(Rect) * cmd = PushCommand(buffer, Rect, layer, RenderTargetBuffer_Emission, zorder);

	cmd->size = size;
	cmd->color = color;
	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void RenderOutlineRect(RenderCommandBuffer * buffer, vec2 size, vec4 color, float thickness, TransformComponent * transform, RenderLayer layer, u32 zorder);

void 
RenderCircle(RenderCommandBuffer * buffer, float radius, vec4 color, TransformComponent * transform, float thickness, RenderLayer layer, u32 zorder) {
	RC_STRUCT(Circle) * cmd = PushCommand(buffer, Circle, layer, RenderTargetBuffer_Main, zorder);

	cmd->radius = radius;
	cmd->thickness = thickness;
	cmd->color = color;
	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void 
RenderCircleEmission(RenderCommandBuffer * buffer, float radius, vec4 color, TransformComponent * transform, float thickness, RenderLayer layer, u32 zorder) {
	RC_STRUCT(Circle) * cmd = PushCommand(buffer, Circle, layer, RenderTargetBuffer_Emission, zorder);

	cmd->radius = radius;
	cmd->thickness = thickness;
	cmd->color = color;
	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void 
RenderSprite(RenderCommandBuffer * buffer, Sprite * sprite, TransformComponent * transform, vec4 color, RenderLayer layer, u32 zorder) {
	RC_STRUCT(Sprite) * cmd = PushCommand(buffer, Sprite, layer, RenderTargetBuffer_Main, zorder);

	cmd->size = sprite->size;
	cmd->uv_min = sprite->uv_min;
	cmd->uv_max = sprite->uv_max;
	cmd->color = color;
	cmd->tex = sprite->texture;

	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void 
RenderSpriteEmission(RenderCommandBuffer * buffer, Sprite * sprite, TransformComponent * transform, vec4 color, RenderLayer layer, u32 zorder) {
	RC_STRUCT(Sprite) * cmd = PushCommand(buffer, Sprite, layer, RenderTargetBuffer_Emission, zorder);

	cmd->size = sprite->size;
	cmd->uv_min = sprite->uv_min;
	cmd->uv_max = sprite->uv_max;
	cmd->color = color;
	cmd->tex = sprite->texture;

	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void 
RenderText(RenderCommandBuffer * buffer, char * string, TransformComponent * transform, vec4 color, RenderLayer layer, u32 zorder) {
	RenderCommand_Text * cmd = PushCommand(buffer, Text, layer, RenderTargetBuffer_Main, zorder);

	cmd->color = color;
	
	size_t string_len = strnlen(string, array_count(cmd->text) - 1);
	memcpy(cmd->text, string, string_len);
	cmd->text[string_len] = '\0';

	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void RenderTextEmission(RenderCommandBuffer * buffer, char * string, TransformComponent * transform, vec4 color, RenderLayer layer, u32 zorder) {
	RenderCommand_Text * cmd = PushCommand(buffer, Text, layer, RenderTargetBuffer_Emission, zorder);

	cmd->color = color;
	
	size_t string_len = strnlen(string, array_count(cmd->text) - 1);
	memcpy(cmd->text, string, string_len);
	cmd->text[string_len] = '\0';

	cmd->transform = AddRenderTransform(buffer, transform->position, transform->scale, transform->rotation);
}

void
RenderRectangle(RenderCommandBuffer * buffer, vec2 min, vec2 max, vec4 color, RenderLayer layer, u32 zorder) {
	RenderCommand_Rect * cmd = PushCommand(buffer, Rect, layer, RenderTargetBuffer_Main, zorder);
	
	cmd->size = max - min;
	cmd->color = color;

	vec2 pos = (max + min) * 0.5f;
	cmd->transform = AddRenderTransform(buffer, pos, Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f));
}

void
RenderRectangleEmission(RenderCommandBuffer * buffer, vec2 min, vec2 max, vec4 color, RenderLayer layer, u32 zorder) {
	RenderCommand_Rect * cmd = PushCommand(buffer, Rect, layer, RenderTargetBuffer_Emission, zorder);
	
	cmd->size = max - min;
	cmd->color = color;

	vec2 pos = (max + min) * 0.5f;
	cmd->transform = AddRenderTransform(buffer, pos, Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f));
}

void
RenderOutlineRect(RenderCommandBuffer * buffer, vec2 min, vec2 max, vec4 color, float thickness, RenderLayer layer, u32 zorder) {
	RenderRectangle(buffer, min, Vec2(max.x, min.y - thickness), color, layer);
	RenderRectangle(buffer, Vec2(min.x, max.y), Vec2(max.x, max.y + thickness), color, layer);
	RenderRectangle(buffer, min, Vec2(min.x + thickness, max.y), color, layer);
	RenderRectangle(buffer, Vec2(max.x - thickness, min.y), max, color, layer);
}

void
RenderCircle(RenderCommandBuffer * buffer, vec2 center, float radius, vec4 color, float thickness, RenderLayer layer, u32 zorder) {
	RenderCommand_Circle * cmd = PushCommand(buffer, Circle, layer, RenderTargetBuffer_Main, zorder);

	cmd->radius = radius;
	cmd->color = color;
	// thickness == radius draws a filled circle.
	cmd->thickness = thickness > 0.0f ? thickness : radius;

	cmd->transform = AddRenderTransform(buffer, center, Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f));
}

void
RenderCircleEmission(RenderCommandBuffer * buffer, vec2 center, float radius, vec4 color, float thickness, RenderLayer layer, u32 zorder) {
	RenderCommand_Circle * cmd = PushCommand(buffer, Circle, layer, RenderTargetBuffer_Emission, zorder);

	cmd->radius = radius;
	cmd->color = color;
	// thickness == radius draws a filled circle.
	cmd->thickness = thickness > 0.0f ? thickness : radius;

	cmd->transform = AddRenderTransform(buffer, center, Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f));
}

void
RenderSprite(RenderCommandBuffer * buffer, Sprite * sprite, vec2 position, vec4 color, vec2 rotation, RenderLayer layer, u32 zorder) {
	RenderCommand_Sprite * cmd = PushCommand(buffer, Sprite, layer, RenderTargetBuffer_Main, zorder);

	cmd->size = sprite->size;
	cmd->uv_min = sprite->uv_min;
	cmd->uv_max = sprite->uv_max;
	cmd->color = color;
	cmd->tex = sprite->texture;

	cmd->transform = AddRenderTransform(buffer, position, Vec2(1.0f, 1.0f), rotation);
}

void
RenderSpriteEmission(RenderCommandBuffer * buffer, Sprite * sprite, vec2 position, vec4 color, vec2 rotation, RenderLayer layer, u32 zorder) {
	RenderCommand_Sprite * cmd = PushCommand(buffer, Sprite, layer, RenderTargetBuffer_Emission, zorder);

	cmd->size = sprite->size;
	cmd->uv_min = sprite->uv_min;
	cmd->uv_max = sprite->uv_max;
	cmd->color = color;
	cmd->tex = sprite->texture;
	
	cmd->transform = AddRenderTransform(buffer, position, Vec2(1.0f, 1.0f), rotation);
}

void
RenderText(RenderCommandBuffer * buffer, vec2 position, char * string, vec2 scale, vec4 color, RenderLayer layer, u32 zorder) {
	RenderCommand_Text * cmd = PushCommand(buffer, Text, layer, RenderTargetBuffer_Main, zorder);

	cmd->color = color;
	
	size_t string_len = strnlen(string, array_count(cmd->text) - 1);
	memcpy(cmd->text, string, string_len);
	cmd->text[string_len] = '\0';

	cmd->transform = AddRenderTransform(buffer, position, scale, Vec2(1.0f, 0.0f));
}

void
RenderTextEmission(RenderCommandBuffer * buffer, vec2 position, char * string, vec2 scale, vec4 color, RenderLayer layer, u32 zorder) {
	RenderCommand_Text * cmd = PushCommand(buffer, Text, layer, RenderTargetBuffer_Emission, zorder);

	cmd->color = color;
	
	size_t string_len = strnlen(string, array_count(cmd->text) - 1);
	memcpy(cmd->text, string, string_len);
	cmd->text[string_len] = '\0';

	cmd->transform = AddRenderTransform(buffer, position, scale, Vec2(1.0f, 0.0f));
}

void 
SetPostEffectProperty(RenderCommandBuffer * buffer, PostEffectProperty prop, float value) {
	// TODO(bryan):  Should effects apply per individual layers?
	RenderCommand_EffectProperty * cmd = PushCommand(buffer, EffectProperty, RenderLayer_Background, RenderTargetBuffer_Main, 0);
	cmd->property = prop;
	cmd->value = value;
}

struct SOA_Mat2x3 {
	u32 capacity;
	u32 count;
	float * a0;
	float * a1;
	float * a2;
	float * b0;
	float * b1;
	float * b2;
};

// TODO(bryan):  ENGINE  FIXME
// The approximation in sin4f and cos4f is only accurate
// within the range (-pi, pi).  It quickly becomes 
// *very* inaccurate -- the error at 2pi is +/-8.
//
// Clearly the answer is to range-reduce angles when
// the transforms are packed.  In theory, 
// fmodf(a + PI, 2*PI) - PI should work, in practice
// it seems not.
//
// For Invaders it's kind of irrelevant, we're bound
// by the GPU render time anyway.  But later versions
// of the engine should address this.
//
inline __m128
sin4f(__m128 a) {
    // http://forum.devmaster.net/t/fast-and-accurate-sine-cosine/9648
	union {
		float f;
		u32 i;
	} mask;
	mask.i = 0x7fffffff;
	__m128 abs_mask = _mm_set1_ps(mask.f);
    __m128 B = _mm_set1_ps(4.0f / PI32);
    __m128 C = _mm_set1_ps(-4.0f / (PI32*PI32));
    __m128 P = _mm_set1_ps(0.225f);

	__m128 abs_a = _mm_and_ps(a, abs_mask);//_mm_castsi128_ps(_mm_abs_epi32(_mm_castps_si128(a)));

	//float y = B * a + C * a * abs(a);
	__m128 y0 = _mm_mul_ps(a, B);
	__m128 y1 = _mm_mul_ps(_mm_mul_ps(a, C), abs_a);
    __m128 y = _mm_add_ps(y0, y1);

	// Refine precision
	__m128 abs_y = _mm_and_ps(y, abs_mask);//_mm_castsi128_ps(_mm_abs_epi32(_mm_castps_si128(y)));
	y = _mm_add_ps(_mm_mul_ps(P, _mm_sub_ps(_mm_mul_ps(y, abs_y), y)), y);

    return y;
}

inline __m128
cos4f(__m128 a) {
    __m128 HALF_PI = _mm_set1_ps(0.5f * PI32);
    __m128 PI = _mm_set1_ps(PI32);
    __m128 TWO_PI = _mm_set1_ps(2.0f * PI32);

    // cos(a) = sin(a + pi/2);
    __m128 y = _mm_add_ps(a, HALF_PI);
    // Wrap:  if ((y) > pi) y -= 2pi
    __m128 sub = _mm_and_ps(_mm_cmpnlt_ps(a, PI), TWO_PI);
    y = _mm_sub_ps(y, sub);

    return sin4f(y);
}

static void
Mat2x3FromTransform(SOA_Transform in, SOA_Mat2x3 out) {
	assert(in.count <= out.capacity);
#if 1
    __m128 ZERO = _mm_set1_ps(0.0f);
    for (u32 i = 0; i < in.count; i += 4) {
        __m128 px = _mm_load_ps(in.px + i);
        __m128 py = _mm_load_ps(in.py + i);
        __m128 sx = _mm_load_ps(in.sx + i);
        __m128 sy = _mm_load_ps(in.sy + i);
		__m128 rx = _mm_load_ps(in.rx + i);
		__m128 ry = _mm_load_ps(in.ry + i);

		__m128 a0 = _mm_mul_ps(rx, sx);
        __m128 b0 = _mm_mul_ps(ry, sx);
        __m128 a1 = _mm_mul_ps(_mm_sub_ps(ZERO, ry), sy);
        __m128 b1 = _mm_mul_ps(rx, sy);

        _mm_store_ps(out.a0 + i, a0);
        _mm_store_ps(out.a1 + i, a1);
        _mm_store_ps(out.a2 + i, px);
        _mm_store_ps(out.b0 + i, b0);
        _mm_store_ps(out.b1 + i, b1);
        _mm_store_ps(out.b2 + i, py);
    }
#else
	for (u32 i = 0; i < in.count; i++) {
		float sina = in.ry[i];//sinf(in.angle[i]);
		float cosa = in.rx[i];//cosf(in.angle[i]);

		float a0 = cosa * in.sx[i];
		float b0 = sina * in.sx[i];
		float a1 = -1.0f * sina * in.sy[i];
		float b1 = cosa * in.sy[i];

		out.a0[i] = a0;
		out.a1[i] = a1;
		out.a2[i] = in.px[i];
		out.b0[i] = b0;
		out.b1[i] = b1;
		out.b2[i] = in.py[i];
	}
#endif
	out.count = in.count;
}

#pragma pack(push, 1)
struct vertex {
	vec2 position;
	vec2 uv;
	vec4 color;
};
#pragma pack(pop)

struct RenderTarget {
	vec2 size;
	GLuint gl_framebuffer;
	GLuint gl_texture;
};

// TODO(bryan):  These should really go... somewhere else.
struct RenderResourceCache {
	GLuint gl_vertex_buffer;
	GLuint gl_vertex_array_object;
	GLuint gl_blur_vertex_buffer;
	GLuint gl_blur_vertex_array_object;

	GLuint program;
	GLuint untextured_program;
	GLuint post_program;
	GLuint blur_horizontal_program;
	GLuint blur_vertical_program;

	RenderTarget main_render_target;
	RenderTarget emission_render_target;

	RenderTarget layer_main_render_target;
	RenderTarget layer_emission_render_target;

	RenderTarget blur_temp_render_target;
	
	GLuint vertex_shader;
	GLuint fragment_shader;

	GLuint untextured_vertex_shader;
	GLuint untextured_fragment_shader;

	GLuint post_vertex_shader;
	GLuint post_fragment_shader;

	GLuint blur_vertex_shader;
	GLuint blur_fragment_shader_h;
	GLuint blur_fragment_shader_v;
};

const char * untextured_shader_vertex_src = GLSL(
	uniform mat4 clip_from_world;

	in vec2 position;
	in vec2 uv;
	in vec4 color;

	out vec2 out_uv;
	out vec4 out_color;

	vec4 Premultiply(vec4 color) {
		return vec4(color.rgb * color.a, color.a);
	}

	void main() {
		out_uv = uv;
		out_color = Premultiply(color);

		gl_Position = clip_from_world * vec4(position, 0.0, 1.0);
	}
);

const char * untextured_shader_fragment_src = GLSL(
	uniform sampler2D tex;

	in vec2 out_uv;
	in vec4 out_color;

	out vec4 frag_color;

	void main() {
		vec4 sample = texture(tex, out_uv);
		frag_color = out_color;
	}
);

const char * vertex_shader_src = GLSL(
	uniform mat4 clip_from_world;

	in vec2 position;
	in vec2 uv;
	in vec4 color;

	out vec4 out_color;
	out vec2 out_uv;

	vec4 Premultiply(vec4 color) {
		return vec4(color.rgb * color.a, color.a);
	}

	void main() {
		out_color = Premultiply(color);
		out_uv = uv;
		gl_Position = clip_from_world * vec4(position, 0.0, 1.0);
	}
);
const char * fragment_shader_src = GLSL(
	uniform sampler2D tex;

	in vec4 out_color;
	in vec2 out_uv;

	out vec4 frag_color;

	void main() {
		frag_color = out_color * texture(tex, out_uv);
		//frag_color = vec4(out_uv, 0.0, 1.0);
	}
);

//////

const char * post_vertex_shader_src = GLSL(
	uniform mat4 clip_from_world;
	uniform vec2 screen_shake_vec;

	in vec2 position;
	in vec2 uv;
	in vec4 color;

	out vec4 out_color;
	out vec2 out_uv;

	vec4 Premultiply(vec4 color) {
		return vec4(color.rgb * color.a, color.a);
	}

	void main() {
		out_color = Premultiply(color);
		out_uv = uv;
		gl_Position = clip_from_world * vec4(position + screen_shake_vec, 0.0, 1.0);
	}
);
const char * post_fragment_shader_src = GLSL(
	uniform sampler2D tex;
	uniform sampler2D emission;

	in vec4 out_color;
	in vec2 out_uv;

	out vec4 frag_color;

	void main() {
//		vec4 emission_sample = textureOffset(emission, out_uv, ivec2(0, 0));
		vec4 tex_color = texture(tex, out_uv);
		vec4 emission_color = texture(emission, out_uv);
//		emission_color.rgb *= emission_color.a;
		frag_color = tex_color + emission_color;
	}
);

////////

const char * blur_vertex_shader_src = GLSL(
	in vec2 position;
	in vec2 in_uv;

	out vec2 uv;

	void main() {
		uv = in_uv;
		gl_Position = vec4(position, 0.0, 1.0);
	}
);

const char * blur_fragment_shader_src_h = GLSL(
	uniform sampler2D source;
	in vec2 uv;
	out vec4 frag_color;

	void main() {
		vec4 color = vec4(0.0);

		color += textureOffset(source, uv, ivec2(-3, 0)) * 1.0;
		color += textureOffset(source, uv, ivec2(-2, 0)) * 2.0;
		color += textureOffset(source, uv, ivec2(-1, 0)) * 4.0;
		color += textureOffset(source, uv, ivec2( 0, 0)) * 8.0;
		color += textureOffset(source, uv, ivec2( 1, 0)) * 4.0;
		color += textureOffset(source, uv, ivec2( 2, 0)) * 2.0;
		color += textureOffset(source, uv, ivec2( 3, 0)) * 1.0;

		frag_color = color / 18.0;
	}
);

const char * blur_fragment_shader_src_v = GLSL(
	uniform sampler2D source;
	in vec2 uv;
	out vec4 frag_color;

	void main() {
		vec4 color = vec4(0.0);

		color += textureOffset(source, uv, ivec2(0, -3)) * 1.0;
		color += textureOffset(source, uv, ivec2(0, -2)) * 2.0;
		color += textureOffset(source, uv, ivec2(0, -1)) * 4.0;
		color += textureOffset(source, uv, ivec2(0,  0)) * 8.0;
		color += textureOffset(source, uv, ivec2(0,  1)) * 4.0;
		color += textureOffset(source, uv, ivec2(0,  2)) * 2.0;
		color += textureOffset(source, uv, ivec2(0,  3)) * 1.0;

		frag_color = color / 18.0;
	}
);

////////
static bool
CheckGLError() {
	GLenum error = glGetError();
	if (error != GL_NO_ERROR) {
		char buffer[1024];
		_snprintf_s(buffer, _countof(buffer), "GL error %d: %s\n", error, glewGetErrorString(error));
		OutputDebugStringA(buffer);

		MessageBoxA(0, buffer, "GL Error", MB_OK);
		
		return true;
	}
	return false;
}
static bool
BuildShader(GLenum type, const char * src, GLuint * shader)
{
	GLuint rv = glCreateShader(type);
	glShaderSource(rv, 1, &src, NULL);
	glCompileShader(rv);

	GLint status;
	glGetShaderiv(rv, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetShaderInfoLog(rv, sizeof(buffer), NULL, buffer);
		OutputDebugStringA(buffer);
		return false;
	}
	*shader = rv;
	return true;
}

RenderTarget MakeRenderTarget(vec2 size);

static void 
InitRenderResourceCache(RenderResourceCache * state) {
	// TODO(bryan): Error handling here
	if (!BuildShader(GL_VERTEX_SHADER, vertex_shader_src, &state->vertex_shader)) {
		assert(!"Failed to build general vertex shader.");
	}
	if (!BuildShader(GL_FRAGMENT_SHADER, fragment_shader_src, &state->fragment_shader)) {
		assert(!"Failed to build general fragment shader.");
	}

	state->program = glCreateProgram();
	glAttachShader(state->program, state->vertex_shader);
	glAttachShader(state->program, state->fragment_shader);
	glBindFragDataLocation(state->program, 0, "frag_color");
	glLinkProgram(state->program);
	GLint status;
	glGetProgramiv(state->program, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetProgramInfoLog(state->program, sizeof(buffer), NULL, buffer);
		OutputDebugStringA(buffer);
		assert(!"Failed to link general shader.");
	}


	if (!BuildShader(GL_VERTEX_SHADER, untextured_shader_vertex_src, &state->untextured_vertex_shader)) {
		assert(!"Failed to build untextured vertex shader.");
	}
	if (!BuildShader(GL_FRAGMENT_SHADER, untextured_shader_fragment_src, &state->untextured_fragment_shader)) {
		assert(!"Failed to build untextured fragment shader.");
	}

	state->untextured_program = glCreateProgram();
	glAttachShader(state->untextured_program, state->untextured_vertex_shader);
	glAttachShader(state->untextured_program, state->untextured_fragment_shader);
	glBindFragDataLocation(state->untextured_program, 0, "frag_color");
	glLinkProgram(state->untextured_program);
	glGetProgramiv(state->untextured_program, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetProgramInfoLog(state->untextured_program, sizeof(buffer), NULL, buffer);
		OutputDebugStringA(buffer);
		assert(!"Failed to link fragment shader.");
	}

	if (!BuildShader(GL_VERTEX_SHADER, post_vertex_shader_src, &state->post_vertex_shader)) {
		assert(!"Failed to build post vertex shader.");
	}
	if (!BuildShader(GL_FRAGMENT_SHADER, post_fragment_shader_src, &state->post_fragment_shader)) {
		assert(!"Failed to build post fragment shader.");
	}

	state->post_program = glCreateProgram();
	glAttachShader(state->post_program, state->post_vertex_shader);
	glAttachShader(state->post_program, state->post_fragment_shader);
	glBindFragDataLocation(state->post_program, 0, "frag_color");
	glLinkProgram(state->post_program);
	glGetProgramiv(state->post_program, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetProgramInfoLog(state->post_program, sizeof(buffer), NULL, buffer);
		OutputDebugStringA(buffer);
		assert(!"Failed to link post shader.");
	}

	// Blur shaders
	if (!BuildShader(GL_VERTEX_SHADER, blur_vertex_shader_src, &state->blur_vertex_shader)) {
		assert(!"Failed to build blur vertex shader.");
	}
	if (!BuildShader(GL_FRAGMENT_SHADER, blur_fragment_shader_src_h, &state->blur_fragment_shader_h)) {
		assert(!"Failed to build h blur fragment shader.");
	}
	if (!BuildShader(GL_FRAGMENT_SHADER, blur_fragment_shader_src_v, &state->blur_fragment_shader_v)) {
		assert(!"Failed to build v blur fragment shader.");
	}

	state->blur_horizontal_program = glCreateProgram();
	glAttachShader(state->blur_horizontal_program, state->blur_vertex_shader);
	glAttachShader(state->blur_horizontal_program, state->blur_fragment_shader_h);
	glBindFragDataLocation(state->blur_horizontal_program, 0, "frag_color");
	glLinkProgram(state->blur_horizontal_program);
	glGetProgramiv(state->blur_horizontal_program, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetProgramInfoLog(state->blur_horizontal_program, sizeof(buffer), NULL, buffer);
		OutputDebugStringA(buffer);
		assert(!"Failed to link h blur shader.");
	}

	state->blur_vertical_program = glCreateProgram();
	glAttachShader(state->blur_vertical_program, state->blur_vertex_shader);
	glAttachShader(state->blur_vertical_program, state->blur_fragment_shader_v);
	glBindFragDataLocation(state->blur_vertical_program, 0, "frag_color");
	glLinkProgram(state->blur_vertical_program);
	glGetProgramiv(state->blur_vertical_program, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetProgramInfoLog(state->blur_vertical_program, sizeof(buffer), NULL, buffer);
		OutputDebugStringA(buffer);
		assert(!"Failed to link v blur shader.");
	}

	glGenBuffers(1, &state->gl_vertex_buffer);
	glGenVertexArrays(1, &state->gl_vertex_array_object);

	glGenBuffers(1, &state->gl_blur_vertex_buffer);
	glGenVertexArrays(1, &state->gl_blur_vertex_array_object);

	state->main_render_target = MakeRenderTarget(Vec2((float)gScreenWidth, (float)gScreenHeight));
	state->emission_render_target = MakeRenderTarget(Vec2((float)gScreenWidth, (float)gScreenHeight) * 0.5f);

	state->layer_main_render_target = MakeRenderTarget(Vec2((float)gScreenWidth, (float)gScreenHeight));
	state->layer_emission_render_target = MakeRenderTarget(Vec2((float)gScreenWidth, (float)gScreenHeight) * 0.5f);
	state->blur_temp_render_target = MakeRenderTarget(Vec2((float)gScreenWidth, (float)gScreenHeight) * 0.5f);
}

RenderTarget
MakeRenderTarget(vec2 size) {
	GLuint fb;
	glGenFramebuffers(1, &fb);
	glBindFramebuffer(GL_FRAMEBUFFER, fb);

	GLuint texture;	
	glGenTextures(1, &texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (s32)size.x, (s32)size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_2D);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

	GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(_countof(draw_buffers), draw_buffers);

	GLenum framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (framebufferStatus != GL_FRAMEBUFFER_COMPLETE) {
		switch (framebufferStatus) {
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			OutputDebugString(TEXT("Incomplete attachment.\n"));
			assert(!"Framebuffer: Incomplete attachment.");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			OutputDebugString(TEXT("Incomplete missing attachment.\n"));
			assert(!"Framebuffer: Incomplete / missing attachment.");
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			OutputDebugString(TEXT("Framebuffer unsupported.\n"));
			assert(!"Framebuffer: Unsupported.");
			break;
		default:
			OutputDebugString(TEXT("Framebuffer is incomplete, unknown reason.\n"));
			assert(!"Framebuffer: Incomplete, unknown reason.");
			break;
		}
		assert(!"Failed to create framebuffer.");
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	RenderTarget result;
	result.size = size;
	result.gl_framebuffer = fb;
	result.gl_texture = texture;

	return result;
}

static void
DestroyRenderTarget(RenderTarget * target) {
	glDeleteTextures(1, &target->gl_texture);
	glDeleteFramebuffers(1, &target->gl_framebuffer);

	target->size = Vec2();
	target->gl_framebuffer = 0;
	target->gl_texture = 0;
}

static void
ClearRenderTarget(RenderTarget * target) {
	glBindFramebuffer(GL_FRAMEBUFFER, target->gl_framebuffer);
	glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
}

struct DrawStateBuffer {
	vertex * vertex_buffer;
	u32 vertex_buffer_top;
	u32 vertex_buffer_max;

	RenderTarget * render_target;

	GLuint shader_program;
	GLuint gl_vbo;
	GLuint gl_vao;
	Texture * texture;
	mat4x4 * clip_from_world;
};

static void
FlushDrawStateBuffer(DrawStateBuffer * draw_buffer, vec4 * viewport=NULL) {
	if (draw_buffer->vertex_buffer_top == 0) return;

	assert(draw_buffer->vertex_buffer_top % 4 == 0);

	assert(!CheckGLError());

	GLuint program = draw_buffer->shader_program;
	mat4x4 * clip_from_world = draw_buffer->clip_from_world;

	glBindFramebuffer(GL_FRAMEBUFFER, draw_buffer->render_target->gl_framebuffer);
	assert(!CheckGLError() && "Bind framebuffer");
	if (viewport) {
		glViewport((GLint)viewport->x, (GLint)viewport->y, (GLsizei)viewport->z, (GLsizei)viewport->w);
	}
	else {
		glViewport(0, 0, (GLsizei)draw_buffer->render_target->size.x, (GLsizei)draw_buffer->render_target->size.y);
	}
	assert(!CheckGLError() && "Set viewport");

	glEnable(GL_BLEND);
	glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
	glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);
//	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	assert(!CheckGLError() && "enable blend");

	glUseProgram(program);
	assert(!CheckGLError() && "use program");

	glBindVertexArray(draw_buffer->gl_vao);
	assert(!CheckGLError() && "bind vao");

	glBindBuffer(GL_ARRAY_BUFFER, draw_buffer->gl_vbo);
	assert(!CheckGLError() && "bind vbo");
	static u32 buffer_size = 0;
	if (buffer_size < sizeof(vertex) * draw_buffer->vertex_buffer_top) {
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * draw_buffer->vertex_buffer_top, draw_buffer->vertex_buffer, GL_DYNAMIC_DRAW);
		buffer_size = sizeof(vertex) * draw_buffer->vertex_buffer_top;
	}
	else {
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertex) * draw_buffer->vertex_buffer_top, draw_buffer->vertex_buffer);
	}
	assert(!CheckGLError() && "fill vbo");

	GLint clip_from_world_location = glGetUniformLocation(program, "clip_from_world");
	glUniformMatrix4fv(clip_from_world_location, 1, GL_FALSE, &clip_from_world->m[0]);
	assert(!CheckGLError() && "set clip_from_world");
	GLint texture_location = glGetUniformLocation(program, "tex");
	glUniform1i(texture_location, 0);
	assert(!CheckGLError() && "set texture");

	GLint position_location = glGetAttribLocation(program, "position");
	GLint uv_location = glGetAttribLocation(program, "uv");
	GLint color_location = glGetAttribLocation(program, "color");

	if (position_location >= 0) {
		glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 8, 0);
		glEnableVertexAttribArray(position_location);
		assert(!CheckGLError() && "position ptr");
	}

	if (uv_location >= 0) {
		glVertexAttribPointer(uv_location, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void *)(sizeof(float) * 2));
		glEnableVertexAttribArray(uv_location);
		assert(!CheckGLError() && "uv ptr");
	}

	if (color_location >= 0) {
		glVertexAttribPointer(color_location, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void *)(sizeof(float) * 4));
		glEnableVertexAttribArray(color_location);
		assert(!CheckGLError() && "color ptr");
	}

	glDrawArrays(GL_QUADS, 0, draw_buffer->vertex_buffer_top);
	assert(!CheckGLError() && "draw arrays");
	
	if (buffer_size > 0) {
		// Orphan the buffer, driver should allocate a new one.
		glBufferData(GL_ARRAY_BUFFER, buffer_size, NULL, GL_DYNAMIC_DRAW);
	}
	// Empty draw buffer
	draw_buffer->vertex_buffer_top = 0;
}

static void
SetShader(DrawStateBuffer * draw_buffer, GLuint shader_program) {
	if (shader_program != draw_buffer->shader_program) {
		FlushDrawStateBuffer(draw_buffer);
	
		draw_buffer->shader_program = shader_program;
	}
}

static void
SetTexture(DrawStateBuffer * draw_buffer, Texture * texture) {
	if (texture != draw_buffer->texture) {
		FlushDrawStateBuffer(draw_buffer);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture ? texture->texture_id : 0);

		draw_buffer->texture = texture;
	}
}

static void
SetRenderTarget(DrawStateBuffer * draw_buffer, RenderTarget * render_target) {
	if (draw_buffer->render_target != render_target) {
		FlushDrawStateBuffer(draw_buffer);

		draw_buffer->render_target = render_target;
	}
}

static vertex *
GetNextQuad(DrawStateBuffer * draw_buffer) {
	if (draw_buffer->vertex_buffer_top + 4 >= draw_buffer->vertex_buffer_max) {
		FlushDrawStateBuffer(draw_buffer);
	}

	vertex * result = draw_buffer->vertex_buffer + draw_buffer->vertex_buffer_top;
	draw_buffer->vertex_buffer_top += 4;
	return result;
}

inline DrawStateBuffer
AllocateDrawStateBuffer(MemoryArena * memory_arena, u32 size) {
	DrawStateBuffer result = {};

	result.vertex_buffer = (vertex *)memory_arena->Allocate(size);
	result.vertex_buffer_max = size / sizeof(vertex);
	result.vertex_buffer_top = 0;

	return result;
}

struct PostEffectState {
	float screen_shake_intensity;
};

static vec2
GetScreenShakeVec(float intensity) {
	return NextRandVec2() * intensity;
}

void
DoBlurPass(RenderTarget * target, RenderResourceCache * render_state) {
	RenderTarget temp = render_state->blur_temp_render_target;

	glViewport(0, 0, (GLsizei)target->size.x, (GLsizei)target->size.y);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
//	glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
//	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);

	float vertices[] = {
		// position        uv
		-1.0f,  1.0f,  0.0f, 1.0f,
		 1.0f,  1.0f,  1.0f, 1.0f,
		 1.0f, -1.0f,  1.0f, 0.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
	};

//	GLuint temp_buffer;
//	GLuint temp_vao;
//	glGenBuffers(1, &temp_buffer);
//	glGenVertexArrays(1, &temp_vao);
	glBindVertexArray(render_state->gl_blur_vertex_array_object);
	glBindBuffer(GL_ARRAY_BUFFER, render_state->gl_blur_vertex_buffer);
	static int init = 0;
	if (!init) {
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * array_count(vertices), vertices, GL_DYNAMIC_DRAW);
		init = 1;
	}
	else {
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * array_count(vertices), vertices);
	}
	assert(!CheckGLError());
	
	glActiveTexture(GL_TEXTURE0);

	GLint position_location;
	GLint uv_location;
	// vertical blur:  target -> temp
	glUseProgram(render_state->blur_vertical_program);
	glBindFramebuffer(GL_FRAMEBUFFER, temp.gl_framebuffer);
	ClearRenderTarget(&temp);
	glBindTexture(GL_TEXTURE_2D, target->gl_texture);
	glUniform1i(glGetUniformLocation(render_state->blur_vertical_program, "source"), 0);
	
	assert(!CheckGLError());
	
	position_location = glGetAttribLocation(render_state->blur_vertical_program, "position");
	glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glEnableVertexAttribArray(position_location);

	uv_location = glGetAttribLocation(render_state->blur_vertical_program, "in_uv");
	glVertexAttribPointer(uv_location, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(2 * sizeof(float)));
	glEnableVertexAttribArray(uv_location);

	glDrawArrays(GL_QUADS, 0, 4);
	
	assert(!CheckGLError());

	// horizontal blur: temp -> target
	glUseProgram(render_state->blur_horizontal_program);
	glBindFramebuffer(GL_FRAMEBUFFER, target->gl_framebuffer);
	glBindTexture(GL_TEXTURE_2D, temp.gl_texture);
	glUniform1i(glGetUniformLocation(render_state->blur_horizontal_program, "source"), 0);

	position_location = glGetAttribLocation(render_state->blur_horizontal_program, "position");
	glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glEnableVertexAttribArray(position_location);

	uv_location = glGetAttribLocation(render_state->blur_horizontal_program, "in_uv");
	glVertexAttribPointer(uv_location, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(2 * sizeof(float)));
	glEnableVertexAttribArray(uv_location);

	glDrawArrays(GL_QUADS, 0, 4);

	assert(!CheckGLError());

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, render_state->main_render_target.gl_framebuffer);

//	glDeleteVertexArrays(1, &temp_vao);
//	glDeleteBuffers(1, &temp_buffer);
}

//         x:float
//         y:float
//         z:float
//         color:uint8[4]
#pragma pack(push, 1)
struct ef_vertex {
	float x;
	float y;
	float z;
	u8 color[4];
};
#pragma pack(pop)

u32
RenderCommandSize(RenderCommandHeader header) {
	u32 size = 0;
	switch (header.type) {
	case RC_ENUM(Rect):
		{
			size = sizeof(RC_STRUCT(Rect));
		}
		break;
	case RC_ENUM(Circle):
		{
			size = sizeof(RC_STRUCT(Circle));
		}
		break;
	case RC_ENUM(Text):
		{
			size = sizeof(RC_STRUCT(Text));
		}
		break;
	case RC_ENUM(Sprite):
		{
			size = sizeof(RC_STRUCT(Sprite));
		}
		break;
	case RC_ENUM(EffectProperty):
		{
			size = sizeof(RC_STRUCT(EffectProperty));
		}
		break;
	default:
		assert(0);
	}
	return size;
}

bool
RenderSortPredicate(RenderCommandHeader * a, RenderCommandHeader * b) {
	return a->sort_key < b->sort_key;
}

void 
SortRenderBuffer(RenderCommandBuffer * buffer, MemoryArena * scratch_buffer) {
	// Gather an array of pointers to each element's header.  Sort these, using the header info.  
	// Use the sorted array of pointer to copy into a temporary buffer, then copy back into the 
	// render buffer's memory.
	// This is O(n) for memory, but sorting in-place is unfeasible due to the inconsistent item
	// sizes.  One alternative is to force render commands to be a specific size (using a union),
	// but that will waste a lot of memory in the buffer (because text items are so much larger 
	// than the others.)
	MemoryArenaBookmark scratch_bookmark = scratch_buffer->Bookmark();

	// TODO(bryan):  In theory, we can fit more elements in the buffer than this.  Ensure we don't.
	const u32 sort_buffer_max_count = 4096;
	RenderCommandHeader ** sort_buffer = (RenderCommandHeader **)scratch_buffer->AllocateZeroed(sort_buffer_max_count * sizeof(RenderCommandHeader *));
	u32 sort_buffer_top = 0;

	u32 current = 0;
	while (current < buffer->top) {
		RenderCommandHeader * header = (RenderCommandHeader *)PTR_BYTE_OFFSET(buffer->buffer, current);
		u32 item_size = sizeof(*header) + RenderCommandSize(*header);

		assert(sort_buffer_top < sort_buffer_max_count);
		sort_buffer[sort_buffer_top++] = header;

		current += item_size;
	}
	assert(current == buffer->top);

	std::sort(sort_buffer, sort_buffer + sort_buffer_top, RenderSortPredicate);

	char * temp_buffer = (char *)scratch_buffer->Allocate(buffer->top);
	u32 temp_buffer_top = 0;
	for (u32 i = 0; i < sort_buffer_top; ++i) {
		RenderCommandHeader * header = sort_buffer[i];
		u32 item_size = sizeof(*header) + RenderCommandSize(*header);

		memcpy(temp_buffer + temp_buffer_top, header, item_size); 
		temp_buffer_top += item_size;
	}

	assert(temp_buffer_top == buffer->top);

	memcpy(buffer->buffer, temp_buffer, temp_buffer_top);

	scratch_buffer->Reset(scratch_bookmark);
}

static void
DoLayerPostPass(RenderTarget * blit_target, RenderTarget * layer_main, RenderTarget * layer_emission, DrawStateBuffer draw_buffer, RenderResourceCache * render_state, vec2 screen_shake_vec) {
	DoBlurPass(layer_emission, render_state);

	// TODO(bryan):  Render target should store backing texture in a Texture object.
	Texture fbtex;
	fbtex.width = gScreenWidth;
	fbtex.height = gScreenHeight;
	fbtex.texture_id = layer_main->gl_texture;
	SetTexture(&draw_buffer, &fbtex);

	Texture emission_tex;
	emission_tex.width = (u32)layer_emission->size.x;
	emission_tex.height = (u32)layer_emission->size.y;
	emission_tex.texture_id = layer_emission->gl_texture;

	mat4x4 identity = IdentityMatrix();

	draw_buffer.vertex_buffer_top = 0;

	draw_buffer.render_target = blit_target;
	draw_buffer.texture = &fbtex;
	draw_buffer.clip_from_world = &identity;
	draw_buffer.shader_program = render_state->post_program;
	draw_buffer.gl_vbo = render_state->gl_vertex_buffer;

	glUseProgram(draw_buffer.shader_program);
	
	vertex * window_quad = GetNextQuad(&draw_buffer);
	window_quad[0].color = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
	window_quad[0].position = Vec2(-1.0f, 1.0f);
	window_quad[0].uv = Vec2(0.0f, 1.0f);

	window_quad[1].color = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
	window_quad[1].position = Vec2(1.0f, 1.0f);
	window_quad[1].uv = Vec2(1.0f, 1.0f);

	window_quad[2].color = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
	window_quad[2].position = Vec2(1.0f, -1.0f);
	window_quad[2].uv = Vec2(1.0f, 0.0f);
	
	window_quad[3].color = Vec4(1.0f, 1.0f, 1.0f, 1.0f);
	window_quad[3].position = Vec2(-1.0f, -1.0f);
	window_quad[3].uv = Vec2(0.0f, 0.0f);

	GLint screen_shake_vec_location = glGetUniformLocation(draw_buffer.shader_program, "screen_shake_vec");
	GLint emission_tex_location = glGetUniformLocation(draw_buffer.shader_program, "emission");

	glUniform2f(screen_shake_vec_location, screen_shake_vec.x / (float)gScreenWidth, screen_shake_vec.y / (float)gScreenHeight);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, emission_tex.texture_id);
	glUniform1i(emission_tex_location, 1);

	FlushDrawStateBuffer(&draw_buffer, &gScreenViewport);
}


void
ExecuteRenderBuffer(RenderCommandBuffer * buffer, RenderTarget * render_target, RenderResourceCache * render_state, MemoryArena * render_scratch_buffer) {
	// Just in case the buffer is empty.
	if (buffer->top <= sizeof(RenderCommandHeader)) {
		assert(buffer->top == 0);
		return;
	}

	SortRenderBuffer(buffer, render_scratch_buffer);

	u32 cmd_buffer_pos = 0;

	float half_screen_width = render_target->size.x * 0.5f;
	float half_screen_height = render_target->size.y * 0.5f;

	mat4x4 clip_from_world = OrthoProjectionMatrix(half_screen_width, -half_screen_width, half_screen_height, -half_screen_height, -5.0f, 5.0f);
	
	MemoryArenaBookmark scratch_bookmark = render_scratch_buffer->Bookmark();

	SOA_Mat2x3 transforms = {};
	transforms.count = 0;
	transforms.capacity = buffer->transforms.count;
	transforms.a0 = (float *)render_scratch_buffer->AllocateAlignedZeroed(sizeof(float) * transforms.capacity, 16);
	transforms.a1 = (float *)render_scratch_buffer->AllocateAlignedZeroed(sizeof(float) * transforms.capacity, 16);
	transforms.a2 = (float *)render_scratch_buffer->AllocateAlignedZeroed(sizeof(float) * transforms.capacity, 16);
	transforms.b0 = (float *)render_scratch_buffer->AllocateAlignedZeroed(sizeof(float) * transforms.capacity, 16);
	transforms.b1 = (float *)render_scratch_buffer->AllocateAlignedZeroed(sizeof(float) * transforms.capacity, 16);
	transforms.b2 = (float *)render_scratch_buffer->AllocateAlignedZeroed(sizeof(float) * transforms.capacity, 16);

	Mat2x3FromTransform(buffer->transforms, transforms);
	buffer->transforms.count = 0;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	ClearRenderTarget(&render_state->main_render_target);
	ClearRenderTarget(&render_state->emission_render_target);

	PostEffectState post_effect_state = { };

	RenderTarget layer_main_target = render_state->layer_main_render_target;
	RenderTarget layer_emission_target = render_state->layer_emission_render_target;
	ClearRenderTarget(&layer_main_target);
	ClearRenderTarget(&layer_emission_target);

	DrawStateBuffer draw_buffer = AllocateDrawStateBuffer(render_scratch_buffer, Kilobytes(64));
	draw_buffer.render_target = &layer_main_target;
	draw_buffer.texture = NULL;
	draw_buffer.clip_from_world = &clip_from_world;
	draw_buffer.shader_program = render_state->untextured_program;
	draw_buffer.gl_vbo = render_state->gl_vertex_buffer;
	draw_buffer.gl_vao = render_state->gl_vertex_array_object;

	assert(!CheckGLError());

	vec2 screen_shake_dir = NextRandVec2();

	u32 active_layer = RenderLayer_Count;
	{
		RenderCommandHeader * header = (RenderCommandHeader *)PTR_BYTE_OFFSET(buffer->buffer, cmd_buffer_pos);
		active_layer = header->layer;
	}
	assert(active_layer < RenderLayer_Count);

	while (cmd_buffer_pos < buffer->top) {
		RenderCommandHeader * header = (RenderCommandHeader *)PTR_BYTE_OFFSET(buffer->buffer, cmd_buffer_pos);
		cmd_buffer_pos += sizeof(*header);

		if (header->layer != active_layer) {
			FlushDrawStateBuffer(&draw_buffer);
			
			// Do post and blit current layer to screen
			// TODO(bryan):  Currently this blits directly to the screen framebuffer.  We may want to blit to an intermediate for scaling?
			vec2 screen_shake_vec = screen_shake_dir * post_effect_state.screen_shake_intensity;
			DoLayerPostPass(render_target, &layer_main_target, &layer_emission_target, draw_buffer, render_state, active_layer != RenderLayer_UI ? screen_shake_vec : Vec2());

			draw_buffer.shader_program = render_state->untextured_program;
			draw_buffer.texture = NULL;

			// Begin next layer
			ClearRenderTarget(&layer_main_target);
			ClearRenderTarget(&layer_emission_target);
			active_layer = header->layer;
		}

		switch (header->target) {
		case RenderTargetBuffer_Main:
			{
				SetRenderTarget(&draw_buffer, &layer_main_target);
			}
			break;
		case RenderTargetBuffer_Emission:
			{
				SetRenderTarget(&draw_buffer, &layer_emission_target);
			}
			break;
		default:
			assert(0);
		}

		void * cmd_data = PTR_BYTE_OFFSET(buffer->buffer, cmd_buffer_pos);
		switch (header->type) {
		case RC_ENUM(Rect):
			{
				SetTexture(&draw_buffer, NULL);
				SetShader(&draw_buffer, render_state->untextured_program);

				RenderCommand_Rect * cmd = (RenderCommand_Rect *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);
				vec2 size = cmd->size;
				vec2 min = Vec2(-0.5f, -0.5f) * size;
				vec2 max = Vec2( 0.5f,  0.5f) * size;

				vertex * v = GetNextQuad(&draw_buffer);
				v[0].position = min;
				v[0].color = cmd->color;

				v[1].position = Vec2(max.x, min.y);
				v[1].color = cmd->color;
				
				v[2].position = max;
				v[2].color = cmd->color;
				
				v[3].position = Vec2(min.x, max.y);
				v[3].color = cmd->color;

				u32 tid = cmd->transform;
				vec2 pos = v[0].position;
				v[0].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[0].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

				pos = v[1].position;
				v[1].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[1].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

				pos = v[2].position;
				v[2].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[2].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

				pos = v[3].position;
				v[3].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[3].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];
			}
			break;
		case RC_ENUM(Circle):
			{
				SetTexture(&draw_buffer, NULL);
				SetShader(&draw_buffer, render_state->untextured_program);

				RenderCommand_Circle * cmd = (RenderCommand_Circle *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);
				
				// Scale based on size of circle (smaller -> fewer sides needed)
				u32 num_sides;
				if (cmd->radius <= 5.0f) {
					num_sides = 8;
				}
				else if (cmd->radius <= 30.0f) {
					num_sides = 16;
				}
				else if (cmd->radius <= 100.0f) {
					num_sides = 32;
				}
				else {
					num_sides = 64;
				}
				const float d_theta = (PI32 * 2.0f) / num_sides;

				MemoryArenaBookmark circle_pos_bookmark = render_scratch_buffer->Bookmark();
				vec2 * positions = (vec2 *)render_scratch_buffer->Allocate(sizeof(vec2) * num_sides);
				for (u32 i = 0; i < num_sides; ++i) {
					float theta = d_theta * i;
					// TODO(bryan):  We don't *really* need to call sin/cos this much.
					positions[i] = Vec2(cos(theta), sin(theta));
				}

				float inner_radius = max(cmd->radius - cmd->thickness, 0.0f);
				u32 tid = cmd->transform;
				for (u32 i = 0; i < num_sides; ++i) {
					vertex * v = GetNextQuad(&draw_buffer);
					v[0].position = positions[i] * inner_radius;
					v[1].position = positions[i] * cmd->radius;
					v[2].position = positions[(i + 1) % num_sides] * cmd->radius;
					v[3].position = positions[(i + 1) % num_sides] * inner_radius;

					u32 tid = cmd->transform;
					vec2 pos = v[0].position;
					v[0].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[0].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					pos = v[1].position;
					v[1].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[1].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					pos = v[2].position;
					v[2].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[2].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					pos = v[3].position;
					v[3].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[3].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					v[0].color = cmd->color;
					v[1].color = cmd->color;
					v[2].color = cmd->color;
					v[3].color = cmd->color;
				}
				render_scratch_buffer->Reset(circle_pos_bookmark);
			}
			break;
		case RC_ENUM(Sprite):
			{
				RenderCommand_Sprite * cmd = (RenderCommand_Sprite *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);

				SetTexture(&draw_buffer, cmd->tex);
				SetShader(&draw_buffer, render_state->program);
				
				vec2 size = cmd->size;
				vec2 min = Vec2(-0.5f, -0.5f) * size;
				vec2 max = Vec2( 0.5f,  0.5f) * size;

				vec2 uv_size = cmd->uv_max - cmd->uv_min;

				vertex * v = GetNextQuad(&draw_buffer);
				v[0].position = min;
				v[0].uv = cmd->uv_min + uv_size;
				v[0].color = cmd->color;

				v[1].position = Vec2(min.x, max.y);
				v[1].uv = cmd->uv_min + Vec2(uv_size.x, 0.0f);
				v[1].color = cmd->color;

				v[2].position = max;
				v[2].uv = cmd->uv_min;
				v[2].color = cmd->color;
				
				v[3].position = Vec2(max.x, min.y);
				v[3].uv = cmd->uv_min + Vec2(0.0f, uv_size.y);
				v[3].color = cmd->color;

				// Transform
				u32 tid = cmd->transform;
				vec2 pos = v[0].position;
				v[0].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[0].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

				pos = v[1].position;
				v[1].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[1].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

				pos = v[2].position;
				v[2].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[2].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

				pos = v[3].position;
				v[3].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
				v[3].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];
			}
			break;
		case RC_ENUM(EffectProperty):
			{
				RenderCommand_EffectProperty * cmd = (RenderCommand_EffectProperty *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);

				switch (cmd->property) {
				case PostEffect_ScreenShakeIntensity:
					post_effect_state.screen_shake_intensity = cmd->value;
					break;
				default:
					// Unsupported effect property
					assert(0);
				}
			}
			break;
		case RC_ENUM(Text):
			{
				RenderCommand_Text * cmd = (RenderCommand_Text *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);

				SetTexture(&draw_buffer, NULL);
				SetShader(&draw_buffer, render_state->untextured_program);

				char ef_buf[20000];

				u32 quad_count = stb_easy_font_print(0.0f, 0.0f, cmd->text, NULL, ef_buf, sizeof(ef_buf));
				ef_vertex * ef_vbuffer = (ef_vertex *)ef_buf;

				u32 tid = cmd->transform;
				for (u32 q = 0; q < quad_count; ++q) {
					u32 vertex_offset = q * 4;
					ef_vertex * efv = ef_vbuffer + vertex_offset;

					vertex * v = GetNextQuad(&draw_buffer);

					v[0].position = Vec2(efv[0].x, -efv[0].y);
					v[0].color = cmd->color;

					v[1].position = Vec2(efv[1].x, -efv[1].y);
					v[1].color = cmd->color;

					v[2].position = Vec2(efv[2].x, -efv[2].y);
					v[2].color = cmd->color;

					v[3].position = Vec2(efv[3].x, -efv[3].y);
					v[3].color = cmd->color;

					u32 tid = cmd->transform;
					vec2 pos = v[0].position;
					v[0].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[0].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					pos = v[1].position;
					v[1].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[1].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					pos = v[2].position;
					v[2].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[2].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];

					pos = v[3].position;
					v[3].position.x = pos.x * transforms.a0[tid] + pos.y * transforms.a1[tid] + transforms.a2[tid];
					v[3].position.y = pos.x * transforms.b0[tid] + pos.y * transforms.b1[tid] + transforms.b2[tid];
				}
			}
			break;
		default:
			// Unsupported render command.
			assert(0);
		}
	}

	FlushDrawStateBuffer(&draw_buffer);

	buffer->top = 0;

	vec2 screen_shake_vec = screen_shake_dir * post_effect_state.screen_shake_intensity;
	DoLayerPostPass(render_target, &layer_main_target, &layer_emission_target, draw_buffer, render_state, active_layer != RenderLayer_UI ? screen_shake_vec : Vec2());

	render_scratch_buffer->Reset(scratch_bookmark);
}

struct RenderContext {
	// OS context handles. 
	HGLRC gl_context;
	HDC device_context;
};

inline void
SetCurrentContext(RenderContext * context) {
	assert(context);
	wglMakeCurrent(context->device_context, context->gl_context);
}

RenderContext *
CreateRenderContext(MemoryArena * arena, HWND window) {
	RenderContext * context = (RenderContext *)arena->Allocate(sizeof(RenderContext));
	assert(context);

	context->device_context = GetDC(window);

	PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		24,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		32,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};
	int pixel_format = ChoosePixelFormat(context->device_context, &pfd);
	SetPixelFormat(context->device_context, pixel_format, &pfd);
 
	context->gl_context = wglCreateContext(context->device_context);

	SetCurrentContext(context);

	glewExperimental = GL_TRUE;
	glewInit();

	return context;
}

inline void
SetViewport(RenderContext * context, u32 x, u32 y, u32 w, u32 h) {
	assert(context);
	glViewport(x, y, w, h);
}

inline void
SwapBuffers(RenderContext * context) {
	assert(context);
	SwapBuffers(context->device_context);
}

inline void
ClearBuffer(RenderContext * context) {
	assert(context);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void
Win32ReadKeyState(u8 keyboard_state[256]) {
	GetKeyboardState(keyboard_state);
}

struct KeyboardControllerBinding {
	u32 key_code[GameVirtualInputs_Count];
};

enum GamepadInputType {
	GamepadInputType_Button,
	GamepadInputType_LThumbstick,
	GamepadInputType_RThumbstick,
	GamepadInputType_LThumbstickAxis,
	GamepadInputType_RThumbstickAxis,
	GamepadInputType_LTrigger,
	GamepadInputType_RTrigger,

	GamepadInputType_Count,
};

static void
SetDefaultKeyboardBinding(KeyboardControllerBinding * keyboard_binding) {
	keyboard_binding->key_code[GameVirtualInputs_Up] = VK_UP;
	keyboard_binding->key_code[GameVirtualInputs_Down] = VK_DOWN;
	keyboard_binding->key_code[GameVirtualInputs_Left] = VK_LEFT;
	keyboard_binding->key_code[GameVirtualInputs_Right] = VK_RIGHT;
	keyboard_binding->key_code[GameVirtualInputs_Fire] = VK_SPACE;
	keyboard_binding->key_code[GameVirtualInputs_MenuUp] = VK_UP;
	keyboard_binding->key_code[GameVirtualInputs_MenuDown] = VK_DOWN;
	keyboard_binding->key_code[GameVirtualInputs_MenuLeft] = VK_LEFT;
	keyboard_binding->key_code[GameVirtualInputs_MenuRight] = VK_RIGHT;
	keyboard_binding->key_code[GameVirtualInputs_MenuSelect] = VK_RETURN;
	keyboard_binding->key_code[GameVirtualInputs_MenuEscape] = VK_ESCAPE;
}

enum Direction {
	Direction_Up,
	Direction_Down, 
	Direction_Left, 
	Direction_Right,
};

struct GamepadInput {
	GamepadInputType type;
	union {
		// Additional data for each input type goes here.
		struct {
			u32 button_mask;
		};
		struct {
			u32 thumbstick_direction;
		};
	};
};

struct GamepadControllerBinding {
	GamepadInput input_binds[GameVirtualInputs_Count];
};

static void
SetDefaultGamepadBinding(GamepadControllerBinding * binding) {
	binding->input_binds[GameVirtualInputs_Up].type = GamepadInputType_LThumbstick;
	binding->input_binds[GameVirtualInputs_Up].thumbstick_direction = Direction_Up;

	binding->input_binds[GameVirtualInputs_Down].type = GamepadInputType_LThumbstick;
	binding->input_binds[GameVirtualInputs_Down].thumbstick_direction = Direction_Down;

	binding->input_binds[GameVirtualInputs_Left].type = GamepadInputType_LThumbstick;
	binding->input_binds[GameVirtualInputs_Left].thumbstick_direction = Direction_Left;

	binding->input_binds[GameVirtualInputs_Right].type = GamepadInputType_LThumbstick;
	binding->input_binds[GameVirtualInputs_Right].thumbstick_direction = Direction_Right;

	binding->input_binds[GameVirtualInputs_HorizontalAxis].type = GamepadInputType_LThumbstickAxis;
	binding->input_binds[GameVirtualInputs_HorizontalAxis].thumbstick_direction = Direction_Left;

	binding->input_binds[GameVirtualInputs_VerticalAxis].type = GamepadInputType_LThumbstickAxis;
	binding->input_binds[GameVirtualInputs_VerticalAxis].thumbstick_direction = Direction_Up;

	binding->input_binds[GameVirtualInputs_Fire].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_Fire].button_mask = XINPUT_GAMEPAD_RIGHT_SHOULDER | XINPUT_GAMEPAD_A;

	binding->input_binds[GameVirtualInputs_MenuUp].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_MenuUp].button_mask = XINPUT_GAMEPAD_DPAD_UP;

	binding->input_binds[GameVirtualInputs_MenuDown].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_MenuDown].button_mask = XINPUT_GAMEPAD_DPAD_DOWN;

	binding->input_binds[GameVirtualInputs_MenuLeft].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_MenuLeft].button_mask = XINPUT_GAMEPAD_DPAD_LEFT;

	binding->input_binds[GameVirtualInputs_MenuRight].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_MenuRight].button_mask = XINPUT_GAMEPAD_DPAD_RIGHT;

	binding->input_binds[GameVirtualInputs_MenuSelect].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_MenuSelect].button_mask = XINPUT_GAMEPAD_A;

	binding->input_binds[GameVirtualInputs_MenuEscape].type = GamepadInputType_Button;
	binding->input_binds[GameVirtualInputs_MenuEscape].button_mask = XINPUT_GAMEPAD_START;
}

static bool gCheckForNewControllers = true;

void
Win32ReadInput(GameInput * previous, GameInput * current, KeyboardState * keyboard_state, KeyboardControllerBinding * keyboard_binding, GamepadControllerBinding * gamepad_binding) {
	memset(current, 0, sizeof(*current));

	Win32ReadKeyState(keyboard_state->keys);

	if (DebugConsoleHasFocus()) {
		// We ignore all "controller" input while console is active.
		return;
	}

	// *Always* capture keyboard input.
	ControllerState * previous_controller_state = &previous->controllers[ControllerIndex_Keyboard];
	ControllerState * current_controller_state = &current->controllers[ControllerIndex_Keyboard];

	current_controller_state->flags |= (ControllerFlag_IsActive | ControllerFlag_IsAttached);

	// Copy previous state:
	for (u32 i = 0; i < GameVirtualInputs_Count; ++i) {
		// TODO(bryan):  What's the proper behavior for an axis here?
		current_controller_state->input_states[i].button.was_down = previous_controller_state->input_states[i].button.is_down;
	}

#define CHECK_KEY_STATE(virt_button) (current_controller_state->input_states[(virt_button)].button.is_down = ((keyboard_state->keys[keyboard_binding->key_code[(virt_button)]] & 0x80) != 0))
	CHECK_KEY_STATE(GameVirtualInputs_Up);
	CHECK_KEY_STATE(GameVirtualInputs_Down);
	CHECK_KEY_STATE(GameVirtualInputs_Left);
	CHECK_KEY_STATE(GameVirtualInputs_Right);
	CHECK_KEY_STATE(GameVirtualInputs_Fire);
	CHECK_KEY_STATE(GameVirtualInputs_MenuUp);
	CHECK_KEY_STATE(GameVirtualInputs_MenuDown);
	CHECK_KEY_STATE(GameVirtualInputs_MenuLeft);
	CHECK_KEY_STATE(GameVirtualInputs_MenuRight);
	CHECK_KEY_STATE(GameVirtualInputs_MenuSelect);
	CHECK_KEY_STATE(GameVirtualInputs_MenuEscape);
#undef CHECK_KEY_STATE

	if (gCheckForNewControllers) {
		// Windows told us a device was attached / removed.  Re-poll XInput.
		// With a *sane* API we could do this every frame.  As it is, polling 
		// a non-existent controller is exceedingly expensive.  Seems XInput
		// simply waits for socket timeout, rather than keeping some state of
		// which controllers are attached.
		//
		// Thanks, Microsoft.

		for (u32 controller = ControllerIndex_Gamepad0; controller < ControllerIndex_Count; ++controller) {
			u32 xinput_idx = controller - ControllerIndex_Gamepad0;

			XINPUT_CAPABILITIES caps;
			if (XInputGetCapabilities(xinput_idx, 0, &caps)) {
				// We don't particularly care about the value here, we just
				// whether we get a result.
				current->controllers[controller].flags |= ControllerFlag_IsAttached;
			}
			else {
				current->controllers[controller].flags &= ~(ControllerFlag_IsAttached | ControllerFlag_IsActive);
			}
		}

		gCheckForNewControllers = false;
	}

	for (u32 controller = ControllerIndex_Gamepad0; controller < ControllerIndex_Count; ++controller) {
		ControllerState * previous_controller_state = &previous->controllers[controller];
		ControllerState * current_controller_state = &current->controllers[controller];

		for (u32 i = 0; i < GameVirtualInputs_Count; ++i) {
			// TODO(bryan):  Same as above, what should axes do here?
			current_controller_state->input_states[i].button.was_down = previous_controller_state->input_states[i].button.is_down;
		}

		u32 xinput_controller_idx = controller - ControllerIndex_Gamepad0;  // XInput controllers are zero-indexed

		XINPUT_STATE state;
		if (XInputGetState(xinput_controller_idx, &state) == ERROR_SUCCESS) {
			// TODO(bryan):  Check that we actually have a gamepad, rather than some other device claiming to be a controller.
			current_controller_state->flags |= (ControllerFlag_IsAttached | ControllerFlag_IsController);
			current_controller_state->flags |= (previous_controller_state->flags & ControllerFlag_IsActive); // Keep old "active" state.

			for (u32 bind = 0; bind < GameVirtualInputs_Count; ++bind) {
				InputState * input = &current_controller_state->input_states[bind];
//				input->flags = 0;
				switch (gamepad_binding->input_binds[bind].type) {
				case GamepadInputType_Button:
					{
						input->button.is_down = ((state.Gamepad.wButtons & gamepad_binding->input_binds[bind].button_mask) != 0);
					}
					break;
				case GamepadInputType_LThumbstick:
					{
						s16 thumb_x = state.Gamepad.sThumbLX;
						s16 thumb_y = state.Gamepad.sThumbLY;
						switch (gamepad_binding->input_binds[bind].thumbstick_direction) {
						case Direction_Up:
							{
								input->button.is_down = (thumb_y > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
							}
							break;
						case Direction_Down:
							{
								input->button.is_down = (thumb_y < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
							}
							break;
						case Direction_Left:
							{
								input->button.is_down = (thumb_x < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
							}
							break;
						case Direction_Right:
							{
								input->button.is_down = (thumb_x > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
							}
							break;
						default:
							assert(!"Invalid left thumbstick direction.");
						}
					}
					break;
				case GamepadInputType_RThumbstick:
					{
						s16 thumb_x = state.Gamepad.sThumbRX;
						s16 thumb_y = state.Gamepad.sThumbRY;
						switch (gamepad_binding->input_binds[bind].thumbstick_direction) {
						case Direction_Up:
							{
								input->button.is_down = (thumb_y > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
							}
							break;
						case Direction_Down:
							{
								input->button.is_down = (thumb_y < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
							}
							break;
						case Direction_Left:
							{
								input->button.is_down = (thumb_x < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
							}
							break;
						case Direction_Right:
							{
								input->button.is_down = (thumb_x < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
							}
							break;
						default:
							assert(!"Invalid right thumbstick direction.");
						}
					}
					break;
				case GamepadInputType_LThumbstickAxis:
					{
						s32 thumb_x = state.Gamepad.sThumbLX;
						s32 thumb_y = state.Gamepad.sThumbLY;
						input->flags |= InputStateFlag_IsAxis;
						input->axis.value = 0.0f;
						switch (gamepad_binding->input_binds[bind].thumbstick_direction) {
						case Direction_Up:
						case Direction_Down:
							{
								if (thumb_y > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
									float y = (float)(thumb_y - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
									input->axis.value = y;
								}
								else if (thumb_y < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
									float y = (float)(thumb_y + XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
									input->axis.value = y;
								}
							}
							break;
						case Direction_Left:
						case Direction_Right:
							{
								if (thumb_x > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
									float x = (float)(thumb_x - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
									input->axis.value = x;
								}
								else if (thumb_x < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
									float x = (float)(thumb_x + XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
									input->axis.value = x;
								}
							}
							break;
						}
					}
					break;
				case GamepadInputType_RThumbstickAxis:
					{
						s32 thumb_x = state.Gamepad.sThumbRX;
						s32 thumb_y = state.Gamepad.sThumbRY;
						input->flags |= InputStateFlag_IsAxis;
						input->axis.value = 0.0f;
						switch (gamepad_binding->input_binds[bind].thumbstick_direction) {
						case Direction_Up:
						case Direction_Down:
							{
								if (thumb_y > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) {
									float y = (float)(thumb_y - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
									input->axis.value = y;
								}
								else if (thumb_y < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) {
									float y = (float)(thumb_y + XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
									input->axis.value = y;
								}
							}
							break;
						case Direction_Left:
						case Direction_Right:
							{
								if (thumb_x > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) {
									float x = (float)(thumb_x - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
									input->axis.value = x;
								}
								else if (thumb_x < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) {
									float x = (float)(thumb_x + XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) / (float)(0x7fff - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
									input->axis.value = x;
								}
							}
							break;
						}
					}
					break;
				case GamepadInputType_LTrigger:
					{
						input->button.is_down = (state.Gamepad.bLeftTrigger >= XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
					}
					break;
				case GamepadInputType_RTrigger:
					{
						input->button.is_down = (state.Gamepad.bRightTrigger >= XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
					}
					break;
				default:
					assert(!"Invalid controller binding type.");
				}
			}
		}
		else {
			// Indicate the controller was unplugged.
			current_controller_state->flags &= ~(ControllerFlag_IsAttached | ControllerFlag_IsActive);
		}
	}
}

static u64
Win32TimerFrequency()
{
	LARGE_INTEGER out;
	QueryPerformanceFrequency(&out);
	return out.QuadPart;
}

static u64
Win32GetPerformanceTimer()
{
	LARGE_INTEGER out;
	QueryPerformanceCounter(&out);
	return out.QuadPart;
}

//static FILE * wave_fmt_file = NULL;
void
DebugPrintf(char * fmt, ...) {
#if 0
	if (!wave_fmt_file) {
		wave_fmt_file = fopen("wave_fmt.txt", "w");
		assert(wave_fmt_file && "Could not open debug output file.");
	}
	va_list args;
	va_start(args, fmt);
	vfprintf_s(wave_fmt_file, fmt, args);
	va_end(args);
#else
	va_list args;
	va_start(args, fmt);
	char buffer[1024] = {0};
	vsnprintf_s(buffer, array_count(buffer), fmt, args);
	va_end(args);
	OutputDebugStringA(buffer);
#endif
}

enum Win32SoundFlags {
	Win32SoundFlag_Enabled  = 1 << 0,
	Win32SoundFlag_NoMusic  = 1 << 1,
	Win32SoundFlag_Shutdown = 1 << 2,
};

enum Win32SoundOutputType {
	Win32SoundOutputType_Unknown = 0,
	Win32SoundOutputType_Float,
	Win32SoundOutputType_Integer,

	Win32SoundOutputType_Count,
};

#define SET_FLAG(field, flag) ((field) |= (flag))
#define CLEAR_FLAG(field, flag) ((field) &= ~(flag))

struct Win32Sound {
	IDirectSound * dsound;
	IDirectSoundBuffer * secondary_buffer;

	u32 flags;
	u32 channel_count;                 // Channel count the kernel expects.  While we only mix stereo, we need to output every channel.

	Win32SoundOutputType output_type;  // Will usually be float -- if not, keep track of the integer PCM we need to output.
	u32 sample_container_size_bits;    // Padded sample size -- this is how much data we write per sample.  (Must be multiple of 8.)
	u32 sample_valid_size_bits;        // Valid sample size -- this is the range we scale to for output.

	u32 frames_per_second;             // Sample rate, in samples / second.  ("Sample" here is one group of samples -- two for stereo, 6 for 5.1, etc.)
	u32 buffer_length_frames;          // How many grouped samples fit into the kernel buffer we allocate.
	u32 buffer_length_bytes;

	u32 write_cursor_offset;
	DWORD prev_write;
};

static void
Win32WriteDirectSoundBuffer(Win32Sound * sound_out, u32 write_pos_bytes, u32 write_len_bytes, SoundBuffer * sound_in) {
	assert(write_pos_bytes >= 0);
	assert(write_pos_bytes <= sound_out->buffer_length_bytes);
	assert(write_len_bytes <= (sound_in->sample_count - sound_in->play_position) * 4);
	if (write_len_bytes > 0) {
		void * write0;
		void * write1;
		DWORD write_len0;
		DWORD write_len1;
		HRESULT hr = sound_out->secondary_buffer->Lock(write_pos_bytes, write_len_bytes, &write0, &write_len0, &write1, &write_len1, 0);
		if (hr == DSERR_BUFFERLOST) {
			sound_out->secondary_buffer->Restore();
			hr = sound_out->secondary_buffer->Lock(write_pos_bytes, write_len_bytes, &write0, &write_len0, &write1, &write_len1, 0);
		}
		if (hr != DS_OK) {
			return;
		}
		float scale = (float)((1 << 16) - 2);
		float * in_sample = sound_in->samples + 2 * sound_in->play_position;
		s16 * write0_ptr = (s16 *)write0;
		u32 write_count0 = write_len0 >> 2;
		for (u32 i = 0; i < write_count0; ++i) {
			*write0_ptr++ = (s16)(*in_sample++ * scale);
			*write0_ptr++ = (s16)(*in_sample++ * scale);
		}
		s16 * write1_ptr = (s16 *)write1;
		u32 write_count1 = write_len1 >> 2;
		for (u32 i = 0; i < write_count1; ++i) {
			*write1_ptr++ = (s16)(*in_sample++ * scale);
			*write1_ptr++ = (s16)(*in_sample++ * scale);
		}
		hr = sound_out->secondary_buffer->Unlock(write0, write_len0, write1, write_len1);
	}
}

static int
Win32InitDirectSound(HWND hwnd, Win32Sound * sound_out, u32 buffer_bytes) {
	WAVEFORMATEX wfx;
	HRESULT hr;

	hr = DirectSoundCreate(NULL, &sound_out->dsound, NULL);
	if (hr != DS_OK) {
		return 0;
	}

	memset(&wfx, 0, sizeof(wfx));
	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nChannels = 2;
	wfx.nSamplesPerSec = sound_out->frames_per_second;
	wfx.wBitsPerSample = 16;
	wfx.nBlockAlign = wfx.wBitsPerSample / 8 * wfx.nChannels;
	wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

	if (hwnd == NULL) hwnd = GetDesktopWindow();
		hr = sound_out->dsound->SetCooperativeLevel((HWND)hwnd, DSSCL_PRIORITY);
	if (hr != DS_OK) {
		hr = sound_out->dsound->SetCooperativeLevel((HWND)hwnd, DSSCL_NORMAL);
	}
	else {
		DSBUFFERDESC primary_buff_desc;
		memset(&primary_buff_desc, 0, sizeof(primary_buff_desc));
		primary_buff_desc.dwSize = sizeof(primary_buff_desc);
		primary_buff_desc.dwFlags = DSBCAPS_PRIMARYBUFFER;
		primary_buff_desc.lpwfxFormat = 0;
		IDirectSoundBuffer* Ppsb = 0;
		hr = sound_out->dsound->CreateSoundBuffer(&primary_buff_desc, &Ppsb, NULL);
		if (hr == DS_OK) {
			hr = Ppsb->SetFormat(&wfx);
		}
	}

	sound_out->buffer_length_bytes = buffer_bytes;
	DSBUFFERDESC secondary_buff_desc;
	memset(&secondary_buff_desc, 0, sizeof(secondary_buff_desc));
	secondary_buff_desc.dwSize = sizeof(DSBUFFERDESC);
	secondary_buff_desc.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_GLOBALFOCUS;
	secondary_buff_desc.dwBufferBytes = buffer_bytes;
	secondary_buff_desc.lpwfxFormat = &wfx;

	hr = sound_out->dsound->CreateSoundBuffer(&secondary_buff_desc, &sound_out->secondary_buffer, NULL);
	if (hr != DS_OK) {
		return 0;
	}
	void *pclear;
	DWORD len;
	hr = sound_out->secondary_buffer->Lock(0, 0, &pclear, &len, NULL, 0, DSBLOCK_ENTIREBUFFER);
	if (hr != DS_OK) {
		return 0;
	}
	memset(pclear, 0, len);
	sound_out->secondary_buffer->Unlock(pclear, len, NULL, 0);
	hr = sound_out->secondary_buffer->SetCurrentPosition(0);
	if (hr != DS_OK)
		return 0;
	hr = sound_out->secondary_buffer->Play(0, 0, DSBPLAY_LOOPING);
	if (hr != DS_OK)
		return 0;

	return 1;
}

static Win32Sound
Win32InitSoundOut(HWND hwnd) {
	Win32Sound rv = {};
	float time_offset = (2.0f / 60.0f);
	rv.channel_count = 2;
	rv.frames_per_second = 48000;
	rv.buffer_length_frames = 1 * rv.frames_per_second;
	rv.write_cursor_offset = (u32)(rv.frames_per_second * time_offset);

	u32 buffer_bytes = rv.buffer_length_frames * 4;
	if (Win32InitDirectSound(hwnd, &rv, buffer_bytes)) {
		rv.secondary_buffer->GetCurrentPosition(NULL, &rv.prev_write);
		rv.prev_write >>= 2;
		SET_FLAG(rv.flags, Win32SoundFlag_Enabled);
	}
	return rv;
}

static u32
DSoundBufferDist(Win32Sound * sound_out, u32 first, u32 last) {
	if (last < first) {
		last += sound_out->buffer_length_frames;
	}
	return last - first;
}

static void
Win32SoundMixStep(Win32Sound * sound_out, SoundMixer * mixer, MemoryArena * arena) {
	DWORD play_cursor;
	DWORD write_cursor;
	sound_out->secondary_buffer->GetCurrentPosition(&play_cursor, &write_cursor);
	play_cursor >>= 2; 
	write_cursor >>= 2;
	u32 advance = DSoundBufferDist(sound_out, sound_out->prev_write, write_cursor);

	MixerCommitTime(mixer, advance);

	sound_out->prev_write = write_cursor;
	u32 available = DSoundBufferDist(sound_out, write_cursor, play_cursor) - sound_out->write_cursor_offset;
	if (available > 6400) {
		available = 6400;
	}

	MemoryArenaBookmark mixer_bookmark = arena->Bookmark();
	SoundBuffer mixed = MixSamplesScalar(mixer, available, arena);

	sound_out->secondary_buffer->GetCurrentPosition(&play_cursor, &write_cursor);
	play_cursor >>= 2;
	write_cursor >>= 2;
	u32 d = DSoundBufferDist(sound_out, sound_out->prev_write, write_cursor);

	if (d < available) {
		u32 where = write_cursor + sound_out->write_cursor_offset + d * 4;
		if (where >= (sound_out->buffer_length_frames)) where -= (sound_out->buffer_length_frames);
		u32 len = available - d;
		Win32WriteDirectSoundBuffer(sound_out, where * 4, len * 4, &mixed);
	}
	arena->Reset(mixer_bookmark);
}


static void
Win32StartSound(Win32Sound * sound_out) {
	if (sound_out->flags & Win32SoundFlag_Enabled) {
		sound_out->secondary_buffer->Play(0, 0, DSBPLAY_LOOPING);
	}
}

static void
Win32StopSound(Win32Sound * sound_out) {
	if (sound_out->flags & Win32SoundFlag_Enabled) {
		sound_out->secondary_buffer->Stop();
	}
}

static void
Win32SoundShutdown(Win32Sound * sound_out) {
	Win32StopSound(sound_out);

	// TODO(bryan):  Is this all we need to cleanup DirectSound?
	sound_out->secondary_buffer->Release();
	sound_out->dsound->Release();

	CLEAR_FLAG(sound_out->flags, Win32SoundFlag_Enabled);
	SET_FLAG(sound_out->flags, Win32SoundFlag_Shutdown);
}

void 
ExecuteSoundBuffer(SoundMixer * mixer, SoundCommandBuffer * buffer) {
	// Clear stopped channels:
	for (u32 i = 1; i < mixer->active_channels; ++i) {
		SoundChannel * channel = &mixer->channels[i];
		if (channel->sound.play_position >= channel->sound.sample_count) {
			if (!(channel->flags & CF_Looping)) {
				ClearChannel(mixer, i);
			}
		}
	}

	u32 cmd_buffer_pos = 0;
	while (cmd_buffer_pos < buffer->top) {
		SoundCommandType type = *(SoundCommandType *)PTR_BYTE_OFFSET(buffer->buffer, cmd_buffer_pos);
		cmd_buffer_pos += sizeof(type);

		void * cmd_data = PTR_BYTE_OFFSET(buffer->buffer, cmd_buffer_pos);

		switch (type) {
		case SC_ENUM(Bark):
			{
				SC_STRUCT(Bark) * cmd = (SC_STRUCT(Bark) *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);

				// Headphone mix assumes a listener further away (because there is less
				// travel distance from headphone to ear.)
				float listener_dist = mixer->headphone_mix ? 1024.0f : 1.0f;
				float pos_x = cmd->position.x;

				vec2 listen_vec = Vec2(pos_x, listener_dist);
				float pan = dot(normalize(listen_vec), Vec2(-1.0f, 0.0f));
				pan = (pan + 1.0f) * 0.5f;
				AddSFXBuffer(mixer, GetSFXSoundBuffer(cmd->sfx), cmd->volume, pan);
			}
			break;
		case SC_ENUM(Music):
			{	
				SC_STRUCT(Music) * cmd = (SC_STRUCT(Music) *)cmd_data;
				cmd_buffer_pos += sizeof(*cmd);

				mixer->channels[0].sound = GetSFXSoundBuffer(cmd->sfx);
				mixer->channels[0].pan = 0.5f;
				mixer->channels[0].volume = 2.0f;
				mixer->channels[0].flags = CF_Looping;
			}
			break;
		default:
			assert(0);
		}
	}

	buffer->top = 0;
}

extern bool gRunning = false;
extern bool gQuitAttempt = false;
extern bool gIsFullscreen = false;
static bool gWindowActive = false;

void 
ToggleFullscreen(HWND hwnd, RenderContext * render_context) {
	static WINDOWPLACEMENT gPreviousPlacement = { sizeof(gPreviousPlacement) };

	u32 current_style = GetWindowLong(hwnd, GWL_STYLE);
	if (current_style & WS_OVERLAPPEDWINDOW) {
		// We are not currently in fullscreen mode.
		MONITORINFO monitor_info = { sizeof(monitor_info) };
		if (GetWindowPlacement(hwnd, &gPreviousPlacement)) {
			if (GetMonitorInfo(MonitorFromWindow(hwnd, MONITOR_DEFAULTTOPRIMARY), &monitor_info)) {
				SetWindowLong(hwnd, GWL_STYLE, current_style & ~WS_OVERLAPPEDWINDOW);

				s32 monitor_left = monitor_info.rcMonitor.left;
				s32 monitor_top = monitor_info.rcMonitor.top;
				s32 monitor_width = monitor_info.rcMonitor.right - monitor_info.rcMonitor.left;
				s32 monitor_height = monitor_info.rcMonitor.bottom - monitor_info.rcMonitor.top;

				SetWindowPos(hwnd, HWND_TOP, monitor_left, monitor_top, monitor_width, monitor_height, SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

				float vertical_scale = ((float)monitor_height / (float)gScreenHeight);
				float height = gScreenHeight * vertical_scale;
				float width = gScreenWidth * vertical_scale;

				float left_offset = floorf(((float)monitor_width - width) * 0.5f);
				float top_offset = floorf(((float)monitor_height - height) * 0.5f);

				gScreenViewport = Vec4((float)monitor_left + left_offset, (float)monitor_top + top_offset, width, height);
				SetViewport(render_context, monitor_left, monitor_top, monitor_width, monitor_height);
				ShowCursor(0);
				gIsFullscreen = true;
			}
		}
	}
	else {
		// We are in fullscreen mode.
		SetWindowLong(hwnd, GWL_STYLE, current_style | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(hwnd, &gPreviousPlacement);
		SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		gScreenViewport = Vec4(0, 0, (float)gScreenWidth, (float)gScreenHeight);
		SetViewport(render_context, 0, 0, gScreenWidth, gScreenHeight);
		ShowCursor(1);
		gIsFullscreen = false;
	}
}

struct SplashScreenState {
    float time_ms;

    Sprite * logo_sprite;
    Sprite * typeout_sprite;
    Sprite * cursor_sprite;
    Sprite * interactive_sprite;

	u32 typeout_frame_count;
    s32 segment;

    RenderCommandBuffer * render_buffer;
};

static const float MS_FOR_LOGO_MOVE = 1500.0f;
static const float MS_FOR_TYPEOUT = 6.0f * 600.0f;
static const float MS_FOR_FADE_IN = 2000.0f;
static const float MS_FOR_PAUSE = 5000.0f;
static const float MS_FOR_FADE_OUT = 2000.0f;

static const float CURSOR_BLINK_FREQ_MS = 1.0f / 600.0f;

static const float LOGO_X_FINAL = -282.0f + 90.0f;

#include "colors.h"

static bool gSplashScreen = false;

static void
DrawSplashScreen(SplashScreenState * state) {
	static const float BASE_GLOW = 0.75f;  // Glow modifier for logo and "asobou" text
    switch (state->segment) {
	case -1:
		{
			// Logo fades in at center
			float t = state->time_ms / (MS_FOR_FADE_IN);
			float a_t = clamp(1.0f - (t*t*t + 0.15f), 0.0f, 1.0f);
			float a = a_t*a_t*a_t * 1.5f * PI32;

			vec2 rotation = Vec2(cosf(a), sinf(a));

			RenderSprite(state->render_buffer, state->logo_sprite, Vec2(0.0f, 35.0f), COLOR_WHITE * t, rotation, RenderLayer_UI);

			if (t >= 1.0f) {
                state->segment++;
            }
		}
		break;
    case 0:
        {
            // Logo starts in center, moves to side.
            float t = (state->time_ms - MS_FOR_FADE_IN) / (MS_FOR_LOGO_MOVE); 
            
			float one_minus_t_4 = (1.0f - t*t*t*t);
			float logo_x = LOGO_X_FINAL * (1.0f - (one_minus_t_4*one_minus_t_4*one_minus_t_4*one_minus_t_4));
            vec2 logo_position = Vec2(logo_x, 35.0f);
            RenderSprite(state->render_buffer, state->logo_sprite, logo_position, COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->logo_sprite, logo_position, COLOR_WHITE*BASE_GLOW*t, Vec2(1.0f, 0.0f), RenderLayer_UI);

            if (t >= 1.0f) {
                state->segment++;
            }
        } 
        break;
    case 1:
        {
            // "asobou" type-out animation, with blinking cursor
			RenderSprite(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE*BASE_GLOW, Vec2(1.0f, 0.0f), RenderLayer_UI);

            float t = (state->time_ms - (MS_FOR_FADE_IN + MS_FOR_LOGO_MOVE)) / (MS_FOR_TYPEOUT);
			u32 frame = (u32)floorf(state->typeout_frame_count * t);

            float cursor_alpha = cosf(state->time_ms * PI32 * CURSOR_BLINK_FREQ_MS);

			RenderSprite(state->render_buffer, state->typeout_sprite + frame, Vec2(), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->typeout_sprite + frame, Vec2(), COLOR_WHITE*BASE_GLOW, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSprite(state->render_buffer, state->cursor_sprite + frame, Vec2(), COLOR_WHITE * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->cursor_sprite + frame, Vec2(), COLOR_WHITE*BASE_GLOW * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);

            if (t >= 1.0f) {
                state->segment++;
            }
        } 
        break;
    case 2:
        {
            // "Interactive" fades in, with glow
			RenderSprite(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE*BASE_GLOW, Vec2(1.0f, 0.0f), RenderLayer_UI);

            float cursor_alpha = cosf(state->time_ms * PI32 * CURSOR_BLINK_FREQ_MS);

			RenderSprite(state->render_buffer, state->typeout_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->typeout_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE*BASE_GLOW, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSprite(state->render_buffer, state->cursor_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->cursor_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE*BASE_GLOW * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);

            float t = (state->time_ms - (MS_FOR_FADE_IN + MS_FOR_LOGO_MOVE + MS_FOR_TYPEOUT)) / (MS_FOR_FADE_IN);
            t = clamp(t, 0.0f, 1.0f);

			RenderSprite(state->render_buffer, state->interactive_sprite, Vec2(), COLOR_WHITE * t, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->interactive_sprite, Vec2(), COLOR_WHITE * t * t * t, Vec2(1.0f, 0.0f), RenderLayer_UI);

            if (t >= 1.0f) {
                state->segment++;
            }
        }
        break;
    case 3:
        {
            // Entire logo stays on screen
			RenderSprite(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE*BASE_GLOW, Vec2(1.0f, 0.0f), RenderLayer_UI);

            float cursor_alpha = cosf(state->time_ms * PI32 * CURSOR_BLINK_FREQ_MS);

			RenderSprite(state->render_buffer, state->typeout_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->typeout_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE*BASE_GLOW, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSprite(state->render_buffer, state->cursor_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->cursor_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE*BASE_GLOW * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);

			RenderSprite(state->render_buffer, state->interactive_sprite, Vec2(), COLOR_WHITE, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->interactive_sprite, Vec2(), COLOR_WHITE * (1.0f - cursor_alpha * cursor_alpha * 0.1f), Vec2(1.0f, 0.0f), RenderLayer_UI);

            float t = (state->time_ms - (MS_FOR_FADE_IN + MS_FOR_LOGO_MOVE + MS_FOR_TYPEOUT + MS_FOR_FADE_IN)) / (MS_FOR_PAUSE);

            if (t >= 1.0f) {
                state->segment++;
            }
        }
        break;
    case 4:
        {
            // Entire logo fades to black
            float t = (state->time_ms - (MS_FOR_FADE_IN + MS_FOR_LOGO_MOVE + MS_FOR_TYPEOUT + MS_FOR_FADE_IN + MS_FOR_PAUSE)) / (MS_FOR_FADE_OUT);
            float alpha = (1.0f - t);

            alpha = clamp(alpha*alpha, 0.0f, 1.0f);

			RenderSprite(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE * alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->logo_sprite, Vec2(LOGO_X_FINAL, 35.0f), COLOR_WHITE*BASE_GLOW * alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);

            float cursor_alpha = cosf(state->time_ms * PI32 * CURSOR_BLINK_FREQ_MS) * alpha;

			RenderSprite(state->render_buffer, state->typeout_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE * alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->typeout_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE*BASE_GLOW * alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSprite(state->render_buffer, state->cursor_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->cursor_sprite + state->typeout_frame_count, Vec2(), COLOR_WHITE*BASE_GLOW * cursor_alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);

			RenderSprite(state->render_buffer, state->interactive_sprite, Vec2(), COLOR_WHITE * alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);
			RenderSpriteEmission(state->render_buffer, state->interactive_sprite, Vec2(), COLOR_WHITE * alpha, Vec2(1.0f, 0.0f), RenderLayer_UI);

            if (t >= 1.0f) {
                state->segment++;
            }
        }
        break;
    }
}

static void
RunSplashScreenLoop(HWND window, RenderContext * render_context, GameAssets * assets, RenderCommandBuffer * render_buffer, RenderTarget * window_target, 
					RenderResourceCache * render_cache, MemoryArena * world_arena) {
	
	gSplashScreen = true;
	SplashScreenState splash_state = {};

    SpriteSet * splash_logo_set = &assets->assets[AssetID_SpriteSplashLogo].sprite_set;
    SpriteSet * splash_text_set = &assets->assets[AssetID_SpriteSplashText].sprite_set;
    SpriteSet * splash_cursor_set = &assets->assets[AssetID_SpriteSplashCursor].sprite_set;

    splash_state.logo_sprite = splash_logo_set->sprites + 0;
    splash_state.typeout_sprite = splash_text_set->sprites + 0;
    splash_state.cursor_sprite = splash_cursor_set->sprites + 0;
	splash_state.interactive_sprite = splash_text_set->sprites + splash_text_set->count - 1;

    splash_state.typeout_frame_count = splash_text_set->count - 2;
	splash_state.segment = -1;

    splash_state.render_buffer = render_buffer;

    u64 timer_frequency_ms = Win32TimerFrequency() / 1000;

    u64 start_splash_time = Win32GetPerformanceTimer();
    u64 last_frame_time = Win32GetPerformanceTimer();

    u64 ms_per_frame = 1000 / 60;
    MSG msg;
    while (gRunning) {
        if (splash_state.segment == 5) {
            // Splash animation complete
            break;
        }

        while (PeekMessage(&msg, window, 0, 0, 1)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            switch (msg.message) {
            case WM_KEYDOWN:
                {
                    if (msg.wParam == VK_RETURN || msg.wParam == VK_ESCAPE) {
                        // Skip splash screen
						gSplashScreen = false;
                        return;
                    }
                }
                break;
            case WM_CLOSE:
            case WM_DESTROY:
                {
                    // No prompt to exit from splash screen.
                    gRunning = false;
                }
                break;
            }
        }

        u64 cur_splash_time = Win32GetPerformanceTimer();

        splash_state.time_ms = (float)(cur_splash_time - start_splash_time) / (float)timer_frequency_ms;

        DrawSplashScreen(&splash_state);

        ExecuteRenderBuffer(render_buffer, window_target, render_cache, world_arena);

        u64 this_frame_time = Win32GetPerformanceTimer();
        u64 frame_dt = (this_frame_time - last_frame_time) / timer_frequency_ms;

        if (frame_dt < ms_per_frame) {
            Sleep((DWORD)(ms_per_frame - frame_dt));
            do {
                this_frame_time = Win32GetPerformanceTimer();
                frame_dt = (this_frame_time - last_frame_time) / timer_frequency_ms;
            } while (frame_dt < ms_per_frame);
        }
        else {
            DebugPrintf("Frame drop: %llums\n", frame_dt);
        }
        last_frame_time = this_frame_time;

		SwapBuffers(render_context);
		ClearBuffer(render_context);
    }
	gSplashScreen = false;
} 

LRESULT CALLBACK
Win32MainWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		{
			if (gQuitAttempt || gSplashScreen) {
				// Skip the confirm screen if the user is trying hard to kill us.
				gRunning = false;
			}
			else {
				gQuitAttempt = true;
			}
			return 0;
		} break;
	case WM_SIZE:
		{
			if (wParam == SIZE_MINIMIZED) {
				gIsMinimized = true;
			}
			else if (wParam == SIZE_RESTORED) {
				gIsMinimized = false;
			}
		} break;
	case WM_ACTIVATE:
		{
			if ((wParam & 0x3) == WA_INACTIVE) {
				gWindowActive = false;
			}
			else {
				gWindowActive = true;
			}
			return 0;
		} break;
	case WM_DEVICECHANGE:
		{
			gCheckForNewControllers = true;
		}
		break;
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	return 0;
}

int CALLBACK 
WinMain(HINSTANCE hInstance,
		HINSTANCE hPrevInstance,
		LPSTR lpCmdLine,
		int nCmdShow)
{
	WNDCLASSEX windowClass = { 0 };
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = Win32MainWindowProc;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(101));
	windowClass.hCursor = LoadCursor(0, IDC_ARROW);
	windowClass.lpszClassName = TEXT("InvadersGameMainWindowClass");

	RegisterClassEx(&windowClass);

	RECT client_size;
	client_size.top = 0;
	client_size.left = 0;
	client_size.right = gScreenWidth;
	client_size.bottom = gScreenHeight;

	AdjustWindowRect(&client_size, WS_OVERLAPPEDWINDOW, FALSE);

	HWND window = CreateWindowEx(0, windowClass.lpszClassName, 
								 TEXT("Invaders!"), 
								 WS_OVERLAPPEDWINDOW, 
								 CW_USEDEFAULT, CW_USEDEFAULT, 
								 client_size.right - client_size.left, 
								 client_size.bottom - client_size.top, 
								 NULL, NULL, hInstance, NULL);
	assert(window);

	//ShowCursor(0);

	Win32InitXInput();
	Win32InitDSoundLib();

	Win32Sound sound_out = Win32InitSoundOut(window);
//	Win32StartSound(&sound_out);

	timeBeginPeriod(1);
	u64 timer_frequency_ms = Win32TimerFrequency() / 1000;

	void * memory_block = VirtualAlloc(NULL, Megabytes(50), MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
	MemoryArena world_arena = MemoryArena(memory_block, Megabytes(10), "world_arena");
	MemoryArena load_scratch_arena = MemoryArena(PTR_BYTE_OFFSET(memory_block, world_arena.capacity), Megabytes(20), "load_scratch_arena");
	MemoryArena render_arena = MemoryArena(PTR_BYTE_OFFSET(memory_block, world_arena.capacity + load_scratch_arena.capacity), Kilobytes(256), "render_arena");
	MemoryArena game_arena = MemoryArena(PTR_BYTE_OFFSET(memory_block, world_arena.capacity + load_scratch_arena.capacity + render_arena.capacity), Megabytes(4), "game_arena");

	RenderContext * render_context = CreateRenderContext(&world_arena, window);
	assert(render_context);
	
	SetCurrentContext(render_context);

	SetViewport(render_context, 0, 0, gScreenWidth, gScreenHeight);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	SoundCommandBuffer sound_command_buffer;
	sound_command_buffer.top = 0;
	sound_command_buffer.size = Kilobytes(16);
	sound_command_buffer.buffer = world_arena.AllocateZeroed(sound_command_buffer.size);

	RenderCommandBuffer render_command_buffer;
	render_command_buffer.top = 0;
	render_command_buffer.size = Kilobytes(128);
	render_command_buffer.buffer = world_arena.Allocate(render_command_buffer.size);

	u32 transforms_capacity = 1024 * 2;// TODO(bryan):  Adjust this?
	render_command_buffer.transforms.capacity = transforms_capacity;
	render_command_buffer.transforms.count = 0;
	render_command_buffer.transforms.px = (float *)world_arena.AllocateAligned(sizeof(float) * transforms_capacity, 16);
	render_command_buffer.transforms.py = (float *)world_arena.AllocateAligned(sizeof(float) * transforms_capacity, 16);
	render_command_buffer.transforms.sx = (float *)world_arena.AllocateAligned(sizeof(float) * transforms_capacity, 16);
	render_command_buffer.transforms.sy = (float *)world_arena.AllocateAligned(sizeof(float) * transforms_capacity, 16);
	render_command_buffer.transforms.rx = (float *)world_arena.AllocateAligned(sizeof(float) * transforms_capacity, 16);
	render_command_buffer.transforms.ry = (float *)world_arena.AllocateAligned(sizeof(float) * transforms_capacity, 16);

	RenderResourceCache render_state;
	InitRenderResourceCache(&render_state);

	RenderTarget window_target;
	window_target.size = Vec2((float)gScreenWidth, (float)gScreenHeight);
	window_target.gl_framebuffer = 0;

	GameAssets game_assets;
	LoadAssets(&game_assets, &world_arena, &load_scratch_arena);

	SpriteSet * sset = &game_assets.assets[AssetID_SpriteShip].sprite_set;
	SpriteSet * player_debis_set = &game_assets.assets[AssetID_SpriteShipDebris].sprite_set;
	SpriteSet * invader_set = &game_assets.assets[AssetID_SpriteInvader].sprite_set;
	SpriteSet * saucer_set = &game_assets.assets[AssetID_SpriteSaucer].sprite_set;
	SpriteSet * saucer_emission_set = &game_assets.assets[AssetID_SpriteSaucerEmission].sprite_set;
	SpriteSet * bg_set = &game_assets.assets[AssetID_SpriteBG].sprite_set;
	SpriteSet * bg_emission_set = &game_assets.assets[AssetID_SpriteBGEmission].sprite_set;
	SpriteSet * projectile_set = &game_assets.assets[AssetID_SpriteProjectiles].sprite_set;
	SpriteSet * button_set = &game_assets.assets[AssetID_SpriteButtons].sprite_set;
	SpriteSet * wide_button_set = &game_assets.assets[AssetID_SpriteWideButtons].sprite_set;

	RandomState main_rng;
	Random_Seed(&main_rng, 0xabad1dea03243241LL);
	Random_InitVectorTable(&main_rng);

	InitSoundCache((u32)sound_out.frames_per_second, &world_arena, &game_assets);

	SoundMixer mixer;

	mixer.active_channels = 1;
	SetMusic(&sound_command_buffer, DEBUG_Silence);

	ShowWindow(window, nCmdShow);
	ClearBuffer(render_context);
	SwapBuffers(render_context);

	gRunning = true;
#if 1//_DEBUG
	RunSplashScreenLoop(window, render_context, &game_assets, &render_command_buffer, &window_target, &render_state, &world_arena);
#endif

	float time = 0.0f;

	GameState game_state = { };
	game_state.background_sprite = bg_set->sprites + 0;
	game_state.background_emission = bg_emission_set->sprites + 0;
	game_state.player_sprite = sset->sprites + 0;
	game_state.player_emission_sprite = sset->sprites + 3;
	game_state.player_debris_sprite = player_debis_set->sprites + 0;
	game_state.invader_sprite = invader_set->sprites + 0;
	game_state.invader_emission_sprite = invader_set->sprites + 2;
	game_state.saucer_sprite = saucer_set->sprites;
	game_state.saucer_emission_sprite = saucer_emission_set->sprites;
	game_state.lazer_sprite = projectile_set->sprites + 0;
	game_state.plasma_sprite = projectile_set->sprites + 2;
	game_state.debris_sprite = projectile_set->sprites + 5;
	game_state.shield_pickup_sprite = projectile_set->sprites + 6;
	game_state.health_pickup_sprite = projectile_set->sprites + 7;
	game_state.button_sprite = button_set->sprites + 0;
	game_state.wide_button_sprite = wide_button_set->sprites + 0;
	
	// TODO(bryan):  Not sure if we really want to do this.  There are
	// quite a few things using this scratch arena (game update, mixer, 
	// etc.), which can have disasterous consequences if something clears
	// the arena when some other code doesn't expect it.
	//
	// Give each role it's own scratch arena.  We can reuse the same
	// memory block as the load arena if we need to.
	game_state.scratch_arena = &load_scratch_arena;

	LoadGameOptions(&game_state, &load_scratch_arena);
	LoadHighScores(&game_state, &load_scratch_arena);

	KeyboardState key_state_a = { };
	KeyboardState key_state_b = { };

	KeyboardState * new_key_state = &key_state_a;
	KeyboardState * prev_key_state = &key_state_b;

	GameInput game_input_a = { };
	GameInput game_input_b = { };
	GameInput game_input_null = { };

	GameInput * game_input_cur = &game_input_a;
	GameInput * game_input_prev = &game_input_b;
	GameInput * game_input_saved = &game_input_null;

	KeyboardControllerBinding keyboard_binding = {};
	SetDefaultKeyboardBinding(&keyboard_binding);
	GamepadControllerBinding gamepad_binding = {};
	SetDefaultGamepadBinding(&gamepad_binding);

	u64 last_input_frame_time = Win32GetPerformanceTimer();
	u64 last_frame_time = Win32GetPerformanceTimer();

	float sound_out_timer = 0;

	u64 ms_per_frame = 1000 / 60;

	MSG msg;
	while (gRunning)
	{
		if (!gWindowActive) {
			game_state.paused = true;
		}

		if (gQuitAttempt && game_state.menu_state.current_menu != MenuId_QuitConfirm) {
			SwitchMenu(MenuId_QuitConfirm, &game_state);
		}

		if (gWindowActive) {
			{
				KeyboardState * tmp = prev_key_state;
				prev_key_state = new_key_state;
				new_key_state = tmp;
			}
			if (game_input_cur == &game_input_null) {
				game_input_cur = game_input_saved;
				game_input_saved = &game_input_null;
			}
			{
				GameInput * tmp = game_input_prev;
				game_input_prev = game_input_cur;
				game_input_cur = tmp;
			}
			Win32ReadInput(game_input_prev, game_input_cur, new_key_state, &keyboard_binding, &gamepad_binding);

			game_input_cur->previous = prev_key_state;
			game_input_cur->current = new_key_state;

			// Activate controller when Start is pressed.
			for (u32 i = ControllerIndex_Gamepad0; i < ControllerIndex_Count; ++i) {
				if (game_input_cur->controllers[i].flags & ControllerFlag_IsController) {
					if (game_input_cur->controllers[i].input_states[GameVirtualInputs_MenuEscape].button.is_down) {
						game_input_cur->controllers[i].flags |= ControllerFlag_IsActive;
						game_state.current_controller = i;
					}
				}
			}
		}
		else {
			if (game_input_saved == &game_input_null) game_input_saved = game_input_cur;
			game_input_cur = &game_input_null; 
		}

		u64 this_input_frame_time = Win32GetPerformanceTimer();
		u64 dt = this_input_frame_time - last_input_frame_time;
		last_input_frame_time = this_input_frame_time;

		game_input_cur->delta_time = (float)dt / (float)timer_frequency_ms;
		sound_out_timer += game_input_cur->delta_time;
		time += game_input_cur->delta_time;

		if (game_input_cur->delta_time > 16.67f) {
			// Clamp to a max dt of 16.67ms
			game_input_cur->delta_time = 16.67f;
		}

		while (PeekMessage(&msg, window, 0, 0, 1))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_SYSKEYDOWN) {
				// TODO(bryan):  This will ping-pong between fullscreen and window while
				// the alt+enter chord is held.  Should probably fix that.
				u32 alt_is_down = (msg.lParam & (1 << 29)) != 0;

				if (msg.wParam == VK_RETURN && alt_is_down) {
					ToggleFullscreen(window, render_context);
					ForceSetTimer(&game_state, Timer_MenuTimeout, 500.0f);
				}
			}
#if DEBUG_CONSOLE
			else if (msg.message == WM_KEYDOWN) {
				if (msg.wParam == VK_OEM_3) {
					// Tilde
					ToggleDebugConsole();
					SetDebugFocus(true);
				}
				if (msg.wParam == VK_TAB) {
					SetDebugFocus(!DebugConsoleHasFocus());
				}
			}
#endif
		}

		bool prev_is_fullscreen = gIsFullscreen;

		GameUpdate(&game_state, game_input_cur, &render_command_buffer, &sound_command_buffer, &game_arena);

		if (prev_is_fullscreen != gIsFullscreen) {
			ToggleFullscreen(window, render_context);
		}

		UpdateDebugConsole(&render_command_buffer, new_key_state, prev_key_state, &game_state);

		ExecuteSoundBuffer(&mixer, &sound_command_buffer);

		mixer.master_volume = game_state.options.master_volume;
		mixer.music_volume = game_state.options.music_volume;
		mixer.effects_volume = game_state.options.effect_volume;
		mixer.mute = game_state.options.mute;
		mixer.headphone_mix = game_state.options.headphone_mix;

		if (sound_out.flags & Win32SoundFlag_Enabled) {
			Win32SoundMixStep(&sound_out, &mixer, &load_scratch_arena);
		}
		
		if (time <= MS_FOR_FADE_OUT) {
			// Fade in from splash screen
			vec2 screen_min = Vec2(gScreenWidth * -0.5f, gScreenHeight *  0.5f);
			vec2 screen_max = Vec2(gScreenWidth *  0.5f, gScreenHeight * -0.5f);
			float t = (time / (float)MS_FOR_FADE_OUT);
			float fade_alpha = 1.0f - t*t;

			RenderRectangle(&render_command_buffer, screen_min, screen_max, COLOR_BLACK * fade_alpha, RenderLayer_UI); 
		}

		if (gIsMinimized) {
			render_command_buffer.top = 0;
			render_command_buffer.transforms.count = 0;
		}
		else {
			ExecuteRenderBuffer(&render_command_buffer, &window_target, &render_state, &render_arena);
		}
		// Controller vibration
		if (game_state.paused) {
			// Turn off vibration while game is paused.
			XINPUT_VIBRATION vibe = {};
			for (u32 i = 0; i < 4; ++i) {
				if (game_input_cur->controllers[i + 1].flags & ControllerFlag_IsAttached) {
					XInputSetState(i, &vibe);
				}
			}
		}
		else {
			float vibe_amount = abs(game_state.screen_shake) * 5000;
			vibe_amount = clamp(vibe_amount, 0.0f, 65535.0f);
			XINPUT_VIBRATION vibe;
			vibe.wLeftMotorSpeed = (u16)vibe_amount;
			vibe.wRightMotorSpeed = (u16)vibe_amount;
			for (u32 i = 0; i < 4; ++i) {
				bool live = (game_input_cur->controllers[i + 1].flags & ControllerFlag_IsActive) && 
							(game_input_cur->controllers[i + 1].flags & ControllerFlag_IsAttached);
				if (live) {
					XInputSetState(i, &vibe);
				}
			}
		}

		u64 this_frame_time = Win32GetPerformanceTimer();
		u64 frame_dt = (this_frame_time - last_frame_time) / timer_frequency_ms;

		game_state.DEBUG_last_frame_dt = frame_dt;

		if (frame_dt < ms_per_frame) {
			DebugPrintf("Input / Frame times: %fms / %llums\n", game_input_cur->delta_time, frame_dt);
			Sleep((DWORD)(ms_per_frame - frame_dt));
			do {
				this_frame_time = Win32GetPerformanceTimer();
				frame_dt = (this_frame_time - last_frame_time) / timer_frequency_ms;
			} while (frame_dt < ms_per_frame);
		}
		else {
			DebugPrintf("Frame drop: %llums\n", frame_dt);
		}
		last_frame_time = this_frame_time;

		SwapBuffers(render_context);
		ClearBuffer(render_context);
	}

	Win32SoundShutdown(&sound_out);

	// Unset our increase scheduler resolution.
	// Not strictly neccessary, process termination should do this for us.
	timeEndPeriod(1);

	return 0;
}