//
// Invaders - mat4x4.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include "mat4x4.h"

mat4x4
operator*(mat4x4 a, mat4x4 b) {
	mat4x4 result;

	result.m[0]  = a.m[0] * b.m[0] + a.m[4] * b.m[1] + a.m[8]  * b.m[2] + a.m[12] * b.m[3];
	result.m[1]  = a.m[1] * b.m[0] + a.m[5] * b.m[1] + a.m[9]  * b.m[2] + a.m[13] * b.m[3];
	result.m[2]  = a.m[2] * b.m[0] + a.m[6] * b.m[1] + a.m[10] * b.m[2] + a.m[14] * b.m[3];
	result.m[3]  = a.m[3] * b.m[0] + a.m[7] * b.m[1] + a.m[11] * b.m[2] + a.m[15] * b.m[3];

	result.m[4]  = a.m[0] * b.m[4] + a.m[4] * b.m[5] + a.m[8]  * b.m[6] + a.m[12] * b.m[7];
	result.m[5]  = a.m[1] * b.m[4] + a.m[5] * b.m[5] + a.m[9]  * b.m[6] + a.m[13] * b.m[7];
	result.m[6]  = a.m[2] * b.m[4] + a.m[6] * b.m[5] + a.m[10] * b.m[6] + a.m[14] * b.m[7];
	result.m[7]  = a.m[3] * b.m[4] + a.m[7] * b.m[5] + a.m[11] * b.m[6] + a.m[15] * b.m[7];

	result.m[8]  = a.m[0] * b.m[8] + a.m[4] * b.m[9] + a.m[8]  * b.m[10] + a.m[12] * b.m[11];
	result.m[9]  = a.m[1] * b.m[8] + a.m[5] * b.m[9] + a.m[9]  * b.m[10] + a.m[13] * b.m[11];
	result.m[10] = a.m[2] * b.m[8] + a.m[6] * b.m[9] + a.m[10] * b.m[10] + a.m[14] * b.m[11];
	result.m[11] = a.m[3] * b.m[8] + a.m[7] * b.m[9] + a.m[11] * b.m[10] + a.m[15] * b.m[11];

	result.m[12] = a.m[0] * b.m[12] + a.m[4] * b.m[13] + a.m[8]  * b.m[14] + a.m[12] * b.m[15];
	result.m[13] = a.m[1] * b.m[12] + a.m[5] * b.m[13] + a.m[9]  * b.m[14] + a.m[13] * b.m[15];
	result.m[14] = a.m[2] * b.m[12] + a.m[6] * b.m[13] + a.m[10] * b.m[14] + a.m[14] * b.m[15];
	result.m[15] = a.m[3] * b.m[12] + a.m[7] * b.m[13] + a.m[11] * b.m[14] + a.m[15] * b.m[15];

	return result;
}

vec4
operator*(mat4x4 a, vec4 b) {
	vec4 rv;
	rv.x = a.m[0] * b.x + a.m[4] * b.y + a.m[8]  * b.z + a.m[12] * b.w;
	rv.y = a.m[1] * b.x + a.m[5] * b.y + a.m[9]  * b.z + a.m[13] * b.w;
	rv.z = a.m[2] * b.x + a.m[6] * b.y + a.m[10] * b.z + a.m[14] * b.w;
	rv.w = a.m[3] * b.x + a.m[7] * b.y + a.m[11] * b.z + a.m[15] * b.w;
	return rv;
}

mat4x4
OrthoProjectionMatrix(float r, float l, float t, float b, float n, float f) {
	mat4x4 result = {};
	result.m[0]  = ( 2.0f) / (r - l);
	result.m[5]  = ( 2.0f) / (t - b);
	result.m[10] = (-2.0f) / (f - n);

	result.m[12] = (-1.0f) * ((r + l) / (r - l));
	result.m[13] = (-1.0f) * ((t + b) / (t - b));
	result.m[14] = ( 1.0f) * ((f + n) / (f - n));
	result.m[15] = 1.0f;

	return result;
}

mat4x4
InverseOrthoProjectionMatrix(float r, float l, float t, float b, float n, float f) {
	mat4x4 result = {};

	result.m[0]  = (r - l) / 2.0f;
	result.m[5]  = (t - b) / 2.0f;
	result.m[10] = (f - n) / 2.0f;

	result.m[12] = (l + r) / 2.0f;
	result.m[13] = (t + b) / 2.0f;
	result.m[14] = (f + n) / 2.0f;
	result.m[15] = 1.0f;

	return result;
}

mat4x4
IdentityMatrix() {
	mat4x4 result = {};

	result.m[0]  = 1.0f;
	result.m[5]  = 1.0f;
	result.m[10] = 1.0f;
	result.m[15] = 1.0f;
	
	return result;
}

mat4x4
MatrixFromQuaternion(quaternion q) {
	mat4x4 rv = IdentityMatrix();
	
	rv.m[0] = 1 - 2*q.y*q.y - 2*q.z*q.z;
	rv.m[1] = 2*q.x*q.y + 2*q.w*q.z;
	rv.m[2] = 2*q.x*q.z - 2*q.w*q.y;

	rv.m[4] = 2*q.x*q.y - 2*q.w*q.z;
	rv.m[5] = 1 - 2*q.x*q.x - 2*q.z*q.z;
	rv.m[6] = 2*q.y*q.z + 2*q.w*q.x;

	rv.m[8] = 2*q.x*q.z + 2*q.w*q.y;
	rv.m[9] = 2*q.y*q.z - 2*q.w*q.x;
	rv.m[10] = 1 - 2*q.x*q.x - 2*q.y*q.y;

	return rv;
}

// 2x2

//
// [ 0  2 ]
// [ 1  3 ]
//

mat2x2 
operator*(mat2x2 a, mat2x2 b) {
	mat2x2 rv;

	rv.m[0] = a.m[0]*b.m[0] + a.m[2]*b.m[1];
	rv.m[1] = a.m[1]*b.m[0] + a.m[3]*b.m[1];
	rv.m[2] = a.m[0]*b.m[2] + a.m[2]*b.m[3];
	rv.m[3] = a.m[1]*b.m[2] + a.m[3]*b.m[3];

	return rv;
}

vec2 
operator*(mat2x2 a, vec2 b) {
	return Vec2(a.m[0]*b.x + a.m[2]*b.y, a.m[1]*b.x + a.m[3]*b.y);
}

mat2x2 
RotationMatrix(float theta) {
	mat2x2 rv;
	float s = sinf(theta);
	float c = cosf(theta);
	rv.m[0] = c;
	rv.m[1] = s;
	rv.m[2] = -s;
	rv.m[3] = c;

	return rv;
}
