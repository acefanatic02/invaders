//
// Invaders - mat4x4.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"

//  Column-major as per GL convention.
//
//  Indices:
//  [ 0  4   8  12 ]
//  | 1  5   9  13 |
//  | 2  6  10  14 |
//  [ 3  7  11  15 ]
//
struct mat4x4 {
	float m[16];
};

mat4x4 operator*(mat4x4 a, mat4x4 b);

vec4 operator*(mat4x4 a, vec4 b);

mat4x4 OrthoProjectionMatrix(float r, float l, float t, float b, float n, float f);

mat4x4 InverseOrthoProjectionMatrix(float r, float l, float t, float b, float n, float f);

mat4x4 IdentityMatrix();

mat4x4 MatrixFromQuaternion(quaternion q);


// 0 2
// 1 3
struct mat2x2 {
	float m[4];
};

mat2x2 operator*(mat2x2 a, mat2x2 b);
vec2 operator*(mat2x2 a, vec2 b);

mat2x2 RotationMatrix(float theta);