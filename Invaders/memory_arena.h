//
// Invaders - memory_arena.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"

#include <cstring>

struct MemoryArenaBookmark {
	size_t top;
};

struct MemoryArena {
	void * memory;
	size_t capacity;
	size_t top;
	char * name;
	size_t high_water_mark;

	inline MemoryArena(void * _memory, size_t _capacity, char * _name);

	inline void * Allocate(size_t size);
	inline void * AllocateZeroed(size_t size);
	inline void * AllocateAligned(size_t size, size_t align);
	inline void * AllocateAlignedZeroed(size_t size, size_t align);

	inline MemoryArenaBookmark Bookmark();

	inline void Reset();
	inline void Reset(MemoryArenaBookmark bookmark);
};

inline
MemoryArena::MemoryArena(void * _memory, size_t _capacity, char * _name) :
	memory(_memory),
	capacity(_capacity),
	top(0),
	name(_name),
	high_water_mark(0)
{
}

inline void *
MemoryArena::Allocate(size_t size) {
	assert((top + size) <= capacity);
	
	void * ptr = NULL;
	if ((top + size) <= capacity) {
		ptr = PTR_BYTE_OFFSET(memory, top);
		top += size;

		if (top > high_water_mark) {
			high_water_mark = top;
		}
	}
	return ptr;
}

inline void *
MemoryArena::AllocateZeroed(size_t size) {
	void * ptr = Allocate(size);
	if (ptr) memset(ptr, 0, size);
	return ptr;
}

inline void *
MemoryArena::AllocateAligned(size_t size, size_t align) {
	size_t aligned_top = top;
	if (aligned_top % align) {
		aligned_top += align - (aligned_top % align);
	}

	assert((aligned_top + size) <= capacity);

	void * ptr = NULL;
	if ((aligned_top + size) <= capacity) {
		ptr = PTR_BYTE_OFFSET(memory, aligned_top);
		top = aligned_top + size;

		if (top > high_water_mark) {
			high_water_mark = top;
		}
	}
	return ptr;
}

inline void *
MemoryArena::AllocateAlignedZeroed(size_t size, size_t align) {
	void * ptr = AllocateAligned(size, align);
	if (ptr) memset(ptr, 0, size);
	return ptr;
}

inline MemoryArenaBookmark
MemoryArena::Bookmark() {
	MemoryArenaBookmark rv;
	rv.top = top;
	return rv;
}

inline void
MemoryArena::Reset() {
	top = 0;
}

inline void
MemoryArena::Reset(MemoryArenaBookmark bookmark) {
	top = bookmark.top;
}
