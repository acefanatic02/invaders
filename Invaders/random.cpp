//
// Invaders - random.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include "random.h"
#include "vectypes.h"

// Implements an xorshift 1024* PRNG.
// http://xorshift.di.unimi.it/

void 
Random_Seed(RandomState * state, u64 seed) {
	if (seed == 0) {
		// Alternating 1 and 0 bits.
		// xorshift requires a non-zero seed value.
		seed = 0x5555555555555555LL;
	}

	state->_p = 0;

	// Use an xorshift64* PRNG to seed the state.
	u64 x = seed;
	for (u32 i = 0; i < array_count(state->_state); ++i) {
		x ^= x >> 12;
		x ^= x << 25;
		x ^= x >> 27;
		state->_state[i] = x * 2685821657736338717LL;
	}
}

u64 
Random_Next(RandomState * state) {
	u64 s0 = state->_state[state->_p];
	state->_p++;
	state->_p &= 15;
	u64 s1 = state->_state[state->_p];

	s1 ^= s1 << 31;
	s1 ^= s1 >> 11;
	s0 &= s0 >> 30;
	state->_state[state->_p] = s0 ^ s1;
	return state->_state[state->_p] * 1181783497276652981LL;
}

s64 
Random_NextInt(RandomState * state) {
	return (s64)Random_Next(state);
}

float 
Random_NextFloat01(RandomState * state) {
	const u64 MAX_RAND_VALUE = 0xFFFFFFFFFFFFFFFFLL;
	float f = (float)Random_Next(state) / (float)MAX_RAND_VALUE;
	return clamp(f, 0.0f, 1.0f);
}

float
Random_NextFloat11(RandomState * state) {
	float f = Random_NextFloat01(state);
	return (f * 2.0f) - 1.0f;
}

bool 
Random_WeightedChoice(RandomState * state, float probability) {
	float f = Random_NextFloat01(state);
	return (f <= probability);
}

void 
Random_ChooseN(RandomState * state, u32 * out_buffer, u32 choose_count, u32 list_count) {
	assert(choose_count < list_count);

	for (u32 i = 0; i < choose_count; ++i) {
retry:
		u32 choice = (u32)(Random_Next(state) % list_count);
		for (u32 j = 0; j < i; ++j) {
			if (out_buffer[j] == choice) {
				// This is the cleanest way to do this I can think of.  Yes, really.
				goto retry;
			}
		}

		out_buffer[i] = choice;
	}
}

static vec2 RandomVec2Table[256];
static u32 RandomVec2Idx = 0;

void 
Random_InitVectorTable(RandomState * state) {
	for (u32 i = 0; i < array_count(RandomVec2Table); ++i) {
		float theta = Random_NextFloat01(state);
		theta *= (2.0f * PI32);

		RandomVec2Table[i].x = cos(theta);
		RandomVec2Table[i].y = sin(theta);
	}
}

vec2
NextRandVec2() {
	vec2 result = RandomVec2Table[RandomVec2Idx++];

	RandomVec2Idx %= array_count(RandomVec2Table);

	return result;
}
