//
// Invaders - random.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//
#pragma once

#include "invaders.h"
#include "vectypes.h"

struct RandomState {
	u64 _state[16];
	s32 _p;
};

void Random_Seed(RandomState * state, u64 seed);
u64 Random_Next(RandomState * state);
s64 Random_NextInt(RandomState * state);
float Random_NextFloat01(RandomState * state);
float Random_NextFloat11(RandomState * state);

bool Random_WeightedChoice(RandomState * state, float probability);

void Random_ChooseN(RandomState * state, u32 * out_buffer, u32 choose_count, u32 list_count);

void Random_InitVectorTable(RandomState * state);
vec2 NextRandVec2();
