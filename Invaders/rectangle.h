//
// Invaders - rectangle.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "vectypes.h"

struct Rect {
	vec2 center;
	vec2 half_extents;
};

inline Rect
RectFromCenterHalfExtent(vec2 center, vec2 half_extents) {
	Rect result;

	result.center = center;
	result.half_extents = half_extents;

	return result;
}

inline Rect
RectFromMinMax(vec2 min, vec2 max) {
	Rect result;

	vec2 half_extent = (max - min) * 0.5f;
	result.center = min + half_extent;
	result.half_extents = half_extent;

	return result;
}

inline vec2
GetRectMin(Rect r) {
	return r.center - r.half_extents;
}

inline vec2
GetRectMax(Rect r) {
	return r.center + r.half_extents;
}

inline bool
PointInRect(vec2 p, Rect r) {
	vec2 rel_p = p - r.center;

	if (rel_p.x <= -r.half_extents.x || rel_p.x > r.half_extents.x ||
		rel_p.y <= -r.half_extents.y || rel_p.y > r.half_extents.y) {

		return false;
	}

	return true;
}

inline bool
RectInsideRect(Rect bounds, Rect check) {
	return PointInRect(GetRectMin(check), bounds)
		&& PointInRect(GetRectMax(check), bounds);
}