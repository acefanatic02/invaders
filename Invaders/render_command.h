//
// Invaders - render_command.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"

struct Sprite;

struct SOA_Transform {
	u32 capacity;
	u32 count;
	float * px;
	float * py;
	float * sx;
	float * sy;
	float * rx;
	float * ry;
};

struct RenderCommandBuffer {
	u32 size;
	u32 top;
	void * buffer;
	SOA_Transform transforms;
};

const u32 ZOrder_Player = 0x4;

enum RenderLayer {
	RenderLayer_Background,
	RenderLayer_Foreground,
	RenderLayer_UI,
	RenderLayer_Count,
};

enum RenderTargetBuffer {
	RenderTargetBuffer_Emission,
	RenderTargetBuffer_Main,
};

enum PostEffectProperty {
	PostEffect_ScreenShakeIntensity,
};

void RenderRectangle(RenderCommandBuffer * buffer, vec2 min, vec2 max, vec4 color, RenderLayer layer=RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderRectangleEmission(RenderCommandBuffer * buffer, vec2 min, vec2 max, vec4 color, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderOutlineRect(RenderCommandBuffer * buffer, vec2 min, vec2 max, vec4 color, float thickness, RenderLayer layer=RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderCircle(RenderCommandBuffer * buffer, vec2 center, float radius, vec4 color, float thickness = 0.0f, RenderLayer layer=RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderCircleEmission(RenderCommandBuffer * buffer, vec2 center, float radius, vec4 color, float thickness = 0.0f, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderSprite(RenderCommandBuffer * buffer, Sprite * sprite, vec2 position, vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2 rotation=Vec2(1.0f, 0.0f), RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderSpriteEmission(RenderCommandBuffer * buffer, Sprite * sprite, vec2 position, vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2 rotation = Vec2(1.0f, 0.0f), RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderText(RenderCommandBuffer * buffer, vec2 position, char * string, vec2 scale = Vec2(1.0f, 1.0f), vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer layer = RenderLayer_Foreground, u32 zorder = ZOrder_Player);
void RenderTextEmission(RenderCommandBuffer * buffer, vec2 position, char * string, vec2 scale = Vec2(1.0f, 1.0f), vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer layer = RenderLayer_Foreground, u32 zorder = ZOrder_Player);

struct TransformComponent;

void RenderRectangle(RenderCommandBuffer * buffer, vec2 size, vec4 color, TransformComponent * transform, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderRectangleEmission(RenderCommandBuffer * buffer, vec2 size, vec4 color, TransformComponent * transform, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderOutlineRect(RenderCommandBuffer * buffer, vec2 size, vec4 color, float thickness, TransformComponent * transform, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderCircle(RenderCommandBuffer * buffer, float radius, vec4 color, TransformComponent * transform, float thickness = 0.0f, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderCircleEmission(RenderCommandBuffer * buffer, float radius, vec4 color, TransformComponent * transform, float thickness = 0.0f, RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderSprite(RenderCommandBuffer * buffer, Sprite * sprite, TransformComponent * transform, vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderSpriteEmission(RenderCommandBuffer * buffer, Sprite * sprite, TransformComponent * transform, vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer layer = RenderLayer_Background, u32 zorder = ZOrder_Player);
void RenderText(RenderCommandBuffer * buffer, char * string, TransformComponent * transform, vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer layer = RenderLayer_Foreground, u32 zorder = ZOrder_Player);
void RenderTextEmission(RenderCommandBuffer * buffer, char * string, TransformComponent * transform, vec4 color = Vec4(1.0f, 1.0f, 1.0f, 1.0f), RenderLayer layer = RenderLayer_Foreground, u32 zorder = ZOrder_Player);


void SetPostEffectProperty(RenderCommandBuffer * buffer, PostEffectProperty prop, float value);