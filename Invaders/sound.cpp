//
// Invaders - sound.cpp
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#include "invaders.h"
#include "sound.h"
#include "memory_arena.h"
#include "loader.h"
#include "asset.h"

#define SOUND_WAVE_FUNC(name) float name(float t)
typedef SOUND_WAVE_FUNC(sound_wave_func);

static SoundBuffer
GenerateSoundBuffer(sound_wave_func * f, float seconds, u32 samples_per_second, float volume, MemoryArena * dst_arena) {
	SoundBuffer result = {};
	result.sample_count = (u32)(seconds * samples_per_second);
	result.samples = (float *)(dst_arena->AllocateZeroed(sizeof(float) * result.sample_count));
	result.play_position = 0;

	for (u32 i = 0; i < result.sample_count; ++i) {
		float t = (float)i * 1.0f / (float)samples_per_second;
		float fsample = f(t) * volume;
		result.samples[i] = fsample;
	}

	return result;
}

SOUND_WAVE_FUNC(ShieldSound) {
	float freq = 440.0f + 880.0f * sqrtf(t);
	float a = 1.0f / freq;
	float b = floorf(t / a + 0.5f);

	float f = 0.8f * ((2.0f / a) * (t - a * b) * powf(-1, b));

	freq /= 1.5f;
	a = 1.0f / freq;
	b = floorf(t / a + 0.5f);

	f += 0.2f * ((2.0f / a) * (t - a * b) * powf(-1, b));

	return f * expf(-5 * t * t);
}

SOUND_WAVE_FUNC(ShieldBreakSound) {
	float t_ = t * 2.0f;

	float freq = 440.0f - 220.0f * sqrtf(t_);
	float a = 1.0f / freq;
	float b = floorf(t_ / a + 0.5f);

	float f = (2.0f / a) * (t_ - a * b) * powf(-1, b);

	return f * expf(-5 * t_ * t_);
}

SOUND_WAVE_FUNC(ShootSound) {
	float freq = 880.0f * expf(-12 * t * t);

	float s = sinf(PI32 * freq * t);
	float f = 0.8f * sgnf(s);

	freq *= (6.0f / 5.0f);
	s = sinf(PI32 * freq * t);
	f += 0.2f * sgnf(s);

	return f;
}

static float
SquareWave(float freq, float t) {
	return (float)sgnf(sinf(PI32 * freq * t));
}

static float
TriangleWave(float freq, float t) {
	float a = 1.0f / freq;
	float b = floorf(t / a + 0.5f);

	return ((2.0f / a) * (t - a * b) * powf(-1, b));
}

static float
SawWave(float freq, float t) {
	float a = 1.0f / freq;
	float b = t / a;

	float f = 2.0f * (b - floorf(0.5f + b));

	return f;
}

SOUND_WAVE_FUNC(ExplosionSound) {
	float f = 0.0f;
	f += SawWave(110.0f, t);
	f += SawWave(120.0f - 10.0f * t, t);
	f *= expf(-5 * t);
	f *= f;

	f += 0.25f * SawWave(274.0f - 100.0f * t * t, t);
	f += TriangleWave(220.0f - 110.0f * t, t);
	f += TriangleWave(110.0f, t);
	f += TriangleWave(330.0f, t);
	f += SquareWave(70.0f, t);

	return clamp(f, -1.0f, 1.0f) * expf(-2 * t);
}

SOUND_WAVE_FUNC(HitSound) {
	float f = 0.0f;
	f += SawWave(220.0f, t);
	f += SawWave(340.0f - 10.0f * t, t);
	f *= expf(-7 * t);
	f *= f;

	f += 0.25f * SawWave(274.0f - 100.0f * t * t, t);
	f += TriangleWave(220.0f + 110.0f * t, t);
	f += TriangleWave(110.0f, t);
	f += TriangleWave(330.0f, t);
	f += SquareWave(70.0f, t);

	return clamp(f, -1.0f, 1.0f) * expf(-4 * t);
}

SOUND_WAVE_FUNC(MenuSound) {
	float f = 0.0f;
	f += SquareWave(500.0f, t);
	f += 0.5f * TriangleWave(750.0f, t);
	f += 0.5f * TriangleWave(1000.0f, t);

	return clamp(f, -1.0f, 1.0f) * expf(-12 * t);
}

SOUND_WAVE_FUNC(LaserFireSound) {
	float x = t / 0.5f;
	float h = 9.0f*x;
	float freq = 400.0f + 800.0f * h*expf(1.0f-h);
	float f = 0.0f;
	f += SquareWave(freq, t);
	f += 0.5f * TriangleWave(freq  * 0.5f, t);
	f += 0.35f * SawWave(freq * 0.25f, t);

	return clamp(f, -1.0f, 1.0f) * expf(-2 * t);
}

SOUND_WAVE_FUNC(PlasmaFireSound) {
	float x = t / 0.5f;
	float h = 9.0f*x;
	float freq = 500.0f - 200.0f * h*expf(1.0f - h);
	float f = 0.0f;
	f += TriangleWave(freq  * 0.5f, t);
	f += 0.5f * SawWave(freq * 0.25f, t);

	return clamp(f, -1.0f, 1.0f) * expf(-2 * t);
}

SoundBuffer 
MakeSineWave(float frequency, u32 seconds, u32 sample_rate, float volume, MemoryArena * arena) {
	SoundBuffer result;
	result.sample_count = seconds * sample_rate;
	result.samples = (float *)arena->AllocateZeroed(result.sample_count * sizeof(float));
	result.play_position = 0;

	float dtheta = (frequency * (PI32 * 2.0f)) / (float)sample_rate;
	float theta = 0;
	for (u32 i = 0; i < result.sample_count; ++i) {
		float sample = (float)(volume* sin(theta));
		result.samples[i] = sample;
		theta += dtheta;
		if (theta > 2.0f * PI32) { theta -= PI32 * 2.0f; }
	}

	return result;
}

SoundBuffer
MakeSquareWave(float frequency, float seconds, u32 sample_rate, float volume, MemoryArena * arena) {
	SoundBuffer result;
	result.sample_count = (u32)(seconds * sample_rate);
	result.samples = (float *)arena->AllocateZeroed(result.sample_count * sizeof(float));
	result.play_position = 0;

	u32 half_wave_period = (u32)(sample_rate / (frequency * 2.0f));

	float sample = volume;
	for (u32 i = 0; i < result.sample_count; ++i) {
		if (i % half_wave_period == 0) {
			sample = -sample;
		}
		result.samples[i] = sample;
	}

	return result;
}

SoundBuffer
MakeSawWave(float frequency, float seconds, u32 sample_rate, float volume, MemoryArena * arena) {
	SoundBuffer result;
	result.sample_count = (u32)(seconds * sample_rate);
	result.samples = (float *)arena->AllocateZeroed(result.sample_count * sizeof(float));
	result.play_position = 0;

	u32 wave_period = (u32)(sample_rate / frequency);
	float damp = -(volume * 2.0f / wave_period);
	float sample = volume;
	for (u32 i = 0; i < result.sample_count; ++i) {
		if (i % wave_period == 0) {
			sample = volume;
		}
		result.samples[i] = sample;
		sample -= damp;
	}

	return result;
}

SoundBuffer
MakeSilence(u32 seconds, u32 sample_rate, MemoryArena * arena) {
	SoundBuffer result;
	result.sample_count = seconds * sample_rate;
	result.samples = (float *)arena->AllocateZeroed(result.sample_count * sizeof(float));
	result.play_position = 0;

	return result;
}

static SoundBuffer sound_cache[SFXHandle_count];

void 
InitSoundCache(u32 sample_rate, MemoryArena * arena, GameAssets * assets) {
	sound_cache[DEBUG_Silence] = MakeSilence(2, sample_rate, arena);
	sound_cache[DEBUG_Sine440] = MakeSineWave(440.0f, 1, sample_rate, 0.4f, arena);
	sound_cache[DEBUG_Square500] = MakeSquareWave(500.0f, 0.125f, sample_rate, 0.5f, arena);
	sound_cache[DEBUG_Saw330] = MakeSawWave(220.0f, 0.25f, sample_rate, 0.5f, arena);
	sound_cache[OGG_Music01] = assets->assets[AssetID_OggGameMusic].music;
	sound_cache[GEN_ShieldPickup] = GenerateSoundBuffer(ShieldSound, 1.0f, sample_rate, 0.5f, arena);
	sound_cache[GEN_ShieldBreak] = GenerateSoundBuffer(ShieldBreakSound, 0.5f, sample_rate, 0.5f, arena);
	sound_cache[GEN_Shoot] = GenerateSoundBuffer(ShootSound, 0.125f, sample_rate, 0.5f, arena);
	sound_cache[GEN_LaserFire] = GenerateSoundBuffer(LaserFireSound, 0.5f, sample_rate, 1.0f, arena);
	sound_cache[GEN_PlasmaFire] = GenerateSoundBuffer(PlasmaFireSound, 0.125f, sample_rate, 0.5f, arena);
	sound_cache[GEN_Explosion] = GenerateSoundBuffer(ExplosionSound, 0.75f, sample_rate, 0.5f, arena);
	sound_cache[GEN_HitSound] = GenerateSoundBuffer(HitSound, 0.125f, sample_rate, 0.75f, arena);
	sound_cache[GEN_MenuSound] = GenerateSoundBuffer(MenuSound, 0.125f, sample_rate, 0.5f, arena);
}

SoundBuffer 
GetSFXSoundBuffer(SFXHandle handle) {
	assert(handle < array_count(sound_cache));
	return sound_cache[handle];
}

bool
AddSFXBuffer(SoundMixer * mixer, SoundBuffer sound, float volume, float pan) {
	if (mixer->active_channels < array_count(mixer->channels)) {
		SoundChannel * chan = &mixer->channels[mixer->active_channels++];
		chan->sound = sound;
		chan->pan = pan;
		chan->volume = volume;
		chan->flags = 0;
		return true;
	}
	else {
		return false;
	}
}

void 
ClearChannel(SoundMixer * mixer, u32 i) {
	assert(i > 0);  // Channel 0 is reserved for music, cannot be removed.
	assert(i < array_count(mixer->channels));
	assert(i < mixer->active_channels);

	mixer->channels[i] = mixer->channels[--mixer->active_channels];
}

void
MixerCommitTime(SoundMixer * mixer, u32 frames_advance) {
	for (u32 i = 0; i < mixer->active_channels; ++i) {
		SoundChannel * channel = mixer->channels + i;
		channel->sound.play_position += frames_advance;
		if (channel->sound.play_position >= channel->sound.sample_count) {
			if (channel->flags & CF_Looping) {
				channel->sound.play_position = 0;
			}
			else {
				ClearChannel(mixer, i);
			}
		}
	}
}

SoundBuffer
MixSamplesScalar(SoundMixer * mixer, u32 stereo_samples_to_mix, MemoryArena * arena) {
	// Always output a multiple of 8 samples, for compatability w/ SIMD version.
	u32 sample_buffer_size = (stereo_samples_to_mix + 7) / 8;
	sample_buffer_size *= 8;
	SoundBuffer result;

	result.samples = (float *)arena->AllocateZeroed(sample_buffer_size * 2 * sizeof(float));
	result.sample_count = stereo_samples_to_mix;
	result.play_position = 0;

	u32 active_channels = mixer->active_channels;
	for (u32 chan_idx = 0; chan_idx < active_channels; ++chan_idx) {
		SoundChannel * channel = &mixer->channels[chan_idx];

		float volume = channel->volume / array_count(mixer->channels);

		// Scale by relative volumes.
		// Music is always on channel 0.
		volume *= mixer->master_volume;
		volume *= chan_idx == 0 ? mixer->music_volume : mixer->effects_volume;

		float pan_left = channel->pan;
		float pan_right = 1.0f - pan_left;

		pan_left *= 2.0f;
		pan_right *= 2.0f;

		u32 sample_iters = stereo_samples_to_mix;
		if (!(channel->flags & CF_Looping)) {
			sample_iters = min(channel->sound.sample_count - channel->sound.play_position, stereo_samples_to_mix);
		}
		u32 cur_play_pos = channel->sound.play_position;
		for (u32 i = 0; i < sample_iters; ++i) {
			float sample = *(channel->sound.samples + cur_play_pos);

			float chan_left = sample * pan_left;
			float chan_right = sample * pan_right;

			chan_left *= volume;
			chan_right *= volume;

			float mix_left = result.samples[i * 2];
			float mix_right = result.samples[i * 2 + 1];

			mix_left += chan_left;
			mix_right += chan_right;

			result.samples[i * 2] = mix_left;
			result.samples[i * 2 + 1] = mix_right;
			cur_play_pos++;
			if (cur_play_pos >= channel->sound.sample_count) {
				if (channel->flags & CF_Looping) {
					cur_play_pos = 0;
				}
				else {
					break;
				}
			}
		}
	}

	// TODO(bryan):  We'd like to just not mix at all in this case, but
	// need to keep track of play positions for each channel either way.
	if (mixer->mute) {
		memset(result.samples, 0, result.sample_count * 2 * sizeof(float));
	}

	return result;
}
