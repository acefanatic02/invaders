//
// Invaders - sound.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"

struct SoundBuffer {
	float * samples;
	u32 sample_count;
	u32 play_position;
};

struct SoundChannel {
	SoundBuffer sound;
	float pan;
	float volume;
	u32 flags;
};

enum ChannelFlags {
	CF_Looping = 1 << 0,
	CF_Stereo = 1 << 1,
};

struct SoundMixer {
	SoundChannel channels[8];
	u32 active_channels;
	float master_volume;
	float music_volume;
	float effects_volume;
	bool mute;
	bool headphone_mix;
};

enum SFXHandle {
	DEBUG_Silence,
	DEBUG_Sine440,
	DEBUG_Square500,
	OGG_Sine440,
	DEBUG_Saw330,
	OGG_Music01,
	GEN_ShieldPickup,
	GEN_ShieldBreak,
	GEN_Shoot,
	GEN_LaserFire,
	GEN_PlasmaFire,
	GEN_Explosion,
	GEN_HitSound,
	GEN_MenuSound,
	SFXHandle_count,
};
struct MemoryArena;

SoundBuffer MakeSineWave(float frequency, u32 seconds, u32 sample_rate, float volume, MemoryArena * arena);
SoundBuffer MakeSquareWave(float frequency, u32 seconds, u32 sample_rate, float volume, MemoryArena * arena);

struct GameAssets;
void InitSoundCache(u32 sample_rate, MemoryArena * arena, GameAssets * assets);
SoundBuffer GetSFXSoundBuffer(SFXHandle handle);

bool AddSFXBuffer(SoundMixer * mixer, SoundBuffer sound, float volume, float pan);
void ClearChannel(SoundMixer * mixer, u32 i);

void MixerCommitTime(SoundMixer * mixer, u32 frames_advance);

SoundBuffer MixSamplesScalar(SoundMixer * mixer, u32 stereo_samples_to_mix, MemoryArena * arena);