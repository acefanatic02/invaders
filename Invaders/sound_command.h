//
// Invaders - sound_command.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "vectypes.h"
#include "sound.h"

struct SoundCommandBuffer {
	u32 size;
	u32 top;
	void * buffer;
};

void PlaySFXBark(SoundCommandBuffer * buffer, vec2 position, float volume, SFXHandle sfx);

void PlaySFXOneShot(SoundCommandBuffer * buffer, vec2 position, float volume, SFXHandle sfx);

void SetMusic(SoundCommandBuffer * buffer, SFXHandle handle);