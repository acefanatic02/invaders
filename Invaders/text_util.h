//
// Invaders - text_util.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"
#include "rectangle.h"
//#include "stb_easy_font.h"

extern int stb_easy_font_width(char *);

float UITextWidth(char * string) {
	return (float)stb_easy_font_width(string);
}

// Returns the bounds around the string from top to baseline.
// Does not include padding, does not include tails below the baseline.
Rect UITextBounds(char * string, vec2 position, float scale) {
	float width = UITextWidth(string);
	float height = -7.0f;

	vec2 min = position;
	vec2 max = position + Vec2(width, height) * scale;

	return RectFromMinMax(min, max);
}

Rect UITextBoundsPadding(char * string, vec2 position, float scale) {
	float width = UITextWidth(string);
	float height = -12.0f;

	vec2 min = position;
	vec2 max = position + Vec2(width, height) * scale;

	return RectFromMinMax(min, max);
}

vec2 UITextCenterOffset(char * string, float scale) {
	float width = UITextWidth(string);
	float height = -12.0f;

	return Vec2(width, height) * (scale * 0.5f);
}