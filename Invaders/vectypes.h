//
// Invaders - vectypes.h
//
// Copyright (c) 2015 asobou Interactive
// All Rights Reserved.
//

#pragma once

#include "invaders.h"

#pragma pack(push, 1)
struct vec2 {
	float x;
	float y;

	inline vec2& operator+=(const vec2& rhs);
	inline vec2& operator-=(const vec2& rhs);
	inline vec2& operator*=(float rhs);
	inline vec2& operator/=(float rhs);
};

inline vec2 Vec2() { vec2 v = {}; return v; }
inline vec2 Vec2(float _x, float _y) { vec2 v = {_x, _y}; return v; }

inline vec2&
vec2::operator+=(const vec2& rhs) {
	x += rhs.x;
	y += rhs.y;
	return *this;
}

inline vec2&
vec2::operator-=(const vec2& rhs) {
	x -= rhs.x;
	y -= rhs.y;
	return *this;
}

inline vec2&
vec2::operator*=(float rhs) {
	x *= rhs;
	y *= rhs;
	return *this;
}

inline vec2&
vec2::operator/=(float rhs) {
	x /= rhs;
	y /= rhs;
	return *this;
}

inline vec2
operator+(vec2 a, vec2 b) {
	return Vec2(
			a.x + b.x,
			a.y + b.y
		);
}

inline vec2
operator-(vec2 a, vec2 b) {
	return Vec2(
			a.x - b.x,
			a.y - b.y
		);
}

inline vec2
operator*(vec2 a, vec2 b) {
	return Vec2(
			a.x * b.x,
			a.y * b.y
		);
}

inline vec2
operator*(vec2 a, float f) {
	return Vec2(
			a.x * f,
			a.y * f
		);
}

inline vec2
operator/(vec2 a, float f) {
	return Vec2(
			a.x / f,
			a.y / f
		);
}

union vec3 {
	struct { float x, y, z; };
	struct { vec2 xy; float ignore_0; };
	struct { float ignore_1; vec2 yz; };
	float e[3];

	inline vec3& operator+=(const vec3& rhs);
	inline vec3& operator-=(const vec3& rhs);
	inline vec3& operator*=(float rhs);
	inline vec3& operator/=(float rhs);
};

inline vec3
Vec3(float x, float y, float z) {
	vec3 v;
	v.x = x;
	v.y = y;
	v.z = z;

	return v;
}

inline vec3&
vec3::operator+=(const vec3& rhs) {
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;

	return *this;
}

inline vec3&
vec3::operator-=(const vec3& rhs) {
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;

	return *this;
}

inline vec3&
vec3::operator*=(float rhs) {
	x *= rhs;
	y *= rhs;
	z *= rhs;

	return *this;
}

inline vec3&
vec3::operator/=(float rhs) {
	x /= rhs;
	y /= rhs;
	z /= rhs;

	return *this;
}

inline vec3
operator+(vec3 a, vec3 b) {
	return Vec3(
			a.x + b.x,
			a.y + b.y,
			a.z + b.z
		);
}

inline vec3
operator-(vec3 a, vec3 b) {
	return Vec3(
			a.x - b.x,
			a.y - b.y,
			a.z - b.z
		);
}

inline vec3
operator*(vec3 a, float f) {
	return Vec3(
			a.x * f,
			a.y * f,
			a.z * f
		);
}

inline vec3
operator/(vec3 a, float f) {
	return Vec3(
			a.x / f,
			a.y / f,
			a.z / f
		);
}

struct vec4 {
	union {
		struct { float x, y, z, w; };
		struct { vec2 xy; vec2 zw; };
		struct { vec3 xyz; float ignore_0; };
		struct { float r, g, b, a; };
		struct { vec3 rgb; float ignore_1; };
		float e[4];
	};

	inline vec4& operator+=(const vec4& rhs);
	inline vec4& operator-=(const vec4& rhs);
	inline vec4& operator*=(float rhs);
	inline vec4& operator/=(float rhs);
};

inline vec4
Vec4(float x, float y, float z, float w) {
	vec4 v;
	v.x = x;
	v.y = y;
	v.z = z;
	v.w = w;

	return v;
}
inline vec4&
vec4::operator+=(const vec4& rhs) {
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w += rhs.w;

	return *this;
}

inline vec4&
vec4::operator-=(const vec4& rhs) {
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	w -= rhs.w;

	return *this;
}

inline vec4&
vec4::operator*=(float rhs) {
	x *= rhs;
	y *= rhs;
	z *= rhs;
	w *= rhs;

	return *this;
}

inline vec4&
vec4::operator/=(float rhs) {
	x /= rhs;
	y /= rhs;
	z /= rhs;
	w /= rhs;

	return *this;
}

inline vec4
operator+(vec4 a, vec4 b) {
	return Vec4(
			a.x + b.x,
			a.y + b.y,
			a.z + b.z,
			a.w + b.w
		);
}

inline vec4
operator-(vec4 a, vec4 b) {
	return Vec4(
			a.x - b.x,
			a.y - b.y,
			a.z - b.z,
			a.w - b.w
		);
}

inline vec4
operator*(vec4 a, float f) {
	return Vec4(
			a.x * f,
			a.y * f,
			a.z * f,
			a.w * f
		);
}

inline vec4
operator/(vec4 a, float f) {
	return Vec4(
			a.x / f,
			a.y / f,
			a.z / f,
			a.w / f
		);
}

inline vec3
cross(vec3 a, vec3 b) {
	return Vec3(
			a.y*b.z - a.z*b.y,
			a.z*b.x - a.x*b.z,
			a.x*b.y - a.y*b.x
		);
}

inline float magnitude(vec3);

struct quaternion {
	float w;
	float x;
	float y;
	float z;

	inline quaternion() : w(1.0f), x(0.0f), y(0.0f), z(0.0f) { }
	inline quaternion(float _w, float _x, float _y, float _z) : w(_w), x(_x), y(_y), z(_z) { }
	inline quaternion(float theta, vec3 axis) {
		// Assumes axis is normalized:
		//assert(magnitude(axis) == 1.0f);

		float s = sinf(theta / 2.0f);
		float c = cosf(theta / 2.0f);

		w = c;
		x = axis.x * s;
		y = axis.y * s;
		z = axis.z * s;
	}
};

inline quaternion
operator*(quaternion a, quaternion b) {
	return quaternion(
			a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z,
			a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y,
			a.w*b.y - a.x*b.z + a.y*b.w + a.z*b.x,
			a.w*b.z + a.x*b.y - a.y*b.x + a.z*b.w
		);
}

inline quaternion
operator*(quaternion q, float f) {
	return quaternion(q.w * f, q.x * f, q.y * f, q.z * f);
}

inline float dot(vec3, vec3);

inline vec3
operator*(quaternion rot, vec3 v) {
	float s = rot.w;
	float t = sqrtf(rot.x*rot.x + rot.y*rot.y + rot.z*rot.z);
	vec3 axis = Vec3(rot.x, rot.y, rot.z) / t;

	return (v * (s*s - t*t)) + 
		(cross(axis, v) * (2.0f*s*t)) + 
		(v * (2.0f*t*t*dot(axis, v)));
}

inline float 
dot(vec2 a, vec2 b) {
	return a.x * b.x + a.y * b.y;
}

inline float 
dot(vec3 a, vec3 b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline float 
dot(vec4 a, vec4 b) {
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

inline float
dot(quaternion a, quaternion b) {
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

inline float
magnitude(vec2 a) {
	return sqrtf(dot(a, a));
}

inline float
magnitude(vec3 a) {
	return sqrtf(dot(a, a));
}

inline float
magnitude(vec4 a) {
	return sqrtf(dot(a, a));
}

inline float
magnitude(quaternion q) {
	return sqrtf(dot(q, q));
}

inline vec2
normalize(vec2 a) {
	float mag = magnitude(a);
	return a / mag;
}

inline vec3
normalize(vec3 a) {
	float mag = magnitude(a);
	return a / mag;
}

inline vec4
normalize(vec4 a) {
	float mag = magnitude(a);
	return a / mag;
}

inline quaternion 
normalize(quaternion q) {
	float mag = magnitude(q);
	return q * (1.0f / mag);
}

inline vec2
project(vec2 a, vec2 b) {
	return a * dot(a, b);
}

inline vec2
perp(vec2 a) {
	return Vec2(-a.y, a.x);
}

inline quaternion
QuaternionFromEuler(vec3 euler) {
	quaternion rot_x(DEG2RAD(euler.x), Vec3(1.0f, 0.0f, 0.0f));
	quaternion rot_y(DEG2RAD(euler.y), Vec3(0.0f, 1.0f, 0.0f));
	quaternion rot_z(DEG2RAD(euler.z), Vec3(0.0f, 0.0f, 1.0f));

	return rot_x * rot_y * rot_z;
}
#pragma pack(pop)
