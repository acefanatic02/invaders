    asobou Interactive
        Presents

         #########   ###   ###   ###   ###    ###     #######    ########   ########   #######   ###
        #########   ###   ###   ###   ###   #####    ########   ########   ########  ########   ###
          ###      ####  ###   ###   ###   #####    ###   ###  ###        ###  ###  ####       ###
         ###      ##### ###    ### ###   ### ###   ###   ###  #######    ########   #####     ###
        ###      ### #####    ### ###   ### ###   ###   ###  #######    #######      #####   ###
       ###      ###  ####     #####   #########  ###   ###  ###        ###  ###       ####  
   #########   ###   ###     #####   ###   ###  ########   ########   ###  ###  #########  ###
  #########   ###   ###      ###    ###   ###  #######    ########   ###  ###  #######    ###
 ===========================================================================================
===========================================================================================

Invaders! is an homage to old-school, simple, but challenging arcade games.  Stand fast 
as you ward off an endless onslaught of alien invaders.  How long can you last?

Enemies:

   #####################
 #########################
# ##  ###   ###   ###  ## #
# ##  ###   ###   ###  ## #
# ##  ###   ###   ###  ## #
 #########################

- Invader:
  Large, ungainly ships, with minimal protection.  Armed with a plasma cannon whose shots
home in on EM signatures--like your ship!  Take special care while your shield is active, 
as it will draw their fire.

          #######
        ##       ##
       #           #
      #             #
  #######################
###########################
   #####################

- Saucer
  Less common, sturdier ships that hold back and let the others take fire.  Armed with an
extremely powerful, shield-penetrating laser.  Glows red as the laser charges--when the 
glow flashes, dodge!
  The better you perform, the more common Saucers become.

Powerups:

   #####
 ##     ## 
##       ##
 ##     ## 
   #####

- Shield:
  Automatically activates when picked up.  The shield will protect you from a single shot,
but be warned--it will cause plasma to home in on your ship more effectively.
  Will not block a laser strike, but it'll take some of the edge off.

  ####### 
##   #   ##
#  #####  #
##   #   ## 
  ####### 

- Health:
  Restores one point of health, up to a maximum of 9.  Note that normal Invaders will 
never drop health.

You:
           #
          ###
         ## ##
         ## ##
 #      ##   ##      #
 #      ##   ##      #
###    #########    ###
#######################
#######################
 ##    ###   ###    ##

- Ship:
  A sturdy craft armed with dual laser cannons.  Defender or bait?  Only the skill of
its pilot will decide!


Controls:

  Keyboard:
    Menu Navigation:    Arrow Keys
    Menu Select:        Enter
    Menu Escape:        Escape

    Player Movement:    Left / Right Arrow Keys
    Player Fire:        Spacebar

  Gamepad (X360):
    Menu Navigation:    D-Pad
    Menu Select:        A Button
    Menu Escape:        Start Button

    Player Movement:    Left Thumbstick
    Player Fire:        A Button / Right Shoulder Button


Options:

  Display:
  - Choose whether to play in Fullscreen or Windowed mode.  (You can switch at any 
    time by pressing Alt+Enter on the keyboard.)

  Audio Mix:
  - The Headphone Mix uses less severe stereo panning, and is highly recommended when
    using headphones.
  - The Speaker Mix uses the full stereo range, and is recommended when playing through
    speakers.

  Mute:
  - Mute all audio.

  Volume:
  - Master - Adjust the volume of all sounds in game.
  - Music  - Adjust the volume of the in-game music.
  - Effect - Adjust the volume of all sound effects.

  Screenshake:
  - Adjust the amount of visible screenshake.  If you experience motion sickness while
    playing, slide this down.


Invaders! is the first release from asobou Interactive.  Let's play.
  http://asobou.itch.io/
  asobou.interactive (at) gmail.com

  Design / Programming / Art / Music:
  - Bryan Taylor

  Testing:
  - Eric West
  - Shawn Sloan
  - Kanmuri
  - Ritobito
  - BlackDragonHunt

  Special Thanks:
  - Casey Muratori, for his excellent Handmade Hero video series, a constant source of 
    inspiration and knowledge:  

    http://www.twitch.tv/handmade_hero/  
    https://www.youtube.com/handmadeheroarchive/

  - Sean Barrett, for his public-domain stb libraries.  Invaders! uses:
    - stb_image
    - stb_vorbis
    - stb_easy_font
    - stb_audio_mixer     (partial)

    https://github.com/nothings/stb/


Thank you for playing!

Copyright (c) 2015 asobou Interactive.  All Rights Reserved.